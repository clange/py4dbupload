#!/bin/bash

echo 'Available connections: '
python3 ${RHAPI} --url=https://cmsdca.cern.ch/trk_rhapi --login

DB='trker_cmsr'
echo 'Connect to ' ${DB}
echo

if [ $# -eq 1 ]
then

SERIAL="$1"

echo "Gathering condition data attached to component ${PARTID}"
echo

python3 ${RHAPI} --url=https://cmsdca.cern.ch/trk_rhapi --login "select c.part_serial_number  as hybrid, r.run_number as run, r.run_type, r.begin_date as run_date, c.qualification as grade, c.known_problems as problems , c.failure_cause as failure from ${DB}.c9820 c left join ${DB}.datasets d on d.id=c.condition_data_set_id left join ${DB}.runs r on r.id=d.run_id where c.part_serial_number='${SERIAL}' order by r.begin_date asc" --all -n | column -s, -t

else

#KIND="$*"
echo "Scripts need one argument, the Serial Number of the Hybrid."
echo "   usage: check_hybrid_test.sh PSFEH16L-201000227"

fi

