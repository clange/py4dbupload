18.04.2019 18:17 ||I| [1m[34mAntenna potential set to 613[0m
18.04.2019 18:17 ||I| [1m[34mScanning L1 latency between 50 and 60[0m
18.04.2019 18:17 ||I| 


********************************************************************************
                                        [1m[31mHW SUMMARY: [0m
********************************************************************************

[1m[36m|----BeBoard  Id :[1m[34m0[1m[36m BoardType: [1m[34mD19C[1m[36m EventType: [1m[31mVR[0m
[1m[34m|       |----Board Id:      [1m[33mboard
[1m[34m|       |----URI:           [1m[33mchtcp-2.0://localhost:10203?target=192.168.0.7:50001
[1m[34m|       |----Address Table: [1m[33mfile://settings/address_tables/d19c_address_table.xml
[1m[34m|       |[0m
[34m|	|----Register  clock_source: [1m[33m3[0m
[34m|	|----Register  fc7_daq_cnfg.clock.ext_clk_en: [1m[33m0[0m
[34m|	|----Register  fc7_daq_cnfg.ttc.ttc_enable: [1m[33m0[0m
[34m|	|----Register  fc7_daq_cnfg.fast_command_block.triggers_to_accept: [1m[33m0[0m
[34m|	|----Register  fc7_daq_cnfg.fast_command_block.trigger_source: [1m[33m7[0m
[34m|	|----Register  fc7_daq_cnfg.fast_command_block.user_trigger_frequency: [1m[33m10[0m
[34m|	|----Register  fc7_daq_cnfg.fast_command_block.stubs_mask: [1m[33m1[0m
[34m|	|----Register  fc7_daq_cnfg.fast_command_block.stub_trigger_delay_value: [1m[33m0[0m
[34m|	|----Register  fc7_daq_cnfg.fast_command_block.stub_trigger_veto_length: [1m[33m0[0m
[34m|	|----Register  fc7_daq_cnfg.fast_command_block.test_pulse.delay_after_fast_reset: [1m[33m2[0m
[34m|	|----Register  fc7_daq_cnfg.fast_command_block.test_pulse.delay_after_test_pulse: [1m[33m20[0m
[34m|	|----Register  fc7_daq_cnfg.fast_command_block.test_pulse.delay_before_next_pulse: [1m[33m700[0m
[34m|	|----Register  fc7_daq_cnfg.fast_command_block.test_pulse.en_fast_reset: [1m[33m1[0m
[34m|	|----Register  fc7_daq_cnfg.fast_command_block.test_pulse.en_test_pulse: [1m[33m1[0m
[34m|	|----Register  fc7_daq_cnfg.fast_command_block.test_pulse.en_l1a: [1m[33m1[0m
[34m|	|----Register  fc7_daq_cnfg.fast_command_block.ext_trigger_delay_value: [1m[33m50[0m
[34m|	|----Register  fc7_daq_cnfg.fast_command_block.antenna_trigger_delay_value: [1m[33m50[0m
[34m|	|----Register  fc7_daq_cnfg.fast_command_block.delay_between_two_consecutive: [1m[33m10[0m
[34m|	|----Register  fc7_daq_cnfg.fast_command_block.misc.backpressure_enable: [1m[33m1[0m
[34m|	|----Register  fc7_daq_cnfg.fast_command_block.misc.stubOR: [1m[33m1[0m
[34m|	|----Register  fc7_daq_cnfg.fast_command_block.misc.initial_fast_reset_enable: [1m[33m0[0m
[34m|	|----Register  fc7_daq_cnfg.physical_interface_block.i2c.frequency: [1m[33m4[0m
[34m|	|----Register  fc7_daq_cnfg.readout_block.packet_nbr: [1m[33m99[0m
[34m|	|----Register  fc7_daq_cnfg.readout_block.global.data_handshake_enable: [1m[33m1[0m
[34m|	|----Register  fc7_daq_cnfg.readout_block.global.int_trig_enable: [1m[33m0[0m
[34m|	|----Register  fc7_daq_cnfg.readout_block.global.int_trig_rate: [1m[33m0[0m
[34m|	|----Register  fc7_daq_cnfg.readout_block.global.trigger_type: [1m[33m0[0m
[34m|	|----Register  fc7_daq_cnfg.readout_block.global.data_type: [1m[33m0[0m
[34m|	|----Register  fc7_daq_cnfg.readout_block.global.common_stubdata_delay: [1m[33m5[0m
[34m|	|----Register  fc7_daq_cnfg.dio5_block.dio5_en: [1m[33m0[0m
[34m|	|----Register  fc7_daq_cnfg.dio5_block.ch1.out_enable: [1m[33m1[0m
[34m|	|----Register  fc7_daq_cnfg.dio5_block.ch1.term_enable: [1m[33m0[0m
[34m|	|----Register  fc7_daq_cnfg.dio5_block.ch1.threshold: [1m[33m0[0m
[34m|	|----Register  fc7_daq_cnfg.dio5_block.ch2.out_enable: [1m[33m0[0m
[34m|	|----Register  fc7_daq_cnfg.dio5_block.ch2.term_enable: [1m[33m1[0m
[34m|	|----Register  fc7_daq_cnfg.dio5_block.ch2.threshold: [1m[33m50[0m
[34m|	|----Register  fc7_daq_cnfg.dio5_block.ch3.out_enable: [1m[33m1[0m
[34m|	|----Register  fc7_daq_cnfg.dio5_block.ch3.term_enable: [1m[33m0[0m
[34m|	|----Register  fc7_daq_cnfg.dio5_block.ch3.threshold: [1m[33m0[0m
[34m|	|----Register  fc7_daq_cnfg.dio5_block.ch4.out_enable: [1m[33m0[0m
[34m|	|----Register  fc7_daq_cnfg.dio5_block.ch4.term_enable: [1m[33m1[0m
[34m|	|----Register  fc7_daq_cnfg.dio5_block.ch4.threshold: [1m[33m50[0m
[34m|	|----Register  fc7_daq_cnfg.dio5_block.ch5.out_enable: [1m[33m0[0m
[34m|	|----Register  fc7_daq_cnfg.dio5_block.ch5.term_enable: [1m[33m1[0m
[34m|	|----Register  fc7_daq_cnfg.dio5_block.ch5.threshold: [1m[33m50[0m
[34m|	|----Register  fc7_daq_cnfg.tlu_block.handshake_mode: [1m[33m2[0m
[34m|	|----Register  fc7_daq_cnfg.tlu_block.tlu_enabled: [1m[33m0[0m
[34m|	|[0m
[1m[36m|	|----Module  FeId :0[0m
[32m|	|	|----CBC Files Path : hybrid_functional_tests/8CBC318-201000032/calibration/Calibration_Electron_18-04-19_18h17/[0m
[1m[36m|	|	|----CBC  Id :0, File: FE0CBC0.txt[0m
[32m|	|	|	|----ChipType: [31mCBC3[0m
[1m[36m|	|	|----CBC  Id :1, File: FE0CBC1.txt[0m
[32m|	|	|	|----ChipType: [31mCBC3[0m
[1m[36m|	|	|----CBC  Id :2, File: FE0CBC2.txt[0m
[32m|	|	|	|----ChipType: [31mCBC3[0m
[1m[36m|	|	|----CBC  Id :3, File: FE0CBC3.txt[0m
[32m|	|	|	|----ChipType: [31mCBC3[0m
[1m[36m|	|	|----CBC  Id :4, File: FE0CBC4.txt[0m
[32m|	|	|	|----ChipType: [31mCBC3[0m
[1m[36m|	|	|----CBC  Id :5, File: FE0CBC5.txt[0m
[32m|	|	|	|----ChipType: [31mCBC3[0m
[1m[36m|	|	|----CBC  Id :6, File: FE0CBC6.txt[0m
[32m|	|	|	|----ChipType: [31mCBC3[0m
[1m[36m|	|	|----CBC  Id :7, File: FE0CBC7.txt[0m
[32m|	|	|	|----ChipType: [31mCBC3[0m
[1m[36m|	|	|----Global CBC Settings: [0m
[32m|	|	|	|----ChipType: [31mCBC3[0m
[32m|	|	|	|----VCth: [31m0x244 (580)[0m
[32m|	|	|	|----TriggerLatency: [31m0x13 (19)[0m
[32m|	|	|	|----TestPulse: enabled: [31m0[32m, polarity: [31m0[32m, amplitude: [31m128[32m (0x80)[0m
[32m|	|	|	|               channelgroup: [31m0[32m, delay: [31m0[32m, groundohters: [31m1[0m
[32m|	|	|	|----Cluster & Stub Logic: ClusterWidthDiscrimination: [31m4[32m, PtWidth: [31m14[32m, Layerswap: [31m0[0m
[32m|	|	|	|                          Offset1: [31m0[32m, Offset2: [31m0[32m, Offset3: [31m0[32m, Offset4: [31m0[0m
[32m|	|	|	|----Misc Settings:  PipelineLogicSource: [31m0[32m, StubLogicSource: [31m1[32m, OR254: [31m0[32m, TPG Clock: [31m1[32m, Test Clock 40: [31m1[32m, DLL: [31m14[0m
[32m|	|	|	|----Analog Mux value: [31m0 (0x0, 0b00000)[0m
[32m|	|	|	|----List of disabled Channels: [0m
[34m|	|
|	|----SLink[0m
[34m|	|       |----DebugMode[35m : SLinkDebugMode::FULL[0m
[34m|	|       |----ConditionData: Type [31mI2C VCth1[34m, UID [31m1[34m, FeId [31m0[34m, CbcId [31m0[34m, Page [31m0[34m, Address [31m4f[34m, Value [35m68[0m
[34m|	|       |----ConditionData: Type [31mUser [34m, UID [31m128[34m, FeId [31m0[34m, CbcId [31m0[34m, Page [31m0[34m, Address [31m0[34m, Value [35m34[0m
[34m|	|       |----ConditionData: Type [31mHV [34m, UID [31m5[34m, FeId [31m0[34m, CbcId [31m2[34m, Page [31m0[34m, Address [31m0[34m, Value [35m250[0m
[34m|	|       |----ConditionData: Type [31mTDC [34m, UID [31m3[34m, FeId [31m255[34m, CbcId [31m0[34m, Page [31m0[34m, Address [31m0[34m, Value [35m0[0m


********************************************************************************
                                        [1m[31mEND OF HW SUMMARY: [0m
********************************************************************************


[31mSetting[0m --[1m[36mTargetVcth[0m:[1m[33m120[0m
[31mSetting[0m --[1m[36mTargetOffset[0m:[1m[33m80[0m
[31mSetting[0m --[1m[36mNevents[0m:[1m[33m100[0m
[31mSetting[0m --[1m[36mTestPulsePotentiometer[0m:[1m[33m0[0m
[31mSetting[0m --[1m[36mHoleMode[0m:[1m[33m0[0m
[31mSetting[0m --[1m[36mVerificationLoop[0m:[1m[33m0[0m
[31mSetting[0m --[1m[36mInitialVcth[0m:[1m[33m120[0m
[31mSetting[0m --[1m[36mSignalScanStep[0m:[1m[33m2[0m
[31mSetting[0m --[1m[36mFitSignal[0m:[1m[33m0[0m

18.04.2019 18:17 ||I| Creating directory: Results/Commissioning_Electron_18-04-19_18h17
Info in <TCivetweb::Create>: Starting HTTP server on port 8080
18.04.2019 18:17 ||I| Opening THttpServer on port 8080. Point your browser to: [1m[32mcmsuptkhsetup1.dyndns.cern.ch:8080[0m
18.04.2019 18:17 ||I| [1m[34mConfiguring HW parsed from .xml file: [0m
18.04.2019 18:17 ||I| [1m[32mSetting the I2C address table[0m
18.04.2019 18:17 ||I| [1m[32mAccording to the Firmware status registers, it was compiled for: 1 hybrid(s), 8 CBC3 chip(s) per hybrid[0m
18.04.2019 18:17 ||I| Enabling Hybrid 0
18.04.2019 18:17 ||I|      Enabling Chip 0
18.04.2019 18:17 ||I|      Enabling Chip 1
18.04.2019 18:17 ||I|      Enabling Chip 2
18.04.2019 18:17 ||I|      Enabling Chip 3
18.04.2019 18:17 ||I|      Enabling Chip 4
18.04.2019 18:17 ||I|      Enabling Chip 5
18.04.2019 18:17 ||I|      Enabling Chip 6
18.04.2019 18:17 ||I|      Enabling Chip 7
18.04.2019 18:17 ||I| [1m[32m1 hybrid(s) was(were) enabled with the total amount of 8 chip(s)![0m
Reply from chip 0: 2000007c
Reply from chip 1: 2004007c
Reply from chip 2: 2008007c
Reply from chip 3: 200c007c
Reply from chip 4: 2010007c
Reply from chip 5: 2014007c
Reply from chip 6: 2018007c
Reply from chip 7: 201c007c
18.04.2019 18:17 ||I| Successfully received *Pings* from 8 Cbcs
18.04.2019 18:17 ||I| [32mCBC3 Phase tuning finished succesfully[0m
18.04.2019 18:17 ||I| [1m[30m	 Hybrid: [0m0[1m[30m, Chip: [0m0[1m[30m, Line: [0m5
18.04.2019 18:17 ||I| 		 Mode: 0
18.04.2019 18:17 ||I| 		 Manual Delay: 0, Manual Bitslip: 0
18.04.2019 18:17 ||I| 		 Done: 1, PA FSM: [1m[32mTunedPHASE[0m, WA FSM: [1m[32mTunedWORD[0m
18.04.2019 18:17 ||I| 		 Delay: 7, Bitslip: 3
18.04.2019 18:17 ||I| [1m[30m	 Hybrid: [0m0[1m[30m, Chip: [0m1[1m[30m, Line: [0m5
18.04.2019 18:17 ||I| 		 Mode: 0
18.04.2019 18:17 ||I| 		 Manual Delay: 0, Manual Bitslip: 0
18.04.2019 18:17 ||I| 		 Done: 1, PA FSM: [1m[32mTunedPHASE[0m, WA FSM: [1m[32mTunedWORD[0m
18.04.2019 18:17 ||I| 		 Delay: 15, Bitslip: 3
18.04.2019 18:17 ||I| [1m[30m	 Hybrid: [0m0[1m[30m, Chip: [0m2[1m[30m, Line: [0m5
18.04.2019 18:17 ||I| 		 Mode: 0
18.04.2019 18:17 ||I| 		 Manual Delay: 0, Manual Bitslip: 0
18.04.2019 18:17 ||I| 		 Done: 1, PA FSM: [1m[32mTunedPHASE[0m, WA FSM: [1m[32mTunedWORD[0m
18.04.2019 18:17 ||I| 		 Delay: 15, Bitslip: 3
18.04.2019 18:17 ||I| [1m[30m	 Hybrid: [0m0[1m[30m, Chip: [0m3[1m[30m, Line: [0m5
18.04.2019 18:17 ||I| 		 Mode: 0
18.04.2019 18:17 ||I| 		 Manual Delay: 0, Manual Bitslip: 0
18.04.2019 18:17 ||I| 		 Done: 1, PA FSM: [1m[32mTunedPHASE[0m, WA FSM: [1m[32mTunedWORD[0m
18.04.2019 18:17 ||I| 		 Delay: 15, Bitslip: 3
18.04.2019 18:17 ||I| [1m[30m	 Hybrid: [0m0[1m[30m, Chip: [0m4[1m[30m, Line: [0m5
18.04.2019 18:17 ||I| 		 Mode: 0
18.04.2019 18:17 ||I| 		 Manual Delay: 0, Manual Bitslip: 0
18.04.2019 18:17 ||I| 		 Done: 1, PA FSM: [1m[32mTunedPHASE[0m, WA FSM: [1m[32mTunedWORD[0m
18.04.2019 18:17 ||I| 		 Delay: 15, Bitslip: 3
18.04.2019 18:17 ||I| [1m[30m	 Hybrid: [0m0[1m[30m, Chip: [0m5[1m[30m, Line: [0m5
18.04.2019 18:17 ||I| 		 Mode: 0
18.04.2019 18:17 ||I| 		 Manual Delay: 0, Manual Bitslip: 0
18.04.2019 18:17 ||I| 		 Done: 1, PA FSM: [1m[32mTunedPHASE[0m, WA FSM: [1m[32mTunedWORD[0m
18.04.2019 18:17 ||I| 		 Delay: 15, Bitslip: 3
18.04.2019 18:17 ||I| [1m[30m	 Hybrid: [0m0[1m[30m, Chip: [0m6[1m[30m, Line: [0m5
18.04.2019 18:17 ||I| 		 Mode: 0
18.04.2019 18:17 ||I| 		 Manual Delay: 0, Manual Bitslip: 0
18.04.2019 18:17 ||I| 		 Done: 1, PA FSM: [1m[32mTunedPHASE[0m, WA FSM: [1m[32mTunedWORD[0m
18.04.2019 18:17 ||I| 		 Delay: 15, Bitslip: 3
18.04.2019 18:17 ||I| [1m[30m	 Hybrid: [0m0[1m[30m, Chip: [0m7[1m[30m, Line: [0m5
18.04.2019 18:17 ||I| 		 Mode: 0
18.04.2019 18:17 ||I| 		 Manual Delay: 0, Manual Bitslip: 0
18.04.2019 18:17 ||I| 		 Done: 1, PA FSM: [1m[32mTunedPHASE[0m, WA FSM: [1m[32mTunedWORD[0m
18.04.2019 18:17 ||I| 		 Delay: 15, Bitslip: 3
18.04.2019 18:17 ||I| Waiting for DDR3 to finish initial calibration
18.04.2019 18:17 ||I| [32mSuccessfully configured Board 0[0m
18.04.2019 18:17 ||I| [32mSuccessfully configured Cbc 0[0m
18.04.2019 18:17 ||I| [32mSuccessfully configured Cbc 1[0m
18.04.2019 18:17 ||I| [32mSuccessfully configured Cbc 2[0m
18.04.2019 18:17 ||I| [32mSuccessfully configured Cbc 3[0m
18.04.2019 18:17 ||I| [32mSuccessfully configured Cbc 4[0m
18.04.2019 18:17 ||I| [32mSuccessfully configured Cbc 5[0m
18.04.2019 18:17 ||I| [32mSuccessfully configured Cbc 6[0m
18.04.2019 18:17 ||I| [32mSuccessfully configured Cbc 7[0m
18.04.2019 18:17 ||I| 7
18.04.2019 18:17 ||I| Histo Map for CBC 0 (FE 0) does not exist - creating 
18.04.2019 18:17 ||I| Histo Map for CBC 1 (FE 0) does not exist - creating 
18.04.2019 18:17 ||I| Histo Map for CBC 2 (FE 0) does not exist - creating 
18.04.2019 18:17 ||I| Histo Map for CBC 3 (FE 0) does not exist - creating 
18.04.2019 18:17 ||I| Histo Map for CBC 4 (FE 0) does not exist - creating 
18.04.2019 18:17 ||I| Histo Map for CBC 5 (FE 0) does not exist - creating 
18.04.2019 18:17 ||I| Histo Map for CBC 6 (FE 0) does not exist - creating 
18.04.2019 18:17 ||I| Histo Map for CBC 7 (FE 0) does not exist - creating 
18.04.2019 18:17 ||I| Histo Map for Module 0 does not exist - creating 
18.04.2019 18:17 ||I| -186079512
18.04.2019 18:17 ||I| Parsed the following settings:
18.04.2019 18:17 ||I| 	Nevents = 100
18.04.2019 18:17 ||I| 	HoleMode = 0
18.04.2019 18:17 ||I| Histograms and Settings initialised.
18.04.2019 18:17 ||I| [1m[34mEnabling antenna with 613 written to the potentiometer[0m
Trigger source is from FC7, setting CS5 low
ADC SPI channel: 4
Hybrid Temperature = 34.00 deg C
Hybrid current = 840.7 mA
Amux Voltage = 130.7 mV
RDAC value passed to function is 613 - Equivalent to 487.88 mV.
18.04.2019 18:17 ||I| [1m[34mWant to set  DLL to 01110 -- 14[0m
18.04.2019 18:17 ||I| [1m[34mScanning latency for channel 1 of antenna.[0m
18.04.2019 18:17 ||I| [1m[34m	.....CBC0 - 89.8906 percent of channels connected to antenna contain a hit.[0m
18.04.2019 18:17 ||I| [1m[34m	.....CBC1 - 100 percent of channels connected to antenna contain a hit.[0m
18.04.2019 18:17 ||I| [1m[34m	.....CBC2 - 100 percent of channels connected to antenna contain a hit.[0m
18.04.2019 18:17 ||I| [1m[34m	.....CBC3 - 97.4126 percent of channels connected to antenna contain a hit.[0m
18.04.2019 18:17 ||I| [1m[34m	.....CBC4 - 100 percent of channels connected to antenna contain a hit.[0m
18.04.2019 18:17 ||I| [1m[34m	.....CBC5 - 99.5079 percent of channels connected to antenna contain a hit.[0m
18.04.2019 18:17 ||I| [1m[34m	.....CBC6 - 92.5156 percent of channels connected to antenna contain a hit.[0m
18.04.2019 18:17 ||I| [1m[34m	.....CBC7 - 88.2698 percent of channels connected to antenna contain a hit.[0m
18.04.2019 18:17 ||I| [1m[34m	.....CBC0 - 100 percent of channels connected to antenna contain a hit.[0m
18.04.2019 18:17 ||I| [1m[34m	.....CBC3 - 100 percent of channels connected to antenna contain a hit.[0m
18.04.2019 18:17 ||I| [1m[34m	.....CBC5 - 100 percent of channels connected to antenna contain a hit.[0m
18.04.2019 18:17 ||I| [1m[34m	.....CBC6 - 100 percent of channels connected to antenna contain a hit.[0m
18.04.2019 18:17 ||I| [1m[34m	.....CBC7 - 100 percent of channels connected to antenna contain a hit.[0m
18.04.2019 18:17 ||I| [1m[32mSummary of latency scan for FE0 [Antenna switched to group 1 ][0m
18.04.2019 18:17 ||I| [1m[32m	.....Optimal Latency found for CBC0: L1A = 53 , DLL = 14 - with an occupancy of 1[0m
18.04.2019 18:17 ||I| [1m[32m	.....Optimal Latency found for CBC1: L1A = 50 , DLL = 14 - with an occupancy of 1[0m
18.04.2019 18:17 ||I| [1m[32m	.....Optimal Latency found for CBC2: L1A = 50 , DLL = 14 - with an occupancy of 1[0m
18.04.2019 18:17 ||I| [1m[32m	.....Optimal Latency found for CBC3: L1A = 53 , DLL = 14 - with an occupancy of 1[0m
18.04.2019 18:17 ||I| [1m[32m	.....Optimal Latency found for CBC4: L1A = 50 , DLL = 14 - with an occupancy of 1[0m
18.04.2019 18:17 ||I| [1m[32m	.....Optimal Latency found for CBC5: L1A = 53 , DLL = 14 - with an occupancy of 1[0m
18.04.2019 18:17 ||I| [1m[32m	.....Optimal Latency found for CBC6: L1A = 53 , DLL = 14 - with an occupancy of 1[0m
18.04.2019 18:17 ||I| [1m[32m	.....Optimal Latency found for CBC7: L1A = 53 , DLL = 14 - with an occupancy of 1[0m
18.04.2019 18:17 ||I| [1m[34mScanning latency for channel 2 of antenna.[0m
18.04.2019 18:17 ||I| [1m[34m	.....CBC0 - 16.9683 percent of channels connected to antenna contain a hit.[0m
18.04.2019 18:17 ||I| [1m[34m	.....CBC1 - 58.4375 percent of channels connected to antenna contain a hit.[0m
18.04.2019 18:17 ||I| [1m[34m	.....CBC2 - 28.1746 percent of channels connected to antenna contain a hit.[0m
18.04.2019 18:17 ||I| [1m[34m	.....CBC3 - 6.90625 percent of channels connected to antenna contain a hit.[0m
18.04.2019 18:17 ||I| [1m[34m	.....CBC4 - 29 percent of channels connected to antenna contain a hit.[0m
18.04.2019 18:17 ||I| [1m[34m	.....CBC5 - 5.40625 percent of channels connected to antenna contain a hit.[0m
18.04.2019 18:17 ||I| [1m[34m	.....CBC6 - 0.396825 percent of channels connected to antenna contain a hit.[0m
18.04.2019 18:17 ||I| [1m[34m	.....CBC7 - 0.0625 percent of channels connected to antenna contain a hit.[0m
18.04.2019 18:17 ||I| [1m[34m	.....CBC0 - 100 percent of channels connected to antenna contain a hit.[0m
18.04.2019 18:17 ||I| [1m[34m	.....CBC1 - 100 percent of channels connected to antenna contain a hit.[0m
18.04.2019 18:17 ||I| [1m[34m	.....CBC2 - 100 percent of channels connected to antenna contain a hit.[0m
18.04.2019 18:17 ||I| [1m[34m	.....CBC3 - 100 percent of channels connected to antenna contain a hit.[0m
18.04.2019 18:17 ||I| [1m[34m	.....CBC4 - 100 percent of channels connected to antenna contain a hit.[0m
18.04.2019 18:17 ||I| [1m[34m	.....CBC5 - 100 percent of channels connected to antenna contain a hit.[0m
18.04.2019 18:17 ||I| [1m[34m	.....CBC6 - 100 percent of channels connected to antenna contain a hit.[0m
18.04.2019 18:17 ||I| [1m[34m	.....CBC7 - 100 percent of channels connected to antenna contain a hit.[0m
18.04.2019 18:17 ||I| [1m[32mSummary of latency scan for FE0 [Antenna switched to group 2 ][0m
18.04.2019 18:17 ||I| [1m[32m	.....Optimal Latency found for CBC0: L1A = 53 , DLL = 14 - with an occupancy of 1[0m
18.04.2019 18:17 ||I| [1m[32m	.....Optimal Latency found for CBC1: L1A = 53 , DLL = 14 - with an occupancy of 1[0m
18.04.2019 18:17 ||I| [1m[32m	.....Optimal Latency found for CBC2: L1A = 53 , DLL = 14 - with an occupancy of 1[0m
18.04.2019 18:17 ||I| [1m[32m	.....Optimal Latency found for CBC3: L1A = 53 , DLL = 14 - with an occupancy of 1[0m
18.04.2019 18:17 ||I| [1m[32m	.....Optimal Latency found for CBC4: L1A = 53 , DLL = 14 - with an occupancy of 1[0m
18.04.2019 18:17 ||I| [1m[32m	.....Optimal Latency found for CBC5: L1A = 53 , DLL = 14 - with an occupancy of 1[0m
18.04.2019 18:17 ||I| [1m[32m	.....Optimal Latency found for CBC6: L1A = 53 , DLL = 14 - with an occupancy of 1[0m
18.04.2019 18:17 ||I| [1m[32m	.....Optimal Latency found for CBC7: L1A = 53 , DLL = 14 - with an occupancy of 1[0m
18.04.2019 18:17 ||I| [1m[34mScanning latency for channel 3 of antenna.[0m
18.04.2019 18:17 ||I| [1m[34m	.....CBC0 - 43.4531 percent of channels connected to antenna contain a hit.[0m
18.04.2019 18:17 ||I| [1m[34m	.....CBC1 - 95.7301 percent of channels connected to antenna contain a hit.[0m
18.04.2019 18:17 ||I| [1m[34m	.....CBC2 - 89.8281 percent of channels connected to antenna contain a hit.[0m
18.04.2019 18:17 ||I| [1m[34m	.....CBC3 - 33.381 percent of channels connected to antenna contain a hit.[0m
18.04.2019 18:17 ||I| [1m[34m	.....CBC4 - 58.7344 percent of channels connected to antenna contain a hit.[0m
18.04.2019 18:17 ||I| [1m[34m	.....CBC5 - 1.39683 percent of channels connected to antenna contain a hit.[0m
18.04.2019 18:17 ||I| [1m[34m	.....CBC0 - 100 percent of channels connected to antenna contain a hit.[0m
18.04.2019 18:17 ||I| [1m[34m	.....CBC1 - 100 percent of channels connected to antenna contain a hit.[0m
18.04.2019 18:17 ||I| [1m[34m	.....CBC2 - 100 percent of channels connected to antenna contain a hit.[0m
18.04.2019 18:17 ||I| [1m[34m	.....CBC3 - 100 percent of channels connected to antenna contain a hit.[0m
18.04.2019 18:17 ||I| [1m[34m	.....CBC4 - 100 percent of channels connected to antenna contain a hit.[0m
18.04.2019 18:17 ||I| [1m[34m	.....CBC5 - 100 percent of channels connected to antenna contain a hit.[0m
18.04.2019 18:17 ||I| [1m[34m	.....CBC6 - 100 percent of channels connected to antenna contain a hit.[0m
18.04.2019 18:17 ||I| [1m[34m	.....CBC7 - 100 percent of channels connected to antenna contain a hit.[0m
18.04.2019 18:17 ||I| No data in the readout after receiving all triggers. Re-trying the point
18.04.2019 18:17 ||I| [1m[32mSummary of latency scan for FE0 [Antenna switched to group 3 ][0m
18.04.2019 18:17 ||I| [1m[32m	.....Optimal Latency found for CBC0: L1A = 53 , DLL = 14 - with an occupancy of 1[0m
18.04.2019 18:17 ||I| [1m[32m	.....Optimal Latency found for CBC1: L1A = 53 , DLL = 14 - with an occupancy of 1[0m
18.04.2019 18:17 ||I| [1m[32m	.....Optimal Latency found for CBC2: L1A = 53 , DLL = 14 - with an occupancy of 1[0m
18.04.2019 18:17 ||I| [1m[32m	.....Optimal Latency found for CBC3: L1A = 53 , DLL = 14 - with an occupancy of 1[0m
18.04.2019 18:17 ||I| [1m[32m	.....Optimal Latency found for CBC4: L1A = 53 , DLL = 14 - with an occupancy of 1[0m
18.04.2019 18:17 ||I| [1m[32m	.....Optimal Latency found for CBC5: L1A = 53 , DLL = 14 - with an occupancy of 1[0m
18.04.2019 18:17 ||I| [1m[32m	.....Optimal Latency found for CBC6: L1A = 53 , DLL = 14 - with an occupancy of 1[0m
18.04.2019 18:17 ||I| [1m[32m	.....Optimal Latency found for CBC7: L1A = 53 , DLL = 14 - with an occupancy of 1[0m
18.04.2019 18:17 ||I| [1m[34mScanning latency for channel 4 of antenna.[0m
18.04.2019 18:17 ||I| [1m[34m	.....CBC0 - 29.6667 percent of channels connected to antenna contain a hit.[0m
18.04.2019 18:17 ||I| [1m[34m	.....CBC1 - 91.2188 percent of channels connected to antenna contain a hit.[0m
18.04.2019 18:17 ||I| [1m[34m	.....CBC2 - 35.5873 percent of channels connected to antenna contain a hit.[0m
18.04.2019 18:17 ||I| [1m[34m	.....CBC3 - 7.90625 percent of channels connected to antenna contain a hit.[0m
18.04.2019 18:17 ||I| [1m[34m	.....CBC4 - 12.2064 percent of channels connected to antenna contain a hit.[0m
18.04.2019 18:17 ||I| [1m[34m	.....CBC5 - 0.015625 percent of channels connected to antenna contain a hit.[0m
18.04.2019 18:17 ||I| [1m[34m	.....CBC0 - 100 percent of channels connected to antenna contain a hit.[0m
18.04.2019 18:17 ||I| [1m[34m	.....CBC1 - 100 percent of channels connected to antenna contain a hit.[0m
18.04.2019 18:17 ||I| [1m[34m	.....CBC2 - 100 percent of channels connected to antenna contain a hit.[0m
18.04.2019 18:17 ||I| [1m[34m	.....CBC3 - 100 percent of channels connected to antenna contain a hit.[0m
18.04.2019 18:17 ||I| [1m[34m	.....CBC4 - 100 percent of channels connected to antenna contain a hit.[0m
18.04.2019 18:17 ||I| [1m[34m	.....CBC5 - 100 percent of channels connected to antenna contain a hit.[0m
18.04.2019 18:17 ||I| [1m[34m	.....CBC6 - 100 percent of channels connected to antenna contain a hit.[0m
18.04.2019 18:17 ||I| [1m[34m	.....CBC7 - 100 percent of channels connected to antenna contain a hit.[0m
18.04.2019 18:17 ||I| [1m[32mSummary of latency scan for FE0 [Antenna switched to group 4 ][0m
18.04.2019 18:17 ||I| [1m[32m	.....Optimal Latency found for CBC0: L1A = 53 , DLL = 14 - with an occupancy of 1[0m
18.04.2019 18:17 ||I| [1m[32m	.....Optimal Latency found for CBC1: L1A = 53 , DLL = 14 - with an occupancy of 1[0m
18.04.2019 18:17 ||I| [1m[32m	.....Optimal Latency found for CBC2: L1A = 53 , DLL = 14 - with an occupancy of 1[0m
18.04.2019 18:17 ||I| [1m[32m	.....Optimal Latency found for CBC3: L1A = 53 , DLL = 14 - with an occupancy of 1[0m
18.04.2019 18:17 ||I| [1m[32m	.....Optimal Latency found for CBC4: L1A = 53 , DLL = 14 - with an occupancy of 1[0m
18.04.2019 18:17 ||I| [1m[32m	.....Optimal Latency found for CBC5: L1A = 53 , DLL = 14 - with an occupancy of 1[0m
18.04.2019 18:17 ||I| [1m[32m	.....Optimal Latency found for CBC6: L1A = 53 , DLL = 14 - with an occupancy of 1[0m
18.04.2019 18:17 ||I| [1m[32m	.....Optimal Latency found for CBC7: L1A = 53 , DLL = 14 - with an occupancy of 1[0m
Antenna CS1 is low
18.04.2019 18:17 ||I| [1m[34mDisable antenna with 613 written to the potentiometer[0m
18.04.2019 18:17 ||I| [1m[34mConfiguring antenna...[0m
Trigger source is from FC7, setting CS5 low
Setting of CS_Line   -75 15
ADC SPI channel: 4
Hybrid Temperature = 34.00 deg C
Hybrid current = 841.9 mA
Amux Voltage = 130.7 mV
RDAC value passed to function is 613 - Equivalent to 487.88 mV.
18.04.2019 18:17 ||I| Chip Select ID 0
18.04.2019 18:17 ||I| [1m[34mDisabling all channels on the antenna.[0m
Antenna CS1 is low
Info in <TCanvas::Print>: pdf file Results/Commissioning_Electron_18-04-19_18h17/c_online_canvas_fe0.pdf has been created
18.04.2019 18:17 ||I| Results saved!
18.04.2019 18:17 ||I| Results saved!
18.04.2019 18:17 ||I| [1m[31mclosing result file![0m
18.04.2019 18:17 ||I| [1m[31mDestroying memory objects[0m
User comment: 
