08.06.2019 09:20 ||I| 


********************************************************************************
                                        [1m[31mHW SUMMARY: [0m
********************************************************************************

[1m[36m|----BeBoard  Id :[1m[34m0[1m[36m BoardType: [1m[34mD19C[1m[36m EventType: [1m[31mVR[0m
[1m[34m|       |----Board Id:      [1m[33mboard
[1m[34m|       |----URI:           [1m[33mchtcp-2.0://localhost:10203?target=192.168.0.10:50001
[1m[34m|       |----Address Table: [1m[33mfile://settings/address_tables/d19c_address_table.xml
[1m[34m|       |[0m
[34m|	|----Register  clock_source: [1m[33m3[0m
[34m|	|----Register  fc7_daq_cnfg.clock.ext_clk_en: [1m[33m0[0m
[34m|	|----Register  fc7_daq_cnfg.ttc.ttc_enable: [1m[33m0[0m
[34m|	|----Register  fc7_daq_cnfg.fast_command_block.triggers_to_accept: [1m[33m0[0m
[34m|	|----Register  fc7_daq_cnfg.fast_command_block.trigger_source: [1m[33m7[0m
[34m|	|----Register  fc7_daq_cnfg.fast_command_block.user_trigger_frequency: [1m[33m100[0m
[34m|	|----Register  fc7_daq_cnfg.fast_command_block.stubs_mask: [1m[33m1[0m
[34m|	|----Register  fc7_daq_cnfg.fast_command_block.stub_trigger_delay_value: [1m[33m0[0m
[34m|	|----Register  fc7_daq_cnfg.fast_command_block.stub_trigger_veto_length: [1m[33m0[0m
[34m|	|----Register  fc7_daq_cnfg.fast_command_block.test_pulse.delay_after_fast_reset: [1m[33m2[0m
[34m|	|----Register  fc7_daq_cnfg.fast_command_block.test_pulse.delay_after_test_pulse: [1m[33m20[0m
[34m|	|----Register  fc7_daq_cnfg.fast_command_block.test_pulse.delay_before_next_pulse: [1m[33m700[0m
[34m|	|----Register  fc7_daq_cnfg.fast_command_block.test_pulse.en_fast_reset: [1m[33m1[0m
[34m|	|----Register  fc7_daq_cnfg.fast_command_block.test_pulse.en_test_pulse: [1m[33m1[0m
[34m|	|----Register  fc7_daq_cnfg.fast_command_block.test_pulse.en_l1a: [1m[33m1[0m
[34m|	|----Register  fc7_daq_cnfg.fast_command_block.ext_trigger_delay_value: [1m[33m50[0m
[34m|	|----Register  fc7_daq_cnfg.fast_command_block.antenna_trigger_delay_value: [1m[33m50[0m
[34m|	|----Register  fc7_daq_cnfg.fast_command_block.delay_between_two_consecutive: [1m[33m10[0m
[34m|	|----Register  fc7_daq_cnfg.fast_command_block.misc.backpressure_enable: [1m[33m1[0m
[34m|	|----Register  fc7_daq_cnfg.fast_command_block.misc.stubOR: [1m[33m1[0m
[34m|	|----Register  fc7_daq_cnfg.fast_command_block.misc.initial_fast_reset_enable: [1m[33m0[0m
[34m|	|----Register  fc7_daq_cnfg.physical_interface_block.i2c.frequency: [1m[33m4[0m
[34m|	|----Register  fc7_daq_cnfg.readout_block.packet_nbr: [1m[33m99[0m
[34m|	|----Register  fc7_daq_cnfg.readout_block.global.data_handshake_enable: [1m[33m1[0m
[34m|	|----Register  fc7_daq_cnfg.readout_block.global.int_trig_enable: [1m[33m0[0m
[34m|	|----Register  fc7_daq_cnfg.readout_block.global.int_trig_rate: [1m[33m0[0m
[34m|	|----Register  fc7_daq_cnfg.readout_block.global.trigger_type: [1m[33m0[0m
[34m|	|----Register  fc7_daq_cnfg.readout_block.global.data_type: [1m[33m0[0m
[34m|	|----Register  fc7_daq_cnfg.readout_block.global.common_stubdata_delay: [1m[33m5[0m
[34m|	|----Register  fc7_daq_cnfg.dio5_block.dio5_en: [1m[33m0[0m
[34m|	|----Register  fc7_daq_cnfg.dio5_block.ch1.out_enable: [1m[33m1[0m
[34m|	|----Register  fc7_daq_cnfg.dio5_block.ch1.term_enable: [1m[33m0[0m
[34m|	|----Register  fc7_daq_cnfg.dio5_block.ch1.threshold: [1m[33m0[0m
[34m|	|----Register  fc7_daq_cnfg.dio5_block.ch2.out_enable: [1m[33m0[0m
[34m|	|----Register  fc7_daq_cnfg.dio5_block.ch2.term_enable: [1m[33m1[0m
[34m|	|----Register  fc7_daq_cnfg.dio5_block.ch2.threshold: [1m[33m50[0m
[34m|	|----Register  fc7_daq_cnfg.dio5_block.ch3.out_enable: [1m[33m1[0m
[34m|	|----Register  fc7_daq_cnfg.dio5_block.ch3.term_enable: [1m[33m0[0m
[34m|	|----Register  fc7_daq_cnfg.dio5_block.ch3.threshold: [1m[33m0[0m
[34m|	|----Register  fc7_daq_cnfg.dio5_block.ch4.out_enable: [1m[33m0[0m
[34m|	|----Register  fc7_daq_cnfg.dio5_block.ch4.term_enable: [1m[33m1[0m
[34m|	|----Register  fc7_daq_cnfg.dio5_block.ch4.threshold: [1m[33m50[0m
[34m|	|----Register  fc7_daq_cnfg.dio5_block.ch5.out_enable: [1m[33m0[0m
[34m|	|----Register  fc7_daq_cnfg.dio5_block.ch5.term_enable: [1m[33m1[0m
[34m|	|----Register  fc7_daq_cnfg.dio5_block.ch5.threshold: [1m[33m50[0m
[34m|	|----Register  fc7_daq_cnfg.tlu_block.handshake_mode: [1m[33m2[0m
[34m|	|----Register  fc7_daq_cnfg.tlu_block.tlu_enabled: [1m[33m0[0m
[34m|	|[0m
[1m[36m|	|----Module  FeId :0[0m
[32m|	|	|----CBC Files Path : hybrid_functional_tests/8CBC318-201000071/calibration/Calibration_Electron_08-06-19_09h19/[0m
[1m[36m|	|	|----CBC  Id :0, File: FE0CBC0.txt[0m
[32m|	|	|	|----ChipType: [31mCBC3[0m
[1m[36m|	|	|----CBC  Id :1, File: FE0CBC1.txt[0m
[32m|	|	|	|----ChipType: [31mCBC3[0m
[1m[36m|	|	|----CBC  Id :2, File: FE0CBC2.txt[0m
[32m|	|	|	|----ChipType: [31mCBC3[0m
[1m[36m|	|	|----CBC  Id :3, File: FE0CBC3.txt[0m
[32m|	|	|	|----ChipType: [31mCBC3[0m
[1m[36m|	|	|----CBC  Id :4, File: FE0CBC4.txt[0m
[32m|	|	|	|----ChipType: [31mCBC3[0m
[1m[36m|	|	|----CBC  Id :5, File: FE0CBC5.txt[0m
[32m|	|	|	|----ChipType: [31mCBC3[0m
[1m[36m|	|	|----CBC  Id :6, File: FE0CBC6.txt[0m
[32m|	|	|	|----ChipType: [31mCBC3[0m
[1m[36m|	|	|----CBC  Id :7, File: FE0CBC7.txt[0m
[32m|	|	|	|----ChipType: [31mCBC3[0m
[1m[36m|	|	|----Global CBC Settings: [0m
[32m|	|	|	|----ChipType: [31mCBC3[0m
[32m|	|	|	|----VCth: [31m0x23a (570)[0m
[32m|	|	|	|----TriggerLatency: [31m0x13 (19)[0m
[32m|	|	|	|----TestPulse: enabled: [31m0[32m, polarity: [31m0[32m, amplitude: [31m128[32m (0x80)[0m
[32m|	|	|	|               channelgroup: [31m0[32m, delay: [31m0[32m, groundohters: [31m1[0m
[32m|	|	|	|----Cluster & Stub Logic: ClusterWidthDiscrimination: [31m4[32m, PtWidth: [31m14[32m, Layerswap: [31m0[0m
[32m|	|	|	|                          Offset1: [31m0[32m, Offset2: [31m0[32m, Offset3: [31m0[32m, Offset4: [31m0[0m
[32m|	|	|	|----Misc Settings:  PipelineLogicSource: [31m0[32m, StubLogicSource: [31m1[32m, OR254: [31m0[32m, TPG Clock: [31m1[32m, Test Clock 40: [31m1[32m, DLL: [31m21[0m
[32m|	|	|	|----Analog Mux value: [31m0 (0x0, 0b00000)[0m
[32m|	|	|	|----List of disabled Channels: [0m
[34m|	|
|	|----SLink[0m
[34m|	|       |----DebugMode[35m : SLinkDebugMode::FULL[0m
[34m|	|       |----ConditionData: Type [31mI2C VCth1[34m, UID [31m1[34m, FeId [31m0[34m, CbcId [31m0[34m, Page [31m0[34m, Address [31m4f[34m, Value [35m58[0m
[34m|	|       |----ConditionData: Type [31mUser [34m, UID [31m128[34m, FeId [31m0[34m, CbcId [31m0[34m, Page [31m0[34m, Address [31m0[34m, Value [35m34[0m
[34m|	|       |----ConditionData: Type [31mHV [34m, UID [31m5[34m, FeId [31m0[34m, CbcId [31m2[34m, Page [31m0[34m, Address [31m0[34m, Value [35m250[0m
[34m|	|       |----ConditionData: Type [31mTDC [34m, UID [31m3[34m, FeId [31m255[34m, CbcId [31m0[34m, Page [31m0[34m, Address [31m0[34m, Value [35m0[0m


********************************************************************************
                                        [1m[31mEND OF HW SUMMARY: [0m
********************************************************************************


[31mSetting[0m --[1m[36mTargetVcth[0m:[1m[33m120[0m
[31mSetting[0m --[1m[36mTargetOffset[0m:[1m[33m80[0m
[31mSetting[0m --[1m[36mNevents[0m:[1m[33m100[0m
[31mSetting[0m --[1m[36mTestPulsePotentiometer[0m:[1m[33m0[0m
[31mSetting[0m --[1m[36mHoleMode[0m:[1m[33m0[0m
[31mSetting[0m --[1m[36mVerificationLoop[0m:[1m[33m0[0m
[31mSetting[0m --[1m[36mInitialVcth[0m:[1m[33m120[0m
[31mSetting[0m --[1m[36mSignalScanStep[0m:[1m[33m2[0m
[31mSetting[0m --[1m[36mFitSignal[0m:[1m[33m0[0m

08.06.2019 09:20 ||I| Creating directory: Results/IntegratedTester_Electron_08-06-19_09h20
Info in <TCivetweb::Create>: Starting HTTP server on port 8080
08.06.2019 09:20 ||I| Opening THttpServer on port 8080. Point your browser to: [1m[32mcmsuptkhsetup1.dyndns.cern.ch:8080[0m
08.06.2019 09:20 ||I| [1m[34mConfiguring HW parsed from .xml file: [0m
08.06.2019 09:20 ||I| [1m[32mSetting the I2C address table[0m
08.06.2019 09:20 ||I| [1m[32mAccording to the Firmware status registers, it was compiled for: 1 hybrid(s), 8 CBC3 chip(s) per hybrid[0m
08.06.2019 09:20 ||I| Enabling Hybrid 0
08.06.2019 09:20 ||I|      Enabling Chip 0
08.06.2019 09:20 ||I|      Enabling Chip 1
08.06.2019 09:20 ||I|      Enabling Chip 2
08.06.2019 09:20 ||I|      Enabling Chip 3
08.06.2019 09:20 ||I|      Enabling Chip 4
08.06.2019 09:20 ||I|      Enabling Chip 5
08.06.2019 09:20 ||I|      Enabling Chip 6
08.06.2019 09:20 ||I|      Enabling Chip 7
08.06.2019 09:20 ||I| [1m[32m1 hybrid(s) was(were) enabled with the total amount of 8 chip(s)![0m
Reply from chip 0: 2000007c
Reply from chip 1: 2004007c
Reply from chip 2: 2008007c
Reply from chip 3: 200c007c
Reply from chip 4: 2010007c
Reply from chip 5: 2014007c
Reply from chip 6: 2018007c
Reply from chip 7: 201c007c
08.06.2019 09:20 ||I| Successfully received *Pings* from 8 Cbcs
08.06.2019 09:20 ||I| [32mCBC3 Phase tuning finished succesfully[0m
08.06.2019 09:20 ||I| [1m[30m	 Hybrid: [0m0[1m[30m, Chip: [0m0[1m[30m, Line: [0m5
08.06.2019 09:20 ||I| 		 Mode: 0
08.06.2019 09:20 ||I| 		 Manual Delay: 0, Manual Bitslip: 0
08.06.2019 09:20 ||I| 		 Done: 1, PA FSM: [1m[32mTunedPHASE[0m, WA FSM: [1m[32mTunedWORD[0m
08.06.2019 09:20 ||I| 		 Delay: 15, Bitslip: 3
08.06.2019 09:20 ||I| [1m[30m	 Hybrid: [0m0[1m[30m, Chip: [0m1[1m[30m, Line: [0m5
08.06.2019 09:20 ||I| 		 Mode: 0
08.06.2019 09:20 ||I| 		 Manual Delay: 0, Manual Bitslip: 0
08.06.2019 09:20 ||I| 		 Done: 1, PA FSM: [1m[32mTunedPHASE[0m, WA FSM: [1m[32mTunedWORD[0m
08.06.2019 09:20 ||I| 		 Delay: 15, Bitslip: 3
08.06.2019 09:20 ||I| [1m[30m	 Hybrid: [0m0[1m[30m, Chip: [0m2[1m[30m, Line: [0m5
08.06.2019 09:20 ||I| 		 Mode: 0
08.06.2019 09:20 ||I| 		 Manual Delay: 0, Manual Bitslip: 0
08.06.2019 09:20 ||I| 		 Done: 1, PA FSM: [1m[32mTunedPHASE[0m, WA FSM: [1m[32mTunedWORD[0m
08.06.2019 09:20 ||I| 		 Delay: 15, Bitslip: 3
08.06.2019 09:20 ||I| [1m[30m	 Hybrid: [0m0[1m[30m, Chip: [0m3[1m[30m, Line: [0m5
08.06.2019 09:20 ||I| 		 Mode: 0
08.06.2019 09:20 ||I| 		 Manual Delay: 0, Manual Bitslip: 0
08.06.2019 09:20 ||I| 		 Done: 1, PA FSM: [1m[32mTunedPHASE[0m, WA FSM: [1m[32mTunedWORD[0m
08.06.2019 09:20 ||I| 		 Delay: 15, Bitslip: 3
08.06.2019 09:20 ||I| [1m[30m	 Hybrid: [0m0[1m[30m, Chip: [0m4[1m[30m, Line: [0m5
08.06.2019 09:20 ||I| 		 Mode: 0
08.06.2019 09:20 ||I| 		 Manual Delay: 0, Manual Bitslip: 0
08.06.2019 09:20 ||I| 		 Done: 1, PA FSM: [1m[32mTunedPHASE[0m, WA FSM: [1m[32mTunedWORD[0m
08.06.2019 09:20 ||I| 		 Delay: 15, Bitslip: 3
08.06.2019 09:20 ||I| [1m[30m	 Hybrid: [0m0[1m[30m, Chip: [0m5[1m[30m, Line: [0m5
08.06.2019 09:20 ||I| 		 Mode: 0
08.06.2019 09:20 ||I| 		 Manual Delay: 0, Manual Bitslip: 0
08.06.2019 09:20 ||I| 		 Done: 1, PA FSM: [1m[32mTunedPHASE[0m, WA FSM: [1m[32mTunedWORD[0m
08.06.2019 09:20 ||I| 		 Delay: 15, Bitslip: 3
08.06.2019 09:20 ||I| [1m[30m	 Hybrid: [0m0[1m[30m, Chip: [0m6[1m[30m, Line: [0m5
08.06.2019 09:20 ||I| 		 Mode: 0
08.06.2019 09:20 ||I| 		 Manual Delay: 0, Manual Bitslip: 0
08.06.2019 09:20 ||I| 		 Done: 1, PA FSM: [1m[32mTunedPHASE[0m, WA FSM: [1m[32mTunedWORD[0m
08.06.2019 09:20 ||I| 		 Delay: 15, Bitslip: 3
08.06.2019 09:20 ||I| [1m[30m	 Hybrid: [0m0[1m[30m, Chip: [0m7[1m[30m, Line: [0m5
08.06.2019 09:20 ||I| 		 Mode: 0
08.06.2019 09:20 ||I| 		 Manual Delay: 0, Manual Bitslip: 0
08.06.2019 09:20 ||I| 		 Done: 1, PA FSM: [1m[32mTunedPHASE[0m, WA FSM: [1m[32mTunedWORD[0m
08.06.2019 09:20 ||I| 		 Delay: 15, Bitslip: 3
08.06.2019 09:20 ||I| Waiting for DDR3 to finish initial calibration
08.06.2019 09:20 ||I| [32mSuccessfully configured Board 0[0m
08.06.2019 09:20 ||I| [32mSuccessfully configured Cbc 0[0m
08.06.2019 09:20 ||I| [32mSuccessfully configured Cbc 1[0m
08.06.2019 09:20 ||I| [32mSuccessfully configured Cbc 2[0m
08.06.2019 09:20 ||I| [32mSuccessfully configured Cbc 3[0m
08.06.2019 09:20 ||I| [32mSuccessfully configured Cbc 4[0m
08.06.2019 09:20 ||I| [32mSuccessfully configured Cbc 5[0m
08.06.2019 09:20 ||I| [32mSuccessfully configured Cbc 6[0m
08.06.2019 09:20 ||I| [32mSuccessfully configured Cbc 7[0m
08.06.2019 09:20 ||I| [32mCalibrating CBCs before starting antenna test of the CBCs on the DUT.[0m
08.06.2019 09:20 ||I| [1m[34mChip Type = CBC3 - thus disabling Stub logic for offset tuning[0m
08.06.2019 09:20 ||I| [1m[34m CHIP ID FUSE 464644[0m
08.06.2019 09:20 ||I| Histo Map for CBC 0 (FE 0) does not exist - creating 
08.06.2019 09:20 ||I| [1m[34mChip Type = CBC3 - thus disabling Stub logic for offset tuning[0m
08.06.2019 09:20 ||I| [1m[34m CHIP ID FUSE 399093[0m
08.06.2019 09:20 ||I| Histo Map for CBC 1 (FE 0) does not exist - creating 
08.06.2019 09:20 ||I| [1m[34mChip Type = CBC3 - thus disabling Stub logic for offset tuning[0m
08.06.2019 09:20 ||I| [1m[34m CHIP ID FUSE 398989[0m
08.06.2019 09:20 ||I| Histo Map for CBC 2 (FE 0) does not exist - creating 
08.06.2019 09:20 ||I| [1m[34mChip Type = CBC3 - thus disabling Stub logic for offset tuning[0m
08.06.2019 09:20 ||I| [1m[34m CHIP ID FUSE 398979[0m
08.06.2019 09:20 ||I| Histo Map for CBC 3 (FE 0) does not exist - creating 
08.06.2019 09:20 ||I| [1m[34mChip Type = CBC3 - thus disabling Stub logic for offset tuning[0m
08.06.2019 09:20 ||I| [1m[34m CHIP ID FUSE 398968[0m
08.06.2019 09:20 ||I| Histo Map for CBC 4 (FE 0) does not exist - creating 
08.06.2019 09:20 ||I| [1m[34mChip Type = CBC3 - thus disabling Stub logic for offset tuning[0m
08.06.2019 09:20 ||I| [1m[34m CHIP ID FUSE 398955[0m
08.06.2019 09:20 ||I| Histo Map for CBC 5 (FE 0) does not exist - creating 
08.06.2019 09:20 ||I| [1m[34mChip Type = CBC3 - thus disabling Stub logic for offset tuning[0m
08.06.2019 09:20 ||I| [1m[34m CHIP ID FUSE 398942[0m
08.06.2019 09:20 ||I| Histo Map for CBC 6 (FE 0) does not exist - creating 
08.06.2019 09:20 ||I| [1m[34mChip Type = CBC3 - thus disabling Stub logic for offset tuning[0m
08.06.2019 09:20 ||I| [1m[34m CHIP ID FUSE 398927[0m
08.06.2019 09:20 ||I| Histo Map for CBC 7 (FE 0) does not exist - creating 
08.06.2019 09:20 ||I| Created Object Maps and parsed settings:
08.06.2019 09:20 ||I| 	Nevents = 100
08.06.2019 09:20 ||I| 	TestPulseAmplitude = 0
08.06.2019 09:20 ||I|   Target Vcth determined algorithmically for CBC3
08.06.2019 09:20 ||I|   Target Offset fixed to half range (0x80) for CBC3
08.06.2019 09:20 ||I| [1m[34mExtracting Target VCth ...[0m
08.06.2019 09:20 ||I| Disabling all channels by setting offsets to 0xff
08.06.2019 09:20 ||I| Setting offsets of Test Group -1 to 0xff
08.06.2019 09:20 ||I| [32mEnabling Test Group....-1[0m
08.06.2019 09:20 ||I| Setting offsets of Test Group -1 to 0x80
08.06.2019 09:20 ||I| [31mDisabling Test Group....-1[0m
08.06.2019 09:20 ||I| Setting offsets of Test Group -1 to 0xff
08.06.2019 09:20 ||I| [1m[32mMean VCth value for FE 0 CBC 0 is [1m[31m598[0m
08.06.2019 09:20 ||I| [1m[32mMean VCth value for FE 0 CBC 1 is [1m[31m589[0m
08.06.2019 09:20 ||I| [1m[32mMean VCth value for FE 0 CBC 2 is [1m[31m592[0m
08.06.2019 09:20 ||I| [1m[32mMean VCth value for FE 0 CBC 3 is [1m[31m582[0m
08.06.2019 09:20 ||I| [1m[32mMean VCth value for FE 0 CBC 4 is [1m[31m587[0m
08.06.2019 09:20 ||I| [1m[32mMean VCth value for FE 0 CBC 5 is [1m[31m596[0m
08.06.2019 09:20 ||I| [1m[32mMean VCth value for FE 0 CBC 6 is [1m[31m597[0m
08.06.2019 09:20 ||I| [1m[32mMean VCth value for FE 0 CBC 7 is [1m[31m583[0m
08.06.2019 09:20 ||I| [1m[34mMean VCth value of all chips is 590 - using as TargetVcth value for all chips![0m
08.06.2019 09:20 ||I| [32mEnabling Test Group....0[0m
08.06.2019 09:20 ||I| Setting offsets of Test Group 0 to 0xff
08.06.2019 09:20 ||I| [31mDisabling Test Group....0[0m
08.06.2019 09:20 ||I| [32mEnabling Test Group....1[0m
08.06.2019 09:20 ||I| Setting offsets of Test Group 1 to 0xff
08.06.2019 09:20 ||I| [31mDisabling Test Group....1[0m
08.06.2019 09:20 ||I| [32mEnabling Test Group....2[0m
08.06.2019 09:20 ||I| Setting offsets of Test Group 2 to 0xff
08.06.2019 09:20 ||I| [31mDisabling Test Group....2[0m
08.06.2019 09:20 ||I| [32mEnabling Test Group....3[0m
08.06.2019 09:20 ||I| Setting offsets of Test Group 3 to 0xff
08.06.2019 09:20 ||I| [31mDisabling Test Group....3[0m
08.06.2019 09:20 ||I| [32mEnabling Test Group....4[0m
08.06.2019 09:20 ||I| Setting offsets of Test Group 4 to 0xff
08.06.2019 09:20 ||I| [31mDisabling Test Group....4[0m
08.06.2019 09:20 ||I| [32mEnabling Test Group....5[0m
08.06.2019 09:20 ||I| Setting offsets of Test Group 5 to 0xff
08.06.2019 09:20 ||I| [31mDisabling Test Group....5[0m
08.06.2019 09:20 ||I| [32mEnabling Test Group....6[0m
08.06.2019 09:20 ||I| Setting offsets of Test Group 6 to 0xff
08.06.2019 09:20 ||I| [31mDisabling Test Group....6[0m
08.06.2019 09:20 ||I| [32mEnabling Test Group....7[0m
08.06.2019 09:20 ||I| Setting offsets of Test Group 7 to 0xff
08.06.2019 09:20 ||I| [31mDisabling Test Group....7[0m
08.06.2019 09:20 ||I| [1m[34mChip Type = CBC3 - re-enabling stub logic to original value![0m
08.06.2019 09:20 ||I| [1m[34mChip Type = CBC3 - re-enabling stub logic to original value![0m
08.06.2019 09:20 ||I| [1m[34mChip Type = CBC3 - re-enabling stub logic to original value![0m
08.06.2019 09:20 ||I| [1m[34mChip Type = CBC3 - re-enabling stub logic to original value![0m
08.06.2019 09:20 ||I| [1m[34mChip Type = CBC3 - re-enabling stub logic to original value![0m
08.06.2019 09:20 ||I| [1m[34mChip Type = CBC3 - re-enabling stub logic to original value![0m
08.06.2019 09:20 ||I| [1m[34mChip Type = CBC3 - re-enabling stub logic to original value![0m
08.06.2019 09:20 ||I| [1m[34mChip Type = CBC3 - re-enabling stub logic to original value![0m
08.06.2019 09:20 ||I| [1m[32mApplying final offsets determined by tuning to chip - no re-configure necessary![0m
08.06.2019 09:20 ||I| Results saved!
08.06.2019 09:20 ||I| [1m[34mConfigfiles for all Cbcs written to Results/IntegratedTester_Electron_08-06-19_09h20[0m
08.06.2019 09:20 ||I| Calibration finished.
Calibration of the DUT finished at: Sat Jun  8 09:20:32 2019
	elapsed time: 9.83752 seconds
08.06.2019 09:20 ||I| Starting noise occupancy test.
08.06.2019 09:20 ||I| [1m[34mConfiguring HW parsed from .xml file: [0m
08.06.2019 09:20 ||I| [1m[32mSetting the I2C address table[0m
08.06.2019 09:20 ||I| [1m[32mAccording to the Firmware status registers, it was compiled for: 1 hybrid(s), 8 CBC3 chip(s) per hybrid[0m
08.06.2019 09:20 ||I| Enabling Hybrid 0
08.06.2019 09:20 ||I|      Enabling Chip 0
08.06.2019 09:20 ||I|      Enabling Chip 1
08.06.2019 09:20 ||I|      Enabling Chip 2
08.06.2019 09:20 ||I|      Enabling Chip 3
08.06.2019 09:20 ||I|      Enabling Chip 4
08.06.2019 09:20 ||I|      Enabling Chip 5
08.06.2019 09:20 ||I|      Enabling Chip 6
08.06.2019 09:20 ||I|      Enabling Chip 7
08.06.2019 09:20 ||I| [1m[32m1 hybrid(s) was(were) enabled with the total amount of 8 chip(s)![0m
Reply from chip 0: 2000007c
Reply from chip 1: 2004007c
Reply from chip 2: 2008007c
Reply from chip 3: 200c007c
Reply from chip 4: 2010007c
Reply from chip 5: 2014007c
Reply from chip 6: 2018007c
Reply from chip 7: 201c007c
08.06.2019 09:20 ||I| Successfully received *Pings* from 8 Cbcs
08.06.2019 09:20 ||I| [32mCBC3 Phase tuning finished succesfully[0m
08.06.2019 09:20 ||I| [1m[30m	 Hybrid: [0m0[1m[30m, Chip: [0m0[1m[30m, Line: [0m5
08.06.2019 09:20 ||I| 		 Mode: 0
08.06.2019 09:20 ||I| 		 Manual Delay: 0, Manual Bitslip: 0
08.06.2019 09:20 ||I| 		 Done: 1, PA FSM: [1m[32mTunedPHASE[0m, WA FSM: [1m[32mTunedWORD[0m
08.06.2019 09:20 ||I| 		 Delay: 15, Bitslip: 3
08.06.2019 09:20 ||I| [1m[30m	 Hybrid: [0m0[1m[30m, Chip: [0m1[1m[30m, Line: [0m5
08.06.2019 09:20 ||I| 		 Mode: 0
08.06.2019 09:20 ||I| 		 Manual Delay: 0, Manual Bitslip: 0
08.06.2019 09:20 ||I| 		 Done: 1, PA FSM: [1m[32mTunedPHASE[0m, WA FSM: [1m[32mTunedWORD[0m
08.06.2019 09:20 ||I| 		 Delay: 15, Bitslip: 3
08.06.2019 09:20 ||I| [1m[30m	 Hybrid: [0m0[1m[30m, Chip: [0m2[1m[30m, Line: [0m5
08.06.2019 09:20 ||I| 		 Mode: 0
08.06.2019 09:20 ||I| 		 Manual Delay: 0, Manual Bitslip: 0
08.06.2019 09:20 ||I| 		 Done: 1, PA FSM: [1m[32mTunedPHASE[0m, WA FSM: [1m[32mTunedWORD[0m
08.06.2019 09:20 ||I| 		 Delay: 15, Bitslip: 3
08.06.2019 09:20 ||I| [1m[30m	 Hybrid: [0m0[1m[30m, Chip: [0m3[1m[30m, Line: [0m5
08.06.2019 09:20 ||I| 		 Mode: 0
08.06.2019 09:20 ||I| 		 Manual Delay: 0, Manual Bitslip: 0
08.06.2019 09:20 ||I| 		 Done: 1, PA FSM: [1m[32mTunedPHASE[0m, WA FSM: [1m[32mTunedWORD[0m
08.06.2019 09:20 ||I| 		 Delay: 15, Bitslip: 3
08.06.2019 09:20 ||I| [1m[30m	 Hybrid: [0m0[1m[30m, Chip: [0m4[1m[30m, Line: [0m5
08.06.2019 09:20 ||I| 		 Mode: 0
08.06.2019 09:20 ||I| 		 Manual Delay: 0, Manual Bitslip: 0
08.06.2019 09:20 ||I| 		 Done: 1, PA FSM: [1m[32mTunedPHASE[0m, WA FSM: [1m[32mTunedWORD[0m
08.06.2019 09:20 ||I| 		 Delay: 15, Bitslip: 3
08.06.2019 09:20 ||I| [1m[30m	 Hybrid: [0m0[1m[30m, Chip: [0m5[1m[30m, Line: [0m5
08.06.2019 09:20 ||I| 		 Mode: 0
08.06.2019 09:20 ||I| 		 Manual Delay: 0, Manual Bitslip: 0
08.06.2019 09:20 ||I| 		 Done: 1, PA FSM: [1m[32mTunedPHASE[0m, WA FSM: [1m[32mTunedWORD[0m
08.06.2019 09:20 ||I| 		 Delay: 15, Bitslip: 3
08.06.2019 09:20 ||I| [1m[30m	 Hybrid: [0m0[1m[30m, Chip: [0m6[1m[30m, Line: [0m5
08.06.2019 09:20 ||I| 		 Mode: 0
08.06.2019 09:20 ||I| 		 Manual Delay: 0, Manual Bitslip: 0
08.06.2019 09:20 ||I| 		 Done: 1, PA FSM: [1m[32mTunedPHASE[0m, WA FSM: [1m[32mTunedWORD[0m
08.06.2019 09:20 ||I| 		 Delay: 15, Bitslip: 3
08.06.2019 09:20 ||I| [1m[30m	 Hybrid: [0m0[1m[30m, Chip: [0m7[1m[30m, Line: [0m5
08.06.2019 09:20 ||I| 		 Mode: 0
08.06.2019 09:20 ||I| 		 Manual Delay: 0, Manual Bitslip: 0
08.06.2019 09:20 ||I| 		 Done: 1, PA FSM: [1m[32mTunedPHASE[0m, WA FSM: [1m[32mTunedWORD[0m
08.06.2019 09:20 ||I| 		 Delay: 15, Bitslip: 3
08.06.2019 09:20 ||I| Waiting for DDR3 to finish initial calibration
08.06.2019 09:20 ||I| [32mSuccessfully configured Board 0[0m
08.06.2019 09:20 ||I| [32mSuccessfully configured Cbc 0[0m
08.06.2019 09:20 ||I| [32mSuccessfully configured Cbc 1[0m
08.06.2019 09:20 ||I| [32mSuccessfully configured Cbc 2[0m
08.06.2019 09:20 ||I| [32mSuccessfully configured Cbc 3[0m
08.06.2019 09:20 ||I| [32mSuccessfully configured Cbc 4[0m
08.06.2019 09:20 ||I| [32mSuccessfully configured Cbc 5[0m
08.06.2019 09:20 ||I| [32mSuccessfully configured Cbc 6[0m
08.06.2019 09:20 ||I| [32mSuccessfully configured Cbc 7[0m
08.06.2019 09:20 ||I| Trigger source 01: 7
08.06.2019 09:20 ||I| Histo Map for Module 0 does not exist - creating 
08.06.2019 09:20 ||I| Histo Map for CBC 0 (FE 0) does not exist - creating 
08.06.2019 09:20 ||I| Histo Map for CBC 1 (FE 0) does not exist - creating 
08.06.2019 09:20 ||I| Histo Map for CBC 2 (FE 0) does not exist - creating 
08.06.2019 09:20 ||I| Histo Map for CBC 3 (FE 0) does not exist - creating 
08.06.2019 09:20 ||I| Histo Map for CBC 4 (FE 0) does not exist - creating 
08.06.2019 09:20 ||I| Histo Map for CBC 5 (FE 0) does not exist - creating 
08.06.2019 09:20 ||I| Histo Map for CBC 6 (FE 0) does not exist - creating 
08.06.2019 09:20 ||I| Histo Map for CBC 7 (FE 0) does not exist - creating 
08.06.2019 09:20 ||I| 7
08.06.2019 09:20 ||I| Mesuring Efficiency per Strip ... 
08.06.2019 09:20 ||I| Taking data with 100 Events!
08.06.2019 09:20 ||I| 		Mean occupancy for the Top side: 61.2985 error= 8.61003[0m
08.06.2019 09:20 ||I| 		Mean occupancy for the Botton side: 61.0906 error= 8.26779[0m
# Noisy channels on Bottom Sensor : 2,6,12,18,19,22,23,29,30,33,34,35,50,55,57,60,65,69,72,77,78,79,83,85,86,87,90,100,110,115,117,120,121,122,123,131,144,154,158,166,170,172,173,177,178,183,189,192,193,201,205,207,209,210,212,218,222,227,231,236,239,243,245,246,247,252,254,259,260,261,263,264,270,271,274,279,281,283,288,289,294,295,299,300,302,304,308,309,311,312,314,319,326,327,330,336,341,343,344,345,348,351,354,359,363,366,368,374,376,377,378,379,380,382,383,384,386,387,391,392,394,396,398,401,402,406,409,410,412,413,415,417,420,424,425,426,429,433,440,443,444,445,447,449,451,452,453,454,455,456,457,458,461,462,463,468,473,474,476,477,480,481,482,489,491,493,494,496,497,501,503,504,506,508,513,517,518,520,521,524,527,528,529,531,533,540,542,543,545,547,549,555,558,560,561,562,563,565,570,571,576,579,580,585,588,591,594,595,596,597,598,603,604,606,608,609,617,618,627,628,633,634,640,646,647,648,652,653,654,655,657,658,674,678,679,680,683,684,685,687,688,695,696,698,706,707,711,714,723,726,727,734,735,738,741,744,746,749,751,753,754,757,758,761,762,764,765,772,773,776,777,779,783,784,785,786,791,793,795,797,800,801,803,808,810,812,818,819,822,826,829,834,837,842,845,846,848,849,850,852,856,857,858,859,863,866,872,875,876,877,879,880,881,883,884,889,893,897,902,903,909,910,912,913,914,915,920,922,926,927,930,934,939,945,950,952,954,955,956,957,958,959,961,964,967,970,973,975,981,982,983,985,989,992,997,999,1002,1003,1004,1006,1008,1009,1010,1011,1012,1013,1014
# Noisy channels on Top Sensor : 2,5,13,15,16,18,25,39,42,50,54,56,60,64,65,78,82,84,90,99,101,102,104,109,111,112,115,119,120,123,125,126,129,130,132,134,136,140,141,142,149,150,152,154,155,159,160,162,164,166,168,170,171,173,174,179,180,185,187,198,202,203,204,207,209,210,213,217,219,220,224,225,226,227,233,242,243,244,245,246,249,252,256,258,260,261,266,269,270,271,277,279,288,290,294,295,296,297,299,301,302,304,305,306,310,313,314,318,323,329,331,333,336,337,338,341,342,343,346,347,351,352,354,355,359,360,362,366,367,368,369,370,374,375,376,377,381,385,386,390,391,392,394,395,399,400,401,404,410,411,415,419,420,421,423,427,429,432,434,448,461,467,476,478,484,485,487,488,491,493,494,498,501,503,509,511,512,515,517,520,521,523,526,527,528,529,532,536,541,543,545,548,556,558,560,562,564,565,569,570,574,576,577,578,579,580,581,595,596,597,599,601,607,608,613,614,617,621,622,623,625,628,633,634,635,636,641,652,657,658,660,662,666,670,671,673,674,681,682,683,684,685,686,688,689,692,696,700,701,702,705,706,718,719,720,721,722,723,733,735,737,738,741,743,745,747,751,756,757,761,762,764,770,778,781,787,788,789,790,792,793,794,797,800,801,807,808,810,814,817,819,824,825,826,831,832,835,837,838,841,844,845,846,847,851,855,857,861,864,865,868,869,872,874,875,878,881,885,887,891,894,897,899,902,904,905,907,910,912,913,914,917,918,922,926,927,931,932,935,937,942,945,946,947,949,950,955,959,960,963,968,969,972,973,975,978,982,986,989,990,992,993,994,995,998,999,1003,1004,1005,1011,1014,1015
# Dead channels on Bottom Sensor : 165
# Dead channels on Top Sensor : 
08.06.2019 09:20 ||I| Results saved!
(Noise) Occupancy measurement finished at: Sat Jun  8 09:20:33 2019
	elapsed time: 1.39379 seconds
Complete system test finished at: Sat Jun  8 09:20:33 2019
	elapsed time: 12.5717 seconds
08.06.2019 09:20 ||I| [1m[31mclosing result file![0m
08.06.2019 09:20 ||I| [1m[31mDestroying memory objects[0m
User comment: 
