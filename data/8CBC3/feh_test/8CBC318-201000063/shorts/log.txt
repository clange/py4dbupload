12.06.2019 16:08 ||I| 


********************************************************************************
                                        [1m[31mHW SUMMARY: [0m
********************************************************************************

[1m[36m|----BeBoard  Id :[1m[34m0[1m[36m BoardType: [1m[34mD19C[1m[36m EventType: [1m[31mVR[0m
[1m[34m|       |----Board Id:      [1m[33mboard
[1m[34m|       |----URI:           [1m[33mchtcp-2.0://localhost:10203?target=192.168.0.10:50001
[1m[34m|       |----Address Table: [1m[33mfile://settings/address_tables/d19c_address_table.xml
[1m[34m|       |[0m
[34m|	|----Register  clock_source: [1m[33m3[0m
[34m|	|----Register  fc7_daq_cnfg.clock.ext_clk_en: [1m[33m0[0m
[34m|	|----Register  fc7_daq_cnfg.ttc.ttc_enable: [1m[33m0[0m
[34m|	|----Register  fc7_daq_cnfg.fast_command_block.triggers_to_accept: [1m[33m0[0m
[34m|	|----Register  fc7_daq_cnfg.fast_command_block.trigger_source: [1m[33m7[0m
[34m|	|----Register  fc7_daq_cnfg.fast_command_block.user_trigger_frequency: [1m[33m100[0m
[34m|	|----Register  fc7_daq_cnfg.fast_command_block.stubs_mask: [1m[33m1[0m
[34m|	|----Register  fc7_daq_cnfg.fast_command_block.stub_trigger_delay_value: [1m[33m0[0m
[34m|	|----Register  fc7_daq_cnfg.fast_command_block.stub_trigger_veto_length: [1m[33m0[0m
[34m|	|----Register  fc7_daq_cnfg.fast_command_block.test_pulse.delay_after_fast_reset: [1m[33m2[0m
[34m|	|----Register  fc7_daq_cnfg.fast_command_block.test_pulse.delay_after_test_pulse: [1m[33m20[0m
[34m|	|----Register  fc7_daq_cnfg.fast_command_block.test_pulse.delay_before_next_pulse: [1m[33m700[0m
[34m|	|----Register  fc7_daq_cnfg.fast_command_block.test_pulse.en_fast_reset: [1m[33m1[0m
[34m|	|----Register  fc7_daq_cnfg.fast_command_block.test_pulse.en_test_pulse: [1m[33m1[0m
[34m|	|----Register  fc7_daq_cnfg.fast_command_block.test_pulse.en_l1a: [1m[33m1[0m
[34m|	|----Register  fc7_daq_cnfg.fast_command_block.ext_trigger_delay_value: [1m[33m50[0m
[34m|	|----Register  fc7_daq_cnfg.fast_command_block.antenna_trigger_delay_value: [1m[33m50[0m
[34m|	|----Register  fc7_daq_cnfg.fast_command_block.delay_between_two_consecutive: [1m[33m10[0m
[34m|	|----Register  fc7_daq_cnfg.fast_command_block.misc.backpressure_enable: [1m[33m1[0m
[34m|	|----Register  fc7_daq_cnfg.fast_command_block.misc.stubOR: [1m[33m1[0m
[34m|	|----Register  fc7_daq_cnfg.fast_command_block.misc.initial_fast_reset_enable: [1m[33m0[0m
[34m|	|----Register  fc7_daq_cnfg.physical_interface_block.i2c.frequency: [1m[33m4[0m
[34m|	|----Register  fc7_daq_cnfg.readout_block.packet_nbr: [1m[33m99[0m
[34m|	|----Register  fc7_daq_cnfg.readout_block.global.data_handshake_enable: [1m[33m1[0m
[34m|	|----Register  fc7_daq_cnfg.readout_block.global.int_trig_enable: [1m[33m0[0m
[34m|	|----Register  fc7_daq_cnfg.readout_block.global.int_trig_rate: [1m[33m0[0m
[34m|	|----Register  fc7_daq_cnfg.readout_block.global.trigger_type: [1m[33m0[0m
[34m|	|----Register  fc7_daq_cnfg.readout_block.global.data_type: [1m[33m0[0m
[34m|	|----Register  fc7_daq_cnfg.readout_block.global.common_stubdata_delay: [1m[33m5[0m
[34m|	|----Register  fc7_daq_cnfg.dio5_block.dio5_en: [1m[33m0[0m
[34m|	|----Register  fc7_daq_cnfg.dio5_block.ch1.out_enable: [1m[33m1[0m
[34m|	|----Register  fc7_daq_cnfg.dio5_block.ch1.term_enable: [1m[33m0[0m
[34m|	|----Register  fc7_daq_cnfg.dio5_block.ch1.threshold: [1m[33m0[0m
[34m|	|----Register  fc7_daq_cnfg.dio5_block.ch2.out_enable: [1m[33m0[0m
[34m|	|----Register  fc7_daq_cnfg.dio5_block.ch2.term_enable: [1m[33m1[0m
[34m|	|----Register  fc7_daq_cnfg.dio5_block.ch2.threshold: [1m[33m50[0m
[34m|	|----Register  fc7_daq_cnfg.dio5_block.ch3.out_enable: [1m[33m1[0m
[34m|	|----Register  fc7_daq_cnfg.dio5_block.ch3.term_enable: [1m[33m0[0m
[34m|	|----Register  fc7_daq_cnfg.dio5_block.ch3.threshold: [1m[33m0[0m
[34m|	|----Register  fc7_daq_cnfg.dio5_block.ch4.out_enable: [1m[33m0[0m
[34m|	|----Register  fc7_daq_cnfg.dio5_block.ch4.term_enable: [1m[33m1[0m
[34m|	|----Register  fc7_daq_cnfg.dio5_block.ch4.threshold: [1m[33m50[0m
[34m|	|----Register  fc7_daq_cnfg.dio5_block.ch5.out_enable: [1m[33m0[0m
[34m|	|----Register  fc7_daq_cnfg.dio5_block.ch5.term_enable: [1m[33m1[0m
[34m|	|----Register  fc7_daq_cnfg.dio5_block.ch5.threshold: [1m[33m50[0m
[34m|	|----Register  fc7_daq_cnfg.tlu_block.handshake_mode: [1m[33m2[0m
[34m|	|----Register  fc7_daq_cnfg.tlu_block.tlu_enabled: [1m[33m0[0m
[34m|	|[0m
[1m[36m|	|----Module  FeId :0[0m
[32m|	|	|----CBC Files Path : hybrid_functional_tests/8CBC318-201000063/calibration/Calibration_Electron_12-06-19_16h07/[0m
[1m[36m|	|	|----CBC  Id :0, File: FE0CBC0.txt[0m
[32m|	|	|	|----ChipType: [31mCBC3[0m
[1m[36m|	|	|----CBC  Id :1, File: FE0CBC1.txt[0m
[32m|	|	|	|----ChipType: [31mCBC3[0m
[1m[36m|	|	|----CBC  Id :2, File: FE0CBC2.txt[0m
[32m|	|	|	|----ChipType: [31mCBC3[0m
[1m[36m|	|	|----CBC  Id :3, File: FE0CBC3.txt[0m
[32m|	|	|	|----ChipType: [31mCBC3[0m
[1m[36m|	|	|----CBC  Id :4, File: FE0CBC4.txt[0m
[32m|	|	|	|----ChipType: [31mCBC3[0m
[1m[36m|	|	|----CBC  Id :5, File: FE0CBC5.txt[0m
[32m|	|	|	|----ChipType: [31mCBC3[0m
[1m[36m|	|	|----CBC  Id :6, File: FE0CBC6.txt[0m
[32m|	|	|	|----ChipType: [31mCBC3[0m
[1m[36m|	|	|----CBC  Id :7, File: FE0CBC7.txt[0m
[32m|	|	|	|----ChipType: [31mCBC3[0m
[1m[36m|	|	|----Global CBC Settings: [0m
[32m|	|	|	|----ChipType: [31mCBC3[0m
[32m|	|	|	|----VCth: [31m0x23a (570)[0m
[32m|	|	|	|----TriggerLatency: [31m0x13 (19)[0m
[32m|	|	|	|----TestPulse: enabled: [31m0[32m, polarity: [31m0[32m, amplitude: [31m128[32m (0x80)[0m
[32m|	|	|	|               channelgroup: [31m0[32m, delay: [31m0[32m, groundohters: [31m1[0m
[32m|	|	|	|----Cluster & Stub Logic: ClusterWidthDiscrimination: [31m4[32m, PtWidth: [31m14[32m, Layerswap: [31m0[0m
[32m|	|	|	|                          Offset1: [31m0[32m, Offset2: [31m0[32m, Offset3: [31m0[32m, Offset4: [31m0[0m
[32m|	|	|	|----Misc Settings:  PipelineLogicSource: [31m0[32m, StubLogicSource: [31m1[32m, OR254: [31m0[32m, TPG Clock: [31m1[32m, Test Clock 40: [31m1[32m, DLL: [31m21[0m
[32m|	|	|	|----Analog Mux value: [31m0 (0x0, 0b00000)[0m
[32m|	|	|	|----List of disabled Channels: [0m
[34m|	|
|	|----SLink[0m
[34m|	|       |----DebugMode[35m : SLinkDebugMode::FULL[0m
[34m|	|       |----ConditionData: Type [31mI2C VCth1[34m, UID [31m1[34m, FeId [31m0[34m, CbcId [31m0[34m, Page [31m0[34m, Address [31m4f[34m, Value [35m58[0m
[34m|	|       |----ConditionData: Type [31mUser [34m, UID [31m128[34m, FeId [31m0[34m, CbcId [31m0[34m, Page [31m0[34m, Address [31m0[34m, Value [35m34[0m
[34m|	|       |----ConditionData: Type [31mHV [34m, UID [31m5[34m, FeId [31m0[34m, CbcId [31m2[34m, Page [31m0[34m, Address [31m0[34m, Value [35m250[0m
[34m|	|       |----ConditionData: Type [31mTDC [34m, UID [31m3[34m, FeId [31m255[34m, CbcId [31m0[34m, Page [31m0[34m, Address [31m0[34m, Value [35m0[0m


********************************************************************************
                                        [1m[31mEND OF HW SUMMARY: [0m
********************************************************************************


[31mSetting[0m --[1m[36mTargetVcth[0m:[1m[33m120[0m
[31mSetting[0m --[1m[36mTargetOffset[0m:[1m[33m80[0m
[31mSetting[0m --[1m[36mNevents[0m:[1m[33m100[0m
[31mSetting[0m --[1m[36mTestPulsePotentiometer[0m:[1m[33m0[0m
[31mSetting[0m --[1m[36mHoleMode[0m:[1m[33m0[0m
[31mSetting[0m --[1m[36mVerificationLoop[0m:[1m[33m0[0m
[31mSetting[0m --[1m[36mInitialVcth[0m:[1m[33m120[0m
[31mSetting[0m --[1m[36mSignalScanStep[0m:[1m[33m2[0m
[31mSetting[0m --[1m[36mFitSignal[0m:[1m[33m0[0m

12.06.2019 16:08 ||I| Creating directory: Results/HybridTest_Electron_12-06-19_16h08
Info in <TCivetweb::Create>: Starting HTTP server on port 8080
12.06.2019 16:08 ||I| Opening THttpServer on port 8080. Point your browser to: [1m[32mcmsuptkhsetup1.dyndns.cern.ch:8080[0m
12.06.2019 16:08 ||I| [1m[34mConfiguring HW parsed from .xml file: [0m
12.06.2019 16:08 ||I| [1m[32mSetting the I2C address table[0m
12.06.2019 16:08 ||I| [1m[32mAccording to the Firmware status registers, it was compiled for: 1 hybrid(s), 8 CBC3 chip(s) per hybrid[0m
12.06.2019 16:08 ||I| Enabling Hybrid 0
12.06.2019 16:08 ||I|      Enabling Chip 0
12.06.2019 16:08 ||I|      Enabling Chip 1
12.06.2019 16:08 ||I|      Enabling Chip 2
12.06.2019 16:08 ||I|      Enabling Chip 3
12.06.2019 16:08 ||I|      Enabling Chip 4
12.06.2019 16:08 ||I|      Enabling Chip 5
12.06.2019 16:08 ||I|      Enabling Chip 6
12.06.2019 16:08 ||I|      Enabling Chip 7
12.06.2019 16:08 ||I| [1m[32m1 hybrid(s) was(were) enabled with the total amount of 8 chip(s)![0m
Reply from chip 0: 20000040
Reply from chip 1: 20040040
Reply from chip 2: 20080040
Reply from chip 3: 200c0040
Reply from chip 4: 20100040
Reply from chip 5: 20140040
Reply from chip 6: 20180040
Reply from chip 7: 201c0040
12.06.2019 16:08 ||I| Successfully received *Pings* from 8 Cbcs
12.06.2019 16:08 ||I| [32mCBC3 Phase tuning finished succesfully[0m
12.06.2019 16:08 ||I| [1m[30m	 Hybrid: [0m0[1m[30m, Chip: [0m0[1m[30m, Line: [0m5
12.06.2019 16:08 ||I| 		 Mode: 0
12.06.2019 16:08 ||I| 		 Manual Delay: 0, Manual Bitslip: 0
12.06.2019 16:08 ||I| 		 Done: 1, PA FSM: [1m[32mTunedPHASE[0m, WA FSM: [1m[32mTunedWORD[0m
12.06.2019 16:08 ||I| 		 Delay: 15, Bitslip: 3
12.06.2019 16:08 ||I| [1m[30m	 Hybrid: [0m0[1m[30m, Chip: [0m1[1m[30m, Line: [0m5
12.06.2019 16:08 ||I| 		 Mode: 0
12.06.2019 16:08 ||I| 		 Manual Delay: 0, Manual Bitslip: 0
12.06.2019 16:08 ||I| 		 Done: 1, PA FSM: [1m[32mTunedPHASE[0m, WA FSM: [1m[32mTunedWORD[0m
12.06.2019 16:08 ||I| 		 Delay: 15, Bitslip: 3
12.06.2019 16:08 ||I| [1m[30m	 Hybrid: [0m0[1m[30m, Chip: [0m2[1m[30m, Line: [0m5
12.06.2019 16:08 ||I| 		 Mode: 0
12.06.2019 16:08 ||I| 		 Manual Delay: 0, Manual Bitslip: 0
12.06.2019 16:08 ||I| 		 Done: 1, PA FSM: [1m[32mTunedPHASE[0m, WA FSM: [1m[32mTunedWORD[0m
12.06.2019 16:08 ||I| 		 Delay: 15, Bitslip: 3
12.06.2019 16:08 ||I| [1m[30m	 Hybrid: [0m0[1m[30m, Chip: [0m3[1m[30m, Line: [0m5
12.06.2019 16:08 ||I| 		 Mode: 0
12.06.2019 16:08 ||I| 		 Manual Delay: 0, Manual Bitslip: 0
12.06.2019 16:08 ||I| 		 Done: 1, PA FSM: [1m[32mTunedPHASE[0m, WA FSM: [1m[32mTunedWORD[0m
12.06.2019 16:08 ||I| 		 Delay: 15, Bitslip: 3
12.06.2019 16:08 ||I| [1m[30m	 Hybrid: [0m0[1m[30m, Chip: [0m4[1m[30m, Line: [0m5
12.06.2019 16:08 ||I| 		 Mode: 0
12.06.2019 16:08 ||I| 		 Manual Delay: 0, Manual Bitslip: 0
12.06.2019 16:08 ||I| 		 Done: 1, PA FSM: [1m[32mTunedPHASE[0m, WA FSM: [1m[32mTunedWORD[0m
12.06.2019 16:08 ||I| 		 Delay: 15, Bitslip: 3
12.06.2019 16:08 ||I| [1m[30m	 Hybrid: [0m0[1m[30m, Chip: [0m5[1m[30m, Line: [0m5
12.06.2019 16:08 ||I| 		 Mode: 0
12.06.2019 16:08 ||I| 		 Manual Delay: 0, Manual Bitslip: 0
12.06.2019 16:08 ||I| 		 Done: 1, PA FSM: [1m[32mTunedPHASE[0m, WA FSM: [1m[32mTunedWORD[0m
12.06.2019 16:08 ||I| 		 Delay: 15, Bitslip: 3
12.06.2019 16:08 ||I| [1m[30m	 Hybrid: [0m0[1m[30m, Chip: [0m6[1m[30m, Line: [0m5
12.06.2019 16:08 ||I| 		 Mode: 0
12.06.2019 16:08 ||I| 		 Manual Delay: 0, Manual Bitslip: 0
12.06.2019 16:08 ||I| 		 Done: 1, PA FSM: [1m[32mTunedPHASE[0m, WA FSM: [1m[32mTunedWORD[0m
12.06.2019 16:08 ||I| 		 Delay: 15, Bitslip: 3
12.06.2019 16:08 ||I| [1m[30m	 Hybrid: [0m0[1m[30m, Chip: [0m7[1m[30m, Line: [0m5
12.06.2019 16:08 ||I| 		 Mode: 0
12.06.2019 16:08 ||I| 		 Manual Delay: 0, Manual Bitslip: 0
12.06.2019 16:08 ||I| 		 Done: 1, PA FSM: [1m[32mTunedPHASE[0m, WA FSM: [1m[32mTunedWORD[0m
12.06.2019 16:08 ||I| 		 Delay: 15, Bitslip: 3
12.06.2019 16:08 ||I| Waiting for DDR3 to finish initial calibration
12.06.2019 16:08 ||I| [32mSuccessfully configured Board 0[0m
12.06.2019 16:08 ||I| [32mSuccessfully configured Cbc 0[0m
12.06.2019 16:08 ||I| [32mSuccessfully configured Cbc 1[0m
12.06.2019 16:08 ||I| [32mSuccessfully configured Cbc 2[0m
12.06.2019 16:08 ||I| [32mSuccessfully configured Cbc 3[0m
12.06.2019 16:08 ||I| [32mSuccessfully configured Cbc 4[0m
12.06.2019 16:08 ||I| [32mSuccessfully configured Cbc 5[0m
12.06.2019 16:08 ||I| [32mSuccessfully configured Cbc 6[0m
12.06.2019 16:08 ||I| [32mSuccessfully configured Cbc 7[0m
12.06.2019 16:08 ||I| Trigger source 01: 7
12.06.2019 16:08 ||I| Histo Map for Module 0 does not exist - creating 
12.06.2019 16:08 ||I| Histo Map for CBC 0 (FE 0) does not exist - creating 
12.06.2019 16:08 ||I| Histo Map for CBC 1 (FE 0) does not exist - creating 
12.06.2019 16:08 ||I| Histo Map for CBC 2 (FE 0) does not exist - creating 
12.06.2019 16:08 ||I| Histo Map for CBC 3 (FE 0) does not exist - creating 
12.06.2019 16:08 ||I| Histo Map for CBC 4 (FE 0) does not exist - creating 
12.06.2019 16:08 ||I| Histo Map for CBC 5 (FE 0) does not exist - creating 
12.06.2019 16:08 ||I| Histo Map for CBC 6 (FE 0) does not exist - creating 
12.06.2019 16:08 ||I| Histo Map for CBC 7 (FE 0) does not exist - creating 
12.06.2019 16:08 ||I| 7
12.06.2019 16:08 ||I| [1m[34mStarting short finding loop.[0m
12.06.2019 16:08 ||I| [1m[34mChecking for shorts using TP group 0[0m
12.06.2019 16:08 ||I| [1m[34m.... checking FE hybrid 0[0m
12.06.2019 16:08 ||I| [1m[34mChecking for shorts using TP group 1[0m
12.06.2019 16:08 ||I| [1m[34m.... checking FE hybrid 0[0m
12.06.2019 16:08 ||I| [1m[34mChecking for shorts using TP group 2[0m
12.06.2019 16:08 ||I| [1m[34m.... checking FE hybrid 0[0m
12.06.2019 16:08 ||I| [1m[34mChecking for shorts using TP group 3[0m
12.06.2019 16:08 ||I| [1m[34m.... checking FE hybrid 0[0m
12.06.2019 16:08 ||I| [1m[34mChecking for shorts using TP group 4[0m
12.06.2019 16:08 ||I| [1m[34m.... checking FE hybrid 0[0m
12.06.2019 16:08 ||I| [1m[34mChecking for shorts using TP group 5[0m
12.06.2019 16:08 ||I| [1m[34m.... checking FE hybrid 0[0m
12.06.2019 16:08 ||I| [1m[34mChecking for shorts using TP group 6[0m
12.06.2019 16:08 ||I| [1m[34m.... checking FE hybrid 0[0m
12.06.2019 16:08 ||I| [1m[34mChecking for shorts using TP group 7[0m
12.06.2019 16:08 ||I| [1m[34m.... checking FE hybrid 0[0m
12.06.2019 16:08 ||I| [1m[34mIn total found 0 on this hybrid.[0m
12.06.2019 16:08 ||I| Results saved!
12.06.2019 16:08 ||I| Results saved!
12.06.2019 16:08 ||I| [1m[31mclosing result file![0m
12.06.2019 16:08 ||I| [1m[31mDestroying memory objects[0m
User comment: 
