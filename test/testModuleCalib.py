#!/usr/bin/env python
# $Id$
# Created by Alessandro Di Mattia <dimattia@cern.ch>, 15-May-2022

# This script test the upload of the Module Calibration data.

import os,sys,fnmatch,copy
import pandas as pd

from AnsiColor  import Fore, Back, Style
from Exceptions import *
from Module     import ModuleCalibrationT,BaseUploader
from Utils      import DBupload,merge_data
from optparse   import OptionParser
from datetime   import datetime


if __name__ == "__main__":
   p = OptionParser(usage="usage: %prog <Module Serial Number> [options]", version="1.1")
   
   p.add_option( '-i','--inserter',
               type    = 'string',
               default = None,
               dest    = 'inserter',
               metavar = 'STR',
               help    = 'Override the account name of the operator that performs the data update.')
   
   p.add_option( '--pro',
                  action  = 'store_true',
                  default = False,
                  dest    = 'isProduction',
                  help    = 'Set the development database as target.')
   
   p.add_option( '--upload',
                  action  = 'store_true',
                  default = False,
                  dest    = 'upload',
                  help    = 'Perform the data upload in database')
   
   p.add_option( '--verbose',
                  action  = 'store_true',
                  default = False,
                  dest    = 'verbose',
                  help    = 'Force the uploaders to print their configuration and data')

   p.add_option( '--debug',
                  action  = 'store_true',
                  default = False,
                  dest    = 'debug',
                  help    = 'Force the verbose options in the network query uploaders to print their configuration and data')
 

   (opt, args) = p.parse_args()

   if len(args)!=1:
      p.error('wrong usage!')

  
   if opt.verbose:
      BaseUploader.verbose = True


   # Getting the part data from database
   BaseUploader.database = 'int2r' if opt.isProduction==False else 'cmsr'
   #db = DBaccess(database=f'trker_{database}', verbose=opt.debug)

   name = args[0]    # it contains the serial number of the module
   
   #Top entry data structure
   summary = ModuleCalibrationT(name,'CalibrationSummary',inserter=opt.inserter)
   
   summary.load_run_metadata(run_type='mod_final', 
                          #run_number='1',    if not given explicitely run gets automatically incremented
                          run_begin=datetime.strptime('15-05-2022 10:50',"%d-%m-%Y %H:%M"), 
                          run_operator='Awesome Operator', 
                          run_location='FNL',
                          run_comment='This is a comment.')
   summary.load_block('Noise',13.397274,0.734968)
   #load any root file unser the directory ./root_test
   summary.load_root_file('./root_test')
   
   
   
   # Entry for Hybrid 0
   serial_hy = '2SFEH40L-201000002'  #test hybrid
   side_hy   = 'Left'                #side of test hybrid
   hybrid1_data = ModuleCalibrationT(name,'SCurveHybrid',inserter=opt.inserter)
   hybrid1_data.load_hybrid_id(serial_hy,side_hy)
   hybrid1_data.load_block('Noise',6.673712,0.567415,
                              nOutliersLow=3,outliersLow='177,189,1990,',
                              nOutliersHigh=11,outliersHigh='175,179,181,183,187,191,193,195,957,1988,1992,'
                              )
   
   
   # Entry for CBC 0  of Hybrid 0
   cbc0_data = ModuleCalibrationT(name,'SCurveCBC',inserter=opt.inserter)
   cbc0_data.load_hybrid_id(serial_hy,side_hy)
   cbc0_data.load_cbc_id('1010','0X01')
   cbc0_data.load_block('Noise',7.044689,1.070964,
                       nOutliersLow=1,outliersLow='189,',
                       nOutliersHigh=6,outliersHigh='179,181,183,191,193,195,',
                       vplus=595.0)
   cbc0_data.load_block('Occupancy',0.0,0.0)
   cbc0_data.load_block('Offset',137.212601,15.618802)
   cbc0_data.load_block('Pedestal',597.752380,0.359840)
   
   
   # Entry for CBC 1 of Hybrid 0
   cbc1_data = ModuleCalibrationT(name,'SCurveCBC',inserter=opt.inserter)
   cbc1_data.load_hybrid_id(serial_hy,side_hy)
   cbc1_data.load_cbc_id('1011','0X02')
   cbc1_data.load_block('Noise',6.855258,0.240280,vplus=602.0)
   cbc1_data.load_block('Occupancy',0.0,0.0)
   cbc1_data.load_block('Offset',123.811020,14.237988)
   cbc1_data.load_block('Pedestal',597.821045,0.364406)
      
   
   # Entry for CBC 2 of hybrid 0
   cbc2_data = ModuleCalibrationT(name,'SCurveCBC',inserter=opt.inserter)
   cbc2_data.load_hybrid_id(serial_hy,side_hy)
   cbc2_data.load_cbc_id('1012','0X03')
   cbc2_data.load_block('Noise',6.696088,0.235551,vplus=584.0)
   cbc2_data.load_block('Occupancy',0.0,0.0)
   cbc2_data.load_block('Offset',161.547241,14.747607)
   cbc2_data.load_block('Pedestal',597.739868,0.295843)
      
   
   # Entry for CBC 3 of Hybrid 0
   cbc3_data = ModuleCalibrationT(name,'SCurveCBC',inserter=opt.inserter)
   cbc3_data.load_hybrid_id(serial_hy,side_hy)
   cbc3_data.load_cbc_id('1013','0X04')
   cbc3_data.load_block('Noise',6.585096,0.403838,vplus=584.0,
                       nOutliersHigh=1,outliersHigh='195,')
   cbc3_data.load_block('Occupancy',0.0,0.0)
   cbc3_data.load_block('Offset',160.397644,16.145599)
   cbc3_data.load_block('Pedestal',597.806396,0.327375)
      
   
   # Entry for CBC 4 of Hybrid 0
   cbc4_data = ModuleCalibrationT(name,'SCurveCBC',inserter=opt.inserter)
   cbc4_data.load_hybrid_id(serial_hy,side_hy)
   cbc4_data.load_cbc_id('1014','0X05')
   cbc4_data.load_block('Noise',6.535059,0.270681,vplus=593.0)
   cbc4_data.load_block('Occupancy',0.0,0.0)
   cbc4_data.load_block('Offset',141.464569,15.984635)
   cbc4_data.load_block('Pedestal',597.904602,0.330070)
      
   
   # Entry for CBC 5 of Hybrid 0
   cbc5_data = ModuleCalibrationT(name,'SCurveCBC',inserter=opt.inserter)
   cbc5_data.load_hybrid_id(serial_hy,side_hy)
   cbc5_data.load_cbc_id('1015','0X06')
   cbc5_data.load_block('Noise',6.171168,0.277037,vplus=593.0)
   cbc5_data.load_block('Occupancy',0.0,0.0)
   cbc5_data.load_block('Offset',142.578735,15.869291)
   cbc5_data.load_block('Pedestal',597.876160,0.328306)
      
   
   # Entry for CBC 6 of Hybrid 0
   cbc6_data = ModuleCalibrationT(name,'SCurveCBC',inserter=opt.inserter)
   cbc6_data.load_hybrid_id(serial_hy,side_hy)
   cbc6_data.load_cbc_id('1016','0X07')
   cbc6_data.load_block('Noise',6.685368,0.279954,vplus=602.0)
   cbc6_data.load_block('Occupancy',0.0,0.0)
   cbc6_data.load_block('Offset',124.031494,13.551270)
   cbc6_data.load_block('Pedestal',597.980774,0.353777)
   
   
   # Entry for CBC 7 of Hybrid 0
   cbc7_data = ModuleCalibrationT(name,'SCurveCBC',inserter=opt.inserter)
   cbc7_data.load_hybrid_id(serial_hy,side_hy)
   cbc7_data.load_cbc_id('1017','0X08')
   cbc7_data.load_block('Noise',6.816969,0.672795,vplus=594.0,
                       nOutliersLow=1,outliersLow='212,',
                       nOutliersHigh=2,outliersHigh='210,214,')
   cbc7_data.load_block('Occupancy',0.0,0.0)
   cbc7_data.load_block('Offset',124.031494,13.551270)
   cbc7_data.load_block('Pedestal',597.980774,0.353777)
      
   
   
   # Entry for Hybrid 1
   serial1_hy = '2SFEH40R-201000001'
   side1_hy   = 'Right'
   hybrid2_data = ModuleCalibrationT(name,'SCurveHybrid',inserter=opt.inserter)
   hybrid2_data.load_hybrid_id(serial1_hy,side1_hy)
   hybrid2_data.load_block('Noise',6.723562,0.308789,
                              nOutliersHigh=2,outliersHigh='1349,1351,'
                              )
      
   
   # Entry for CBC 0 of Hybrid 1
   cbc9_data = ModuleCalibrationT(name,'SCurveCBC',inserter=opt.inserter)
   cbc9_data.load_hybrid_id(serial1_hy,side1_hy)
   cbc9_data.load_cbc_id('1020','0X01')
   cbc9_data.load_block('Noise',6.869868,0.214595,
                       nOutliersHigh=2,outliersHigh='1,2,',
                       vplus=600.0)
   cbc9_data.load_block('Occupancy',0.0,0.0)
   cbc9_data.load_block('Offset',126.161415,13.784676)
   cbc9_data.load_block('Pedestal',597.160645,0.295576)
      
   
   # Entry for CBC 1 of Hybrid 1
   cbc10_data = ModuleCalibrationT(name,'SCurveCBC',inserter=opt.inserter)
   cbc10_data.load_hybrid_id(serial1_hy,side1_hy)
   cbc10_data.load_cbc_id('1021','0X02')
   cbc10_data.load_block('Noise',6.964753,0.194250,vplus=603.0)
   cbc10_data.load_block('Occupancy',0.0,0.0)
   cbc10_data.load_block('Offset',120.496063,13.741070)
   cbc10_data.load_block('Pedestal',597.142883,0.286469)
   
   
   # Entry for CBC 2 of Hybrid 1
   cbc11_data = ModuleCalibrationT(name,'SCurveCBC',inserter=opt.inserter)
   cbc11_data.load_hybrid_id(serial1_hy,side1_hy)
   cbc11_data.load_cbc_id('1022','0X03')
   cbc11_data.load_block('Noise',6.622918,0.206688,vplus=601.0)
   cbc11_data.load_block('Occupancy',0.0,0.0)
   cbc11_data.load_block('Offset',126.688980,4.881477)
   cbc11_data.load_block('Pedestal',597.198181,0.315046)
   
   
   # Entry for CBC 3 of Hybrid 1
   cbc12_data = ModuleCalibrationT(name,'SCurveCBC',inserter=opt.inserter)
   cbc12_data.load_hybrid_id(serial1_hy,side1_hy)
   cbc12_data.load_cbc_id('1023','0X04')
   cbc12_data.load_block('Noise',6.645944,0.228374,vplus=603.0)
   cbc12_data.load_block('Occupancy',0.0,0.0)
   cbc12_data.load_block('Offset',121.208664,13.470289)
   cbc12_data.load_block('Pedestal',597.200195,0.328985)
   
   
   # Entry for CBC 4 of Hybrid 1
   cbc13_data = ModuleCalibrationT(name,'SCurveCBC',inserter=opt.inserter)
   cbc13_data.load_hybrid_id(serial1_hy,side1_hy)
   cbc13_data.load_cbc_id('1024','0X05')
   cbc13_data.load_block('Noise',6.713649,0.239174,vplus=601.0)
   cbc13_data.load_block('Occupancy',0.0,0.0)
   cbc13_data.load_block('Offset',125.826775,15.009494)
   cbc13_data.load_block('Pedestal',597.196533,0.322533)
   
   
   # Entry for CBC 5 of Hybrid 1
   cbc14_data = ModuleCalibrationT(name,'SCurveCBC',inserter=opt.inserter)
   cbc14_data.load_hybrid_id(serial1_hy,side1_hy)
   cbc14_data.load_cbc_id('1025','0X06')
   cbc14_data.load_block('Noise',6.550499,0.493349,vplus=613.0,
                         nOutliersHigh=2,outliersHigh='79,81,')
   cbc14_data.load_block('Occupancy',0.0,0.0)
   cbc14_data.load_block('Offset',106.566933,10.990756)
   cbc14_data.load_block('Pedestal',597.197754,0.336094)
   
   
   # Entry for CBC 6 of Hybrid 1
   cbc15_data = ModuleCalibrationT(name,'SCurveCBC',inserter=opt.inserter)
   cbc15_data.load_hybrid_id(serial1_hy,side1_hy)
   cbc15_data.load_cbc_id('1026','0X07')
   cbc15_data.load_block('Noise',6.866416,0.229667,vplus=612.0)
   cbc15_data.load_block('Occupancy',0.0,0.0)
   cbc15_data.load_block('Offset',108.448822,12.497692)
   cbc15_data.load_block('Pedestal',597.201965,0.352315)
   
   
   # Entry for CBC 7 of Hybrid 1
   cbc16_data = ModuleCalibrationT(name,'SCurveCBC',inserter=opt.inserter)
   cbc16_data.load_hybrid_id(serial1_hy,side1_hy)
   cbc16_data.load_cbc_id('1027','0X08')
   cbc16_data.load_block('Noise',6.554452,0.236675,vplus=590.0)
   cbc16_data.load_block('Occupancy',0.0,0.0)
   cbc16_data.load_block('Offset',147.433075,15.915174)
   cbc16_data.load_block('Pedestal',596.999207,0.284686)
   
   
   #merge conhruent data
   hybrid_data = merge_data(hybrid1_data,hybrid2_data)
   
   cbc_data = merge_data(cbc0_data, cbc1_data, cbc2_data, cbc3_data, cbc4_data,\
                         cbc5_data, cbc6_data, cbc7_data, cbc9_data, cbc10_data,\
                         cbc11_data,cbc12_data,cbc13_data,cbc14_data,cbc15_data,\
                         cbc16_data)
   
   #set the data hyerarchy
   hybrid_data.add_child(cbc_data)
   summary.add_child(hybrid_data)
   
   summary.dump_xml_data()

   
   
   path = os.path.dirname(os.environ.get('DBLOADER'))
   db = DBupload(database=BaseUploader.database,path_to_dbloader_api=path ,verbose=opt.debug)
   if opt.upload:
      # The test mode of the DB-Loader does not work for attributes 
      db.upload_data(f'{summary.name}.zip',not opt.upload)