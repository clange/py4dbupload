#!/usr/bin/env python
# $Id$
# Created by Alessandro Di Mattia <dimattia@cern.ch>, 22-March-2019

# This script masters the insertion of the CBC component data.

#import os,sys,fnmatch

#from AnsiColor  import Fore, Back, Style
#from Exceptions import *
from Utils      import *
from DataReader import *
from CBC        import *
from optparse   import OptionParser



if __name__ == "__main__":
   p = OptionParser(usage="usage: %prog [options] <path to CBC data>, ...", version="1.1")

   p.add_option( '-d','--desc',
                  type    = 'string',
                  default = 'CBC3.0 Batch 1',
                  dest    = 'description',
                  metavar = 'STR',
                  help    = 'Input a description for the data to be uploaded')

   p.add_option( '-v','--ver',
                  type    = 'string',
                  default = 'CBC3.0',
                  dest    = 'version',
                  metavar = 'STR',
                  help    = 'Input the version of the CBC production')

   p.add_option( '--verbose',
                  action  = 'store_true',
                  default = False,
                  dest    = 'verbose',
                  help    = 'Force the uploaders to print their configuration and data')

   (opt, args) = p.parse_args()

   if len(args)<1:
      p.error('need at least 1 argument!')

   #print args,  args[0]

   for p in args: excel_files = search_files(p,'*.xls')+search_files(p,'*.xlsx')
   for p in args: pdf_files   = search_files(p,'*.pdf')
   for p in args: sinf_files  = search_files(p,'*.sinf')

   #print(excel_files)

   wafer_conf = {
      'kind_of_part'    : 'CBC3 Wafer',
      'unique_location' : 'CERN',
   }

   wafer_conf['version']     = opt.version
   wafer_conf['description'] = opt.description

   cbc_chip_conf = {
      'kind_of_part'    : 'CBC3 Readout Chip',
      'unique_location' : 'CERN',
   }

   cbc_chip_conf['version']     = opt.version
   cbc_chip_conf['description'] = opt.description


   if opt.verbose:  BaseUploader.verbose = True

   data = join_data_files(excel_files,pdf_files,sinf_files)

   for batch in data.keys():
      cbcwafer = CBCWaferFromMapFile(batch,wafer_conf,data[batch][1])
      cbcwafer.dump_xml_data()
      chip_table = TableReader(data[batch][0],t_offset=2)
      chip_sinf  = data[batch][2]
      cbcchip  = CBCChipFromExcelFile(batch,cbc_chip_conf,chip_table,chip_sinf)
      cbcchip.dump_xml_data(chip_wafer=cbcwafer)
