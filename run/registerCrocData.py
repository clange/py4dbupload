#!/usr/bin/env python
# $Id$
# Created by Alessandro Di Mattia <dimattia@cern.ch>, 12-Oct-2021

# This script masters the registration of the CROC Probe data.


import os,traceback,sys,time

from AnsiColor   import Fore, Back, Style
from CROC        import *
from UpdatePart  import Update
from Utils       import *
from datetime    import datetime
from optparse    import OptionParser
from decimal     import Decimal, ROUND_HALF_UP
from progressbar import *

import json

def load(obj,tag,data):
   if isinstance(data,datetime):
      obj.load_result_block(tag,data)
   elif tag in data.keys():
      # Skipping keys with None values
      if data[tag] is None:
         return
      obj.load_result_block(tag,str(data[tag]))


def get_test_time(data):
   try:
      ttime = datetime.strptime(data['TEST_START'], '%Y/%m/%d %H:%M:%S')
      return ttime
   except ValueError:
      try:
         ttime = datetime.strptime(data['TEST_START'], '%Y-%m-%d %H:%M:%S')
         return ttime
      except ValueError:
         stime = data['TEST_START']
         print(f'Date {stime} has not a recognized format!')

         

if __name__ == "__main__":
   p = OptionParser(usage="usage: %prog [options] [json data file/s]", version="1.1")

   p.add_option( '-d','--data',
               type    = 'string',
               default = '',
               dest    = 'data_path',
               metavar = 'STR',
               help    = 'Path to the json data file with CROC data.')

   p.add_option( '-v','--ver',
               type    = 'string',
               default = '1.0',
               dest    = 'ver',
               metavar = 'STR',
               help    = 'Version type of the components; overwitten by the tag in the csv table.')
   
   p.add_option( '-i','--inserter',
               type    = 'string',
               default = None,
               dest    = 'inserter',
               metavar = 'STR',
               help    = 'The person performing the data upload')

   p.add_option( '-o','--operator',
               type    = 'string',
               default = '',
               dest    = 'operator',
               metavar = 'STR',
               help    = 'The operator that performed the probe run')
   
   p.add_option( '--dev',
               action  = 'store_true',
               default = False,
               dest    = 'isDevelopment',
               help    = 'Set the development database as target.')
   
   p.add_option( '--upload',
               action  = 'store_true',
               default = False,
               dest    = 'upload',
               help    = 'Perform the data upload in database')
   
   p.add_option( '--verbose',
               action  = 'store_true',
               default = False,
               dest    = 'verbose',
               help    = 'Force the uploaders to print their configuration and data')

   p.add_option( '--debug',
               action  = 'store_true',
               default = False,
               dest    = 'debug',
               help    = 'Force the verbose options in the network query uploaders to print their configuration and data')


   (opt, args) = p.parse_args()


   BaseUploader.database = 'cmsr' if opt.isDevelopment==False else 'int2r'
   BaseUploader.verbose = opt.verbose
   BaseUploader.debug = opt.debug
   
   data_files = args
   
   if len(data_files)==0 and opt.data_path!='':
      data_files = search_files(opt.data_path,'*.json')
   

   # Open files
   data = []
   wafer_found = False
   for f in data_files:
      result = json.load(open(f))
      if 'WAFER_ID' in result.keys():
         if wafer_found:
            print('Only 1 wafer file for upload cycle!')
            exit(1)
         data.insert(0,result)
         wafer_found = True
      else:
         data.append(result)
   
   wafer_data = data.pop(0)
   chips_data  = data
   
   update_on_result = UploaderContainer('components_update',inserter=opt.inserter)
   
   # Fills the wafer data
   wafer_id = wafer_data['LABEL']
   wafer = CROCWaferTestResultsT(wafer_id,'Results',kind='CROC Proto Wafer',inserter=opt.inserter)
   
   wafer_test_time = get_test_time(wafer_data)
   wafer.load_run_metadata(run_type='CROCprobe',\
                           run_begin=wafer_test_time,\
                           run_operator=opt.operator, \
                           run_location=wafer_data['PROBE_SITE'],\
                           run_comment=wafer_data['NOTES'])
   
   for k in wafer_data.keys():
      if k=='TEST_START':
         load(wafer,k,wafer_test_time)
      elif k=='LABEL' or k=='WAFER_ID' or k=='BATCH_ID': 
         # Skip these data
         pass
      else:
         # Fill the chip test data
         load(wafer,k,wafer_data)
   #load(wafer,'PROBE_CARD_ID',wafer_data)
   #load(wafer,'PROBE_CARD_VERSION',wafer_data)
   #load(wafer,'PROBE_SITE',wafer_data)
   #load(wafer,'TEST_START',wafer_data)
   #load(wafer,'CUT_VERSION',wafer_data)
   #load(wafer,'ENABLED_TESTS',wafer_data)
   #load(wafer,'NOTES',wafer_data)
   
   wafer_update_data = {
      'attributes' : [('Probe Card ID',wafer_data['PROBE_CARD_ID']),\
                      ('Probe Card Version',wafer_data['PROBE_CARD_VERSION']),\
                      ('Probing Site',wafer_data['PROBE_SITE'])]
                       }
   wafer_update = Update(wafer_id,Idt='name_label',\
                         default=wafer_update_data,\
                         inserter=opt.inserter, mode='batch')
   
   update_on_result.add(wafer_update)
   
   # Fills the chip data
   for chip_data in chips_data:
      chip_id = chip_data['LABEL']
      # chip_serial = chip_data['EFUSE_CODE']
      
      chip = CROCWaferTestResultsT(chip_id,'Results',kind='CROC Proto Chip',inserter=opt.inserter)
      #chip_test_time = datetime.strptime(data['TEST_START'], '%Y-%m-%d %H:%M:%S') 
   
      data_points = None
      for k in chip_data.keys():
          
         if k.split()[-1]=='vector':   
            # Fils the chip data points
            if data_points == None:
               data_points = CROCWaferTestResultsT(chip_id,'DataPoint',kind='CROC Proto Chip',inserter=opt.inserter)
            test_type   = k.split()[0]
            for point in chip_data[k].keys():
                data_points.load_data_points(test_type,point,chip_data[k][point])
         
         elif k=='CHIP_COL' or k=='CHIP_ROW' or k=='LABEL': 
            # Skip these data
            pass
         
         elif k=='TEST_START':
            load(chip,k,get_test_time(chip_data))
         
         else:
            # Fill the chip test data
            load(chip,k,chip_data)
         
      if data_points!=None:   chip.add_child(data_points)

      status = 'Good' if chip_data['GRADE']=='green' else 'Bad'
      chip_update_data = {}
      attributes = [('Grade',chip_data['GRADE']),\
                    ('Status',status)]

      # Adding serial number
      try:
         if chip_data['EFUSE_CODE'] is not None:
            chip_update_data['serialNumber'] = str(chip_data['EFUSE_CODE'])
      except KeyError:
         pass

      # Adding optional attributes if present in the input data
      attributes_optional = {
         'FAILURE_REASON': 'Failure Reason',
         'IREF_TRIM_CODE': 'IREF Trim Code',
         'VDDA_TRIM_CODE': 'VDDA Trim Code',
         'VDDD_TRIM_CODE': 'VDDD Trim Code'
      }

      for attr, name in attributes_optional.items():
         try:
            if chip_data[attr] is not None:
               attributes.append((name, str(chip_data[attr])))
         except KeyError:
            continue

      chip_update_data['attributes'] = attributes
       
      chip_update = Update(chip_id,Idt='name_label',\
                           default=chip_update_data,\
                           inserter=opt.inserter, mode='batch')
      
      update_on_result.add(chip_update)
      
      wafer.add_child(chip)

   wafer_file = wafer.dump_xml_data()
   update_on_result.dump_xml_data()
   
   path = os.path.dirname(os.environ.get('DBLOADER'))
   db_loader = DBupload(database=BaseUploader.database,path_to_dbloader_api=path,verbose=opt.debug)

   if opt.upload:
      # The test mode of the DB-Loader does not work for attributes 
      db_loader.upload_data(f'{wafer.name}.xml')
      db_loader.upload_data(f'{update_on_result.name}.xml')
