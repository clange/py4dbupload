#!/usr/bin/env python
# $Id$
# Created by Alessandro Di Mattia <dimattia@cern.ch>, 03-Sep-2020

# This script masters the registration of a generic component.

import os,traceback,sys

from AnsiColor  import Fore, Back, Style
from DataReader import TableReader
from UpdatePart import *
from Utils      import *
from optparse   import OptionParser


if __name__ == "__main__":
   p = OptionParser(usage="usage: %prog [options] <serial number file>, ...", version="1.1")

   p.add_option( '--date',
               type    = 'string',
               default = '2020-05-26',
               dest    = 'date',
               metavar = 'STR',
               help    = 'Date of the component production / test')

   p.add_option( '--kop',
               type    = 'string',
               default = 'Tracker Prototype PS Power Hybrid',
               dest    = 'kop',
               metavar = 'STR',
               help    = 'kind_of_part of the components to upload')

   p.add_option( '-l','--location',
               type    = 'string',
               default = 'Aachen 1B',
               dest    = 'location',
               metavar = 'STR',
               help    = 'Location of the components to upload')

   p.add_option( '-m','--manufacturer',
               type    = 'string',
               default = 'EES-KITIPE',
               dest    = 'manufacturer',
               metavar = 'STR',
               help    = 'Manufacturer of the component to upload')

   p.add_option( '-v','--ver',
               type    = 'string',
               default = 'V1.2',
               dest    = 'version',
               metavar = 'STR',
               help    = 'Version of the component to upload')

   p.add_option( '--verbose',
               action  = 'store_true',
               default = False,
               dest    = 'verbose',
               help    = 'Force the uploaders to print their configuration and data')

   (opt, args) = p.parse_args()

   if len(args)<1:
      p.error('need at least 1 argument!')


   if opt.verbose:  BaseUploader.verbose = True

   # write the configuration dictionary for the Wafer components
   part_conf = {
      'kind_of_part'     : opt.kop,
      'manufacturer'     : opt.manufacturer,
      'unique_location'  : opt.location,
      'description'      : 'Prototype of the PS Power Hybrid.',
      'attributes'       : [('Status','Good')],
      'version'          : opt.version,
      'product_date'     : opt.date,
   }


   for sensor_sn_file in args:
      listname = os.path.splitext(os.path.basename(sensor_sn_file))[0]
      sn_list = TableReader(sensor_sn_file).getDataAsCWiseVector()[0]
      part = Upload('{}'.format(listname),part_conf,sn_list)
      part.dump_xml_data()
