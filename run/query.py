from Query import Query
from optparse     import OptionParser

if __name__ == "__main__":
    p = OptionParser(usage="usage: %prog [options]", version="1.0")

    p.add_option( '--dev',
                    action  = 'store_true',
                    default = False,
                    dest    = 'isDevelopment',
                    help    = 'Set the development database as target.')

    p.add_option( '--verbose',
                    action  = 'store_true',
                    default = False,
                    dest    = 'verbose',
                    help    = 'Force the uploaders to print their configuration and data.')

    p.add_option( '--location',
                    action  = 'store_true',
                    default = False,
                    dest    = 'locations',
                    help    = 'Print all locations stored in the DB')

    p.add_option( '--metrology',
                    type    = 'string',
                    default = None,
                    dest    = 'metrology',
                    metavar = 'STR',
                    help    = 'Get metrology results of a module')

    p.add_option( '--moduleIV',
                    type    = 'string',
                    default = None,
                    dest    = 'moduleIV',
                    metavar = 'STR',
                    help    = 'Get IV results of a module')

    p.add_option( '-l','--last',
                    action  = 'store_true',
                    default = False,
                    dest    = 'last',
                    help    = 'Only get last entry')

    (opt, args) = p.parse_args()

    db = 'trker_cmsr'
    if opt.isDevelopment:
        db = 'trker_int2r'
        
    query = Query(db,opt.verbose)

    if opt.locations:
        [print(location) for location in query.get_locations()]

    if opt.metrology:
        [print(location) for location in query.get_ot_module_metrology(opt.metrology,opt.last)]

    if opt.moduleIV:
        [print(location) for location in query.get_ot_module_iv(opt.moduleIV,opt.last)]
