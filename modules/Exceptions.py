#!/usr/bin/python
# $Id$
# Created by Alessandro Di Mattia <dimattia@cern.ch>, 23-Mar-2019

# This module contains a set of class for handling the exception cases.

from AnsiColor import Fore, Back, Style

# Exception classes for handling bad File System structures  ###################
class BadOsDataProcessing(Exception): pass
class TooManyMatchesInFilelist(BadOsDataProcessing):
   def __init__(self, threshold, filelist, tag):
      self.threshold  = threshold
      self.filelist   = filelist
      self.tag        = tag
   def __str__(self):
      txt = [Fore.RED+'\n  more than %d matches for %s found:\n' % (self.threshold,self.tag)]
      for f in self.filelist:
         txt.append('    %s\n'%f)
      txt.append(Style.RESET_ALL)
      return ''.join(txt)

class FileDoesNotExist(BadOsDataProcessing):
   def __init__(self, filename):
      self.filename   = filename
   def __str__(self):
      txt = [Fore.RED+'\n  file %s doesn not exist!\n' % self.filename]
      txt.append(Style.RESET_ALL)
      return ''.join(txt)

# Exception classes for handling the Reader errors ##############################
class BadDecoding(Exception): pass
class ColumnInconsistency(BadDecoding):
   def __init__(self,worksheet, row, columns_decoded):
      self.worksheet   = worksheet
      self.row         = row
      self.columns     = columns_decoded
   def __str__(self):
      txt = [Fore.RED+'  inconsistency in column decoding for %s:' % self.worksheet,
             '  (row, max_column, decoded) = %d, %d, %d' %\
              (self.row,self.worksheet.max_column, self.columns)+Style.RESET_ALL]
      return ''.join(txt)

class HeaderDataNotFound(BadDecoding):
   """Reports a missing values in a header of a table."""
   def __init__(self, class_name, worksheet, column):
      self.class_name = class_name
      self.worksheet  = worksheet
      self.column     = column
   def __str__(self):
      return Fore.RED+'\n  %s, no header data found for column %s in %s' % \
              (self.class_name, self.column, self.worksheet)+Style.RESET_ALL

class MatrixCellAlreadyFilled(BadDecoding):
   """Reports an attempt to store a data value in a matrix cell that was
      already filled."""
   def __init__(self, class_name, row , column, stored_data):
      self.class_name  = class_name
      self.worksheet   = row
      self.column      = column
      self.stored_data = stored_data
   def __str__(self):
      return Fore.RED+'\n  %s, matrix cell (%d,%d) is already filled with %s' % \
              (self.class_name, self.row, self.column, self.stored_data)+Style.RESET_ALL

class BadStripInconsistency(BadDecoding):
   """Reports an inconsistency in the bad strip data reading."""
   def __init__(self, class_name, bad_strips, number, comment):
      self.class_name = class_name
      self.bad_strips = bad_strips
      self.number     = number
      self.comment    = comment
   def __str__(self):
      return Fore.RED+'\n  %s, %d bad strip reported but only %d was read from the txt file:\n%s' % \
              (self.class_name, self.bad_strips, self.number, self.comment)+Style.RESET_ALL

# Exception classes for handling the uploader configuration errors #############
class BadConfiguration(Exception): pass
class MissingParameter(BadConfiguration):
   """reports a missing configuration parameter."""
   def __init__(self, class_name, parameters):
      self.class_name = class_name
      self.parameters = (" | ").join( set(parameters) ) \
         if isinstance(parameters,list) else parameters
   def __str__(self):
      return Fore.RED+'\n  %s, missing configuration parameter <%s>' % \
              (self.class_name, self.parameters)+Style.RESET_ALL

class MissingValue(BadConfiguration):
   """reports a missing configuration value."""
   def __init__(self, class_name, parameter_name, missing_value):
      self.class_name      = class_name
      self.parameter_name  = parameter_name
      self.parameter_name  = missing_value
   def __str__(self):
      return Fore.RED+'\n  %s, missing value for %s in parameter %s' % \
              (self.class_name, self.missing_value, self.parameter_name)+Style.RESET_ALL

class BadOptionValue(BadConfiguration):
   """Reports a not allowed value for a configuration option."""
   def __init__(self, class_name, parameter_name, parameter_value):
      self.class_name      = class_name
      self.parameter_name  = parameter_name
      self.parameter_value = parameter_value
   def __str__(self):
      return Fore.RED+'\n  %s, bad option value for %s: %s' % \
              (self.class_name, self.parameter_name, self.parameter_value)+Style.RESET_ALL

class BadInputParameter(BadConfiguration):
   """Reports a not allowed input parameter."""
   def __init__(self,class_name, parameter_name, parameter_value):
      self.class_name      = class_name
      self.parameter_name  = parameter_name
      self.parameter_value = parameter_value
   def __str__(self):
      return Fore.RED+'\n  %s, bad input parameter: %s cannot be %s' % \
             (self.class_name, self.parameter_name, self.parameter_value)+Style.RESET_ALL
class NotRecognized(BadConfiguration):
   """Reports a not recognized option or parameter."""
   def __init__(self,class_name, name, comment):
      self.class_name = class_name
      self.name       = name
      self.comment    = comment
   def __str__(self):
      return Fore.RED+'\n %s,  %s is not recognized %s' % \
             (self.class_name, self.name, self.comment)+Style.RESET_ALL

class BadParameters(BadConfiguration):
   """Reports a bad set of input parameters."""
   def __init__(self,class_name, msg):
      self.class_name = class_name
      self.msg        = msg
   def __str__(self):
      return Fore.RED+'\n  %s: %s' % (self.class_name, self.msg )+Style.RESET_ALL

# Exception classes for handling the mismatch among header data and table data
class DataMismatch(Exception): pass
class MissingData(DataMismatch):
   """Reports a missing data from a table reader."""
   def __init__(self,class_name, item_name):
      self.class_name = class_name
      self.item_name  = item_name
   def __str__(self):
      return Fore.RED+'\n  %s, no data found for %s' % \
             (self.class_name, self.item_name)+Style.RESET_ALL

# Exception classes for handling the corruption of data
class DataCorruption(Exception): pass
class BadCBCCalibration(DataCorruption):
   """Reports when the CBC calibration output looks not good."""
   def __init__(self,class_name, item_name):
      self.class_name = class_name
      self.item_name  = item_name
   def __str__(self):
      return Fore.RED+'\n  %s, %s' % \
             (self.class_name, self.item_name)+Style.RESET_ALL

class BadMeasurement(DataCorruption):
   """Reports when two columns of a data table has the same value in a row."""
   def __init__(self,class_name, item_name, item_type):
      self.class_name = class_name
      self.item_name  = item_name
      self.item_type  = item_type
   def __str__(self):
      return Fore.RED+'\n  %s for %s: %s data corrupted, check the table in the file' % \
               (self.class_name, self.item_name, self.item_type)+Style.RESET_ALL

#Exception classes for handling the errors with the database
class DBError(Exception): pass
class BadQuery(DBError):
   """Report when input query is wrong."""
   def __init__(self,class_name, item_name):
      self.class_name = class_name
      self.item_name  = item_name
   def __str__(self):
      return Fore.RED+'\n  %s: query \'%s\' is bad!' % \
             (self.class_name, self.item_name)+Style.RESET_ALL

class RecoveryNotAvailable(DBError):
   """Report when recovery directory is not available."""
   def __init__(self,class_name, msg):
      self.class_name = class_name
      self.msg  = msg
   def __str__(self):
      return Fore.RED+f'\n  {self.class_name}: {self.msg}'+Style.RESET_ALL

class DBAPINotAvailable(DBError):
   """Report when a db api is not available."""
   def __init__(self,class_name, msg):
      self.class_name = class_name
      self.msg  = msg
   def __str__(self):
      return Fore.RED+f'\n  {self.class_name}: {self.msg}'+Style.RESET_ALL
   
class UploadFailure(DBError):
   """Report when an upload procedure fails."""
   def __init__(self,class_name, msg):
      self.class_name = class_name
      self.msg  = msg
   def __str__(self):
      return Fore.RED+f'\n  {self.class_name}: fail to upload {self.msg}'+Style.RESET_ALL
   
# Exception classes for handling bad return code from child processes  ###################
class BadExecution(Exception):
   def __init__(self,command, msg):
      self.cmd = command
      self.msg  = msg
   def __str__(self):
      return Fore.BLUE+f'\n  {self.cmd}'+Fore.RED+f'\n {self.msg}'+Style.RESET_ALL