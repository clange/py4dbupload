#!/usr/bin/python
# $Id$
# Created by Alessandro Di Mattia <dimattia@cern.ch>, 12-Oct-2021

# This module contains the classes that produce the XML file for uploading
# the CROC Wafer, Chip and their Condition data.


from AnsiColor    import Fore, Back, Style
from Exceptions   import MissingData, BadStripInconsistency, BadMeasurement,\
                         NotRecognized, MissingParameter, BadParameters
from BaseUploader import BaseUploader,ComponentType, ConditionData, UploadMode
#from DataReader   import scale2, consistency_check
from lxml         import etree
from time         import gmtime, strftime
from enum         import Enum
from copy         import deepcopy
from datetime     import datetime
from shutil       import copy2

import os,sys


CrocWPDataType = Enum('CrocWPDataType','Results, DataPoint')
CrocTestType = Enum('CrocTestType','VOFS IREF VDDD VDDA ADC IV_ANA IV_DIG VCAL_HIGH VCAL_MED')

CrocKind = Enum('CrocKind','Proto, Production')

class CROC:
   """"Call to assign the correct kind and type to a CROC label"""
   
   def __init__(self, label, kind):
      """Constructor: requires the CROC label and the CROC kind (proto or Production)."""
   
      try:
         CrocKind[str(kind)]  # check if Kind is recognized
      except:
         kinds = [el.name for el in CrocKind]
         print(Fore.RED+f'{kind}'+Style.RESET_ALL+' is not a valid Kind!')
         print('valid Kinds are: '+Fore.BLUE+f'{kinds}'+Style.RESET_ALL)
         exit(1)
   
      
   
   def __init__(self, label, version):
      """Constructor: requires the CROC label and the CROC version (1.0 are Prototype)."""
   
   
    
class CROCwafer(BaseUploader):
   """Class to produce the xml file for the DBloader. It handles the CROC Wafer
      component data read from the csv file."""

   def __init__(self, configuration, mode, batch_nn, batch_id, wafer_id):
      """Constructor: it requires the dict for configuration (configuration)
         defining the user data for the upload the access to the database, the
         upload mode (mode), the wafer batch number (batch_nn), the batch ID 
         (batch_id) and the wafer ID (wafer_id).
         Mandatory parameters in configuration:
            manufacturer:    the manufacturer of the Wafer;
            attributes:      the additional attributes for the Wafer;
            kind_of_part:    the part to be inserted in the database;
            unique_location: the location where the component is located;
            version:         the version tag used to define the kind of part;
      """

      self.description_files = []

      import copy
      self.type = ComponentType.CROCwafer
      #self.database = database
      #self.batch_nn = batch_nn
      cdict = copy.deepcopy(configuration)


      # Use TMSC as the default for Manufacturer and CERN for Location
      if 'manufacturer' not in cdict.keys():  cdict['manufacturer'] = 'TSMC'
      if 'unique_location' not in cdict.keys():cdict['unique_location'] = 'CERN'

      # The class name is the name_label of the Wafer written in the txt file
      name = '{}_{}'.format(batch_nn,wafer_id)

      # Set kind_of_part according to the version tag and batch number
      ver_name = cdict['version']
      kop_name = 'CROC Wafer' if '1.0' not in ver_name else 'CROC Proto Wafer'
      cdict['kind_of_part'] = kop_name
      cdict['batch_number'] = batch_id

      try:
         self.mode = UploadMode[str(mode)]
      except:
         modes = [m.name for m in UploadMode]
         print(Fore.RED+f'{mode}'+Style.RESET_ALL+' is not a valid upload mode!')
         print('valid upload modes are: '+Fore.BLUE+f'{modes}'+Style.RESET_ALL)
         exit(1)

      if self.mode.name=='insert':   
         BaseUploader.__init__(self, name, cdict)
         self.mandatory += ['kind_of_part','unique_location','manufacturer',\
                             'attributes','version']
      else:
         cdict = {}
         cdict['kind_of_part'] = kop_name
         cdict['version']      = ver_name  # version is needed to distinguish among the kind_of_part of CROC
         BaseUploader.__init__(self, name, cdict)
         self.mandatory += ['kind_of_part','version']

      

   def add_description(self,description_files):
      """Load the description files."""
      import copy
      self.description_files = copy.deepcopy(description_files)

   def name_label(self):
      """Returns the name label of the component."""
      return self.name

   def batch_nn(self,name_label=None):
      """Return the batch id from the name label."""
      if name_label==None:  name_label = self.name
      return name_label.split('_')[0]

   def wafer_id(self,name_label=None):
      """Return the wafer_id from the name label."""
      if name_label==None:  name_label = self.name
      return name_label.split('_')[1]

   def match(self,name_label):
      """Return True if the inout name_label matches the class name."""
      if self.batch_nn()    == self.batch_nn(name_label) and\
         self.wafer_id()    == self.wafer_id(name_label):
        return True
      return False

   def kind_of_part(self,version=None):
      """Return the kind_of_part to be associated to this barcode."""
      if version==None:  version = self.retrieve('version')
      kop_name = 'CROC Wafer' if '1.0' not in version else 'CROC Proto Wafer'
      return kop_name

   def dump_xml_data(self, filename=''):
      """Writes the wafer components in the XML file for the database upload.
         It requires in input the name of the XML file to be produced."""
      inserter  = self.retrieve('inserter')
      root      = etree.Element("ROOT")
      if inserter is not None:
         root.set('operator_name',inserter)
         
      parts = etree.SubElement(root,"PARTS")

      self.xml_builder(parts)

      if filename == '':
         filename = '{}_wafer.xml'.format(self.name_label())

      with open(filename, "w") as f:
         f.write( etree.tostring(root, pretty_print = True, \
                                       xml_declaration = True,\
                                       standalone="yes").decode('UTF-8') )


   def xml_builder(self, parts, *args):
      """Processes data."""
      ex = []
      for file in self.description_files:
         filename = os.path.basename(file)
         file_ext = os.path.splitext(file)[1]
         filetime = os.path.getmtime(file)
         dt = datetime.fromtimestamp(filetime)
         filedate = dt.strftime('%Y-%m-%d %H:%M:%S')
         copy2(file, filename)
         
         if   file_ext=='.pdf':
            ex.extend([('MAP_FILE',filename),('MAP_DATE',filedate)])
         elif file_ext=='.py':
            ex.extend([('REGION_FILE',filename),('REGION_DATE',filedate)])
         elif file_ext=='.zip':
            ex.extend([('DOCS_FILE',filename),('DOCS_DATE',filedate)])
         else:
            print(f'{file} has not a recognized format .... exiting')
            sys.exit(1)

      part_id = self.db.component_id(self.name_label(),'name_label')

      if part_id==None and self.mode.name=='insert':
         # not registered
         self.build_parts_on_xml(parts,name_label=self.name_label(),extended_data=ex)
      elif part_id!=None and self.mode.name=='update':
         # registered
         self.update_parts_on_xml(parts,part_id,extended_data=ex)


class CROCchip(BaseUploader):
   """Class to produce the xml file for the DBloader. It handles the CROC chip
      component data."""
   db_check = False

   def __init__(self, configuration, wafer_namelabel, wafer_nn, chip_id):
      """Constructor: it requires the dict for configuration (configuration)
         defining the ancillary data needed for the database upload the wafer
         name_label (wafer_namelabel), the wafer name (wafer_nn) and the chip
         ID (chip_id).
         Mandatory parameters in configuration:
            manufacturer:    the manufacturer of the Wafer;
            attributes:      the additional attributes for the Wafer;
            kind_of_part:    the part to be inserted in the database;
            unique_location: the location where all the components are;
            version:         the version tag used to define the kind of part;
      """

      import copy
      self.type = ComponentType.CROCchip
      self.waferN = wafer_nn
      cdict = copy.deepcopy(configuration)

      # Use Hamamatsu as the default for Manufacturer and Location
      if 'manufacturer' not in cdict.keys():  cdict['manufacturer'] = 'TSMC'
      if 'unique_location' not in cdict.keys():cdict['unique_location'] = 'CERN'

      # The class name is the name_label of the CROC chip
      name = '{}_{}'.format(wafer_namelabel,chip_id)

      # Set kind_of_part according to the version tag
      ver_name = cdict['version']
      kop_name = 'CROC Chip' if '1.0' not in ver_name else 'CROC Proto Chip'
      cdict['kind_of_part'] = kop_name


      BaseUploader.__init__(self, name, cdict)
      self.mandatory += ['kind_of_part','unique_location','manufacturer',\
                         'attributes','version']

      attributes = self.retrieve('attributes')
      from ast import literal_eval
      row_on_wafer = 'Row {}'.format(literal_eval('0x'+chip_id[0]))
      col_on_wafer = 'Col {}'.format(literal_eval('0x'+chip_id[1]))
      attributes.append( ('Chip Row on Wafer', row_on_wafer) )
      attributes.append( ('Chip Column on Wafer', col_on_wafer) )
      self.update_configuration('attributes',attributes)
      #print (self.name, self.retrieve('attributes'))



   def name_label(self):
      """Returns the name label of the component."""
      return self.name

   def barcode(self):
      """Returns the barcode of the component."""
      return '{}{}{}'.format(self.batch_nn(),self.wafer_nn(),self.chip_id())

   def batch_nn(self,name_label=None):
      """Return the batch id from the name label."""
      if name_label==None:  name_label = self.name
      return name_label.split('_')[0]

   def wafer_id(self,name_label=None):
      """Return the wafer_id from the name label."""
      if name_label==None:  name_label = self.name
      return name_label.split('_')[1]

   def chip_id(self,name_label=None):
      """Return the chip_id from the name label."""
      if name_label==None:  name_label = self.name
      return name_label.split('_')[2]

   def wafer_nn(self):
      """Return the wafer_nn given at init time."""
      return self.waferN

   def match(self,name_label):
      """Return True if the inout name_label matches the class name."""
      if self.batch_nn()    == self.batch_nn(name_label) and\
         self.wafer_id()    == self.wafer_id(name_label) and\
         self.chip_id()     == self.chip_id(name_label):
        return True
      return False

   def kind_of_part(self,version=None):
      """Return the kind_of_part to be associated to this barcode."""
      if version==None:  version = self.retrieve('version')
      kop_name = 'CROC Chip' if '1.0' not in ver_name else 'CROC Proto Chip'
      return kop_name

   def dump_xml_data(self,filename='',wafers=None):
      """Writes the sensor component in the XML file for the database upload.
         It requires the name of the XML file to be produced. A CROCwafer
         class can also be input: in this case a parent-child relationship
         with the Wafer components is set (the wafer components must have
         been already entered in the database).
      """
      inserter  = self.retrieve('inserter')
      root      = etree.Element("ROOT")
      if inserter is not None:
         root.set('operator_name',inserter)
         
      parts = etree.SubElement(root,"PARTS")

      self.xml_builder(parts,wafers)

      if filename == '':
         filename = '%s_sensors.xml'%self.name

      with open(filename, "w") as f:
         f.write( etree.tostring(root, pretty_print = True, \
                                       xml_declaration = True,\
                                       standalone="yes").decode('UTF-8') )

   def xml_builder(self, parts, *args):
      """Processes data in the reader."""
      wafers = args[0]

      children = parts
      if wafers is not None:
         for wafer in wafers:
            if wafer.match(self.name_label()):
               wafer_kop  = wafer.kind_of_part()
               wafer_part = etree.SubElement(parts, "PART", mode="auto")
               etree.SubElement(wafer_part,"KIND_OF_PART").text  = wafer_kop
               etree.SubElement(wafer_part,"NAME_LABEL").text = wafer.name_label()
               children = etree.SubElement(wafer_part,"CHILDREN")

      part_id=None
      if self.db_check:
         part_id = self.db.component_id(self.name_label(),'name_label')
      if part_id==None:
        # not registered
        self.build_parts_on_xml(children,name_label=self.name_label(),barcode=self.barcode())



class CROCWaferTestResultsT(ConditionData):
   """Produces the XML file for storing the CROC Wafer Test result data."""
   default_CrocKind  = 'Production'
   data_description  = {('CROC Proto Wafer','Results'):  {  
                         'kind_of_part' : 'CROC Proto Wafer',     
                         'cond_name'    : 'CROC Proto Wafer Probe Results',
                         'table_name'   : 'CROC_WAFER_RESULT',
                         'DBvar_vs_TxtHeader' : { 'PROBE_CARD_ID'      : '',
                                                  'PROBE_CARD_VERSION' : '',
                                                  'PROBE_SITE'         : '',
                                                  'TEST_START'         : '',
                                                  'CUT_VERSION'        : '',
                                                  'ENABLED_TESTS'      : '',
                                                  'NOTES'              : '',
                                                },
                         'mandatory' : ['PROBE_CARD_ID','PROBE_CARD_VERSION','PROBE_SITE',
                                        'TEST_START','CUT_VERSION','ENABLED_TESTS']
                                                          },
                        
                        ('CROC Proto Chip','Results'):  {  
                         'kind_of_part' : 'CROC Proto Chip',     
                         'cond_name'    : 'CROC Proto Chip Probe Results',
                         'table_name'   : 'CROC_CHIP_RESULT',
                         'DBvar_vs_TxtHeader' : { 'PROBE_SITE'        : '',
                                                  'TEST_START'        : '',
                                                  'VREF_OVP_V'        : '',
                                                  'BANDGAP_V'         : '',
                                                  'IANA_DEFAULT_A'    : '',
                                                  'IDIG_DEFAULT_A'    : '',
                                                  'IREF_TRIM_CODE'    : '',
                                                  'VOFS_V'            : '',
                                                  'IREF_A'            : '',
                                                  'VDDA_DEFAULT_V'    : '',
                                                  'VDDD_DEFAULT_V'    : '',
                                                  'VDDD_TRIM_CODE'    : '',
                                                  'VDDD_TRIM_V'       : '',
                                                  'IDIG_CONFIG_A'     : '',
                                                  'VDDA_TRIM_V'       : '',
                                                  'VDDA_TRIM_CODE'    : '',
                                                  'IANA_CONFIG_A'     : '',
                                                  'VREF_ADC_V'        : '',
                                                  'VREF_PRE_V'        : '',
                                                  'VINA_START_V'      : '',
                                                  'VIND_START_V'      : '',
                                                  'OCCUP_ANA'         : '',
                                                  'OCCUP_DIG'         : '',
                                                  'EFUSE_CODE'        : '',
                                                  'IANA_PERIFERY_A'   : '',
                                                  'IDIG_PERIFERY_A'   : '',
                                                  'ADC_OFF_V'         : '',
                                                  'ADC_SLO'           : '',
                                                  'IV_ANA_OFF_V'      : '',
                                                  'IV_ANA_K'          : '',
                                                  'IV_DIG_OFF_V'      : '',
                                                  'IV_DIG_K'          : '',
                                                  'THRES_MEAN'        : '',
                                                  'THRES_RMS_UNTUNED' : '',
                                                  'PIX_STUCK'         : '',
                                                  'THRES_RMS_TUNED'   : '',
                                                  'NOISE_MEAN'        : '',
                                                  'NOISE_RMS'         : '',
                                                  'THRES_HIGH'        : '',
                                                  'THRES_LOW'         : '',
                                                  'TDAC_LOW'          : '',
                                                  'TDAC_HIGH'         : '',
                                                  'VCAL_HIGH_OFF'     : '',
                                                  'VCAL_HIGH_SLO'     : '',
                                                  'VCAL_MED_OFF'      : '',
                                                  'VCAL_MED_SLO'      : '',
                                                  'RADSENS_ACB'       : '',
                                                  'RADSENS_SLDOA'     : '',
                                                  'RADSENS_SLDOD'     : '',
                                                  'TEMPSENS_ACB'      : '',
                                                  'TEMPSENS_SLDOA'    : '',
                                                  'TEMPSENS_SLDOD'    : '',
                                                  'TEMP_DEGC'         : '',
                                                  'GRADE'             : '',
                                                  'FAILURE_REASON'    : '',
                                                  'INJ_CAPACIT_F'     : '',
                                                },
                         'mandatory' : ['PROBE_SITE','TEST_START','GRADE']
                                                          },
                        ('CROC Proto Chip','DataPoint'):  {  
                         'kind_of_part' : 'CROC Proto Chip',     
                         'cond_name'    : 'CROC Proto Chip Probe Data',
                         'table_name'   : 'CROC_CHIP_DATA',
                         'DBvar_vs_TxtHeader' : { 'CROC_DATA_ID' : '',
                                                  'X'            : '',
                                                  'Y'            : '',
                                                },
                         'mandatory' : ['CROC_DATA_ID','X','Y']
                                                          }
                        }
   
   def __init__(self, label, data_type, data_version='v1', kind=None, inserter=None):
      """Constructor: it requires the label of the component and the data type (Results
         or Data Point). It also accepts the data set version (set to 'v1' as default) 
         and the name of the shadown operator (inserter, set to None by default).

         Mandatory confirguration parameter to be provided are:
            run_name:        name of condition run measurement;
            run_type:        type of condition run measurement;
            run_begin:       timestamp of the begin of run
            kind_of_part:    the type of component whose data belong to;
            cond_name:       kind_of_condition name corresponding to these data;
            table_name:      condition data table where data have to be recorded;
            data_version:    data set version;
            DBV_vs_Theader:  describe how to associate data to the DB variables;
         run_name and run_type + run_number are exclusive. Serial, barcode and
         name_label are mutual exclusive.

         Optional configuration parameters are:
            run_number:   run number to be associated to the run type
            run_end:      timestamp of the end of run
            run_operator: operator that performed the run
            run_location: site where the run was executed
            run_comment:  description/comment on the run
            data_comment: description/comment on the dataset
            inserter:     shadow of the person uploading the data (override the CERN user name)
      """
      
      self.h_kind = kind
      
      try:
         self.h_type = CrocWPDataType[str(data_type)]
      except:
         data_types = [t.name for t in CrocWPDataType]
         print(Fore.RED+f'{data_type}'+Style.RESET_ALL+' is not a valid type for CROC condition!')
         print('valid types are: '+Fore.BLUE+f'{data_types}'+Style.RESET_ALL)
         exit(1)
   
      self.data_points = []
      name = '{}_results'.format(label)

      configuration = {}
      data_description=self.data_description[(self.h_kind,self.h_type.name)]
      configuration.update(deepcopy(data_description))
      configuration['name_label'] = label
      configuration['data_version']=data_version
      
      if inserter!=None:
         configuration['inserter']=inserter
      

      ConditionData.__init__(self, name, configuration, None)


   def build_data_block(self,dataset):
      """Builds the data block."""
      
      if self.h_type.name=="DataPoint":      # Fills the data point block in the XML
         
         data_description = self.retrieve('DBvar_vs_TxtHeader')
         for point in self.data_points:
            #print(point)
            data = etree.SubElement(dataset,"DATA")
            for var in data_description.keys():
               #print(var)
               try:
                  value = point[var]
                  #print(value)
                  etree.SubElement(data,"{}".format(var)).text = value
               except:  pass
      
      else:                                       # Fills the result block in the XML
         
         data_description = self.retrieve('DBvar_vs_TxtHeader')
         data = etree.SubElement(dataset,"DATA")
         tags = data_description.keys()

         # Check for mandatory tags
         for el in self.retrieve("mandatory"):
            if data_description[el]=='':
               raise MissingParameter(self.__class__.__name__,el)

         for el in data_description.keys():
            if data_description[el]!='':
               etree.SubElement(data,f"{el}").text = data_description[el]



   def load_result_block(self,name,value):
      """Load the result block content. Check the consistency of the name tag."""
      data_block = self.retrieve('DBvar_vs_TxtHeader')
      data_block_tags = data_block.keys()
      if name not in data_block_tags:
         raise BadParameters(self.__class__.__name__,f'{name} is not a tag for {self.h_kind} {self.h_type}.')
      if isinstance(value,datetime):
         data_block[name]=value.strftime('%Y-%m-%d %H:%M:%S')
      else:
         data_block[name]=value



   def load_data_points(self,test_type,x,y):
      """Load the data points and check consistency with the data description."""
      try:
         test_type = CrocTestType[str(test_type)]
      except:
         test_types = [t.name for t in CrocTestType]
         print(Fore.RED+f'{test_type}'+Style.RESET_ALL+' is not a valid type for CROC test!')
         print('valid types are: '+Fore.BLUE+f'{test_types}'+Style.RESET_ALL)
         exit(1)
      self.data_points.append({'CROC_DATA_ID' : str(test_type.name),
                               'X'            : str(x),
                               'Y'            : str(y)})


   def load_run_metadata(self, run_type=None, run_number=None, run_begin=None, run_end=None, 
                               run_operator=None, run_location=None, run_comment=None ):
      """Loads the run metadata."""
      if run_type!=None: self.update_configuration('run_type',run_type)
      if run_number!=None: self.update_configuration('run_number',run_number)
      if run_begin!=None: self.update_configuration('run_begin',run_begin.strftime('%Y-%m-%d %H:%M:%S'))
      if run_end!=None: self.update_configuration('run_end',run_end.strftime('%Y-%m-%d %H:%M:%S'))
      if run_operator!=None: self.update_configuration('run_operator',run_operator)
      if run_location!=None: self.update_configuration('run_location',run_location)
      if run_comment!=None: self.update_configuration('run_comment',run_comment)

      
   def dump_xml_data(self, filename=''):
      """Dump the sensor condition data in the XML file for the database upload.
         It requires in input the name of the XML file to be produced."""
      inserter  = self.retrieve('inserter')
      root      = etree.Element("ROOT")
      if inserter is not None:
         root.set('operator_name',inserter)

      self.xml_builder(root)

      if filename == '':
         filename = '{}.xml'.format(self.name)

      with open(filename, "w") as f:
         f.write( etree.tostring(root, pretty_print = True, \
                                       xml_declaration = True,\
                                       standalone="yes").decode('UTF-8') )
