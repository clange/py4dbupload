#!/usr/bin/python
# $Id$
# Created by Alessandro Di Mattia <dimattia@cern.ch>, 23-Mar-2019

# This module contains the base classes implementing the common functionalities
# needed for uploading the data in the database.

from Exceptions import *
from lxml       import etree
from AnsiColor  import Fore, Back, Style
from enum       import Enum
from Utils      import DBaccess


UploadMode = Enum('UploadMode','insert update')

ComponentType = Enum('ComponentType','OTwafer OTsensor OThalfmoon CROCwafer CROCchip SSAwafer SSAchip CICwafer CICchip MPAwafer MPAchip')
#OTDataType    = Enum('OTDataType','iv cv bad_strip summary')

class BaseUploader:
   """Class implementing the basic functionalities needed to produce the XML
      and Json file for the database upload."""

   verbose  = False
   debug    = False
   database = 'cmsr'

   manufacturer_tags = {
               'Hamamatsu-HPK'    : 'HPK',
               'Global Foundries' : 'GLF'
             }

   manufacturers = {}
   locations     = {}
   institutions  = {}
   

   def __init__(self, name=None, configuration=None, reader=None):
      """Constructor setting the configuration and some default values."""

      self.name = name if name!=None else ""
      self.reader = reader
      self.configuration = configuration
      self.mandatory = []
      
      self.db = DBaccess(database=f'trker_{self.database}',verbose=self.debug)
      if len(self.locations) == 0:
         qy=f'select * from {self.db.database}.trkr_locations_v l order by l.location_name ASC'
         data = self.db.data_query(qy)
         for l in data:
            self.locations[l['locationId']] = {
                                        'locationName'    : l['locationName'],
                                        'institutionId'   : l['institutionId'],
                                        'institutionName' : l['institutitonName']
                                     }
            self.institutions[l['locationName']] = l['institutitonName']

      if len(self.manufacturers) == 0:
         qy=f'select * from {self.db.database}.trkr_manufacturers_v l order by l.manufacturer_name ASC'
         data = self.db.data_query(qy)
         for m in data:
            self.manufacturers[m['manufacturerId']] = m['manufacturerName']      
      
      if self.verbose:   print(self)

   def check_for_missing_parameter(self,name):
      """Raise an missing parameter exception if a parameter is mandatory."""
      if name in self.mandatory:
         raise MissingParameter(self.__class__.__name__,name)

   def update_configuration(self,key,value):
      """Update the value corresponding to the key inside the configuration
         dictionary. If key is not present it will be inserted."""
      self.configuration[key] = value

   def update_locations(self,location, number):
      """Update a location for a part identified by its number (or serial
         number). The update is registered into the "locations" parameter."""
      if location not in self.configuration['locations'].keys():
         self.configuration['locations'].update( {location:[ number ]} )
      else:
         self.configuration['locations'][location].append( number )

   def retrieve(self,name):
      """Retrieve the configuration values. If the parameter is not found in
         the configuration dictionary it checks if the parameter is mandatory."""
      if name in self.configuration.keys():
         return self.configuration[name]
      self.check_for_missing_parameter(name)
      return None


   def update_parts_on_xml(self, parts, part_id, \
                                 serial=None, barcode=None, name_label=None,\
                                 extended_data=None):
      """Updates the part data in the XML tree in input (parts). The parameters
         are retrieved from the configuration dictionary apart from the part_id
         that must be provided as input parameters. The specialized classes have
         to provide the part_id by gathering it from the database with a query.
      """

      part = etree.SubElement(parts,"PART",mode="auto")

      etree.SubElement(part,"PART_ID").text = part_id

      #if serial==None and barcode==None and name_label==None:
      #   raise BadParameters(self.__class__.__name__,\
      #      'serial number, barcode and name label cannot be all None!')

      etree.SubElement(part,"KIND_OF_PART").text = self.retrieve('kind_of_part')

      if serial is not None:
        etree.SubElement(part,"SERIAL_NUMBER").text = serial

      if barcode is not None:
        etree.SubElement(part,"BARCODE").text = barcode

      if name_label is not None:
        etree.SubElement(part,"NAME_LABEL").text = name_label

      location = self.location(serial)
      if location is not None:
        etree.SubElement(part,"LOCATION").text = location
        etree.SubElement(part,"INSTITUTION").text =\
           self.institution(location,serial)

      manufacturer = self.retrieve('manufacturer')
      if manufacturer is not None:
         if manufacturer in list(self.manufacturers.values()):
            etree.SubElement(part,"MANUFACTURER").text = manufacturer
         else:
            raise BadParameters(self.__class__.__name__,
                             f'manufacturer {manufacturer} is not described in CMSR')

      description = self.retrieve('description')
      if description is not None:
         etree.SubElement(part,"COMMENT_DESCRIPTION").text = description

      version = self.retrieve('version')
      if version is not None:
         etree.SubElement(part,"VERSION").text  = version

      production_date = self.retrieve('product_date')
      if production_date is not None:
         etree.SubElement(part,"PRODUCTION_DATE").text = production_date

      batch_number = self.retrieve('batch_number')
      if batch_number is not None:
         etree.SubElement(part,"BATCH_NUMBER").text = batch_number

      ###inserter = self.retrieve('inserter')
      ###if inserter is not None:
      ###etree.SubElement(part,"RECORD_INSERTION_USER").text = inserter

      attributes = self.retrieve('attributes')
      if attributes is not None and len(attributes)!=0:
         patt = etree.SubElement(part,"PREDEFINED_ATTRIBUTES")
         for a in attributes:
            attr = etree.SubElement(patt,"ATTRIBUTE")
            etree.SubElement(attr,"NAME").text = a[0]
            etree.SubElement(attr,"VALUE").text = a[1]
            try:
               if a[2]=='delete': etree.SubElement(attr,"DELETED").text = 'true'
            except:
               pass
      
      if extended_data!=None:
         extension_table = etree.SubElement(part,"PART_EXTENSION")
         for d in extended_data:
            key  = d[0]
            data = d[1]
            etree.SubElement(extension_table,key).text = data
            
      return part


   def build_parts_on_xml(self,parts,serial=None,barcode=None,name_label=None,\
                          extended_data=None):
      """Builds the part data in the XML tree in input (parts). The parameters
         are retrieved from the configuration dictionary apart from the serial
         number, the barcode,the name label and the extended data that must be
         provided as input parameters. The specialized classes have to provide
         them either by gathering them from the configuration or by obtaining
         them from the worksheets."""

      part = etree.SubElement(parts,"PART",mode="auto")

      if serial==None and barcode==None and name_label==None:
         raise BadParameters(self.__class__.__name__,\
            'serial number, barcode and name label cannot be all None!')

      etree.SubElement(part,"KIND_OF_PART").text = self.retrieve('kind_of_part')
      
      manufacturer = self.retrieve('manufacturer')
      if manufacturer is not None:
         if manufacturer in list(self.manufacturers.values()):
            etree.SubElement(part,"MANUFACTURER").text = manufacturer
         else:
            raise BadParameters(self.__class__.__name__,
                             f'manufacturer {manufacturer} is not described in CMSR')
      #etree.SubElement(part,"MANUFACTURER").text = self.retrieve('manufacturer')
      #etree.SubElement(part,"SERIAL_NUMBER").text = serial

      if serial is not None:
         etree.SubElement(part,"SERIAL_NUMBER").text = serial

      if barcode is not None:
        etree.SubElement(part,"BARCODE").text = barcode

      if name_label is not None:
        etree.SubElement(part,"NAME_LABEL").text = name_label

      description = self.retrieve('description')
      if description is not None:
         etree.SubElement(part,"COMMENT_DESCRIPTION").text = description

      version = self.retrieve('version')
      if version is not None:
         etree.SubElement(part,"VERSION").text  = version

      etree.SubElement(part,"LOCATION").text = self.location(serial)
      etree.SubElement(part,"INSTITUTION").text =\
           self.institution(self.location(serial),serial)

      ###inserter = self.retrieve('inserter')
      ###if inserter is not None:
      ###   etree.SubElement(part,"RECORD_INSERTION_USER").text = inserter

      production_date = self.retrieve('product_date')
      if production_date is not None:
         etree.SubElement(part,"PRODUCTION_DATE").text = production_date

      batch_number = self.retrieve('batch_number')
      if batch_number is not None:
         etree.SubElement(part,"BATCH_NUMBER").text = batch_number

      attributes = self.retrieve('attributes')
      if attributes is not None and len(attributes)!=0:
         patt = etree.SubElement(part,"PREDEFINED_ATTRIBUTES")
         for a in attributes:
            attr = etree.SubElement(patt,"ATTRIBUTE")
            etree.SubElement(attr,"NAME").text = a[0]
            etree.SubElement(attr,"VALUE").text = a[1]
            try:
               if a[2]=='delete': etree.SubElement(attr,"DELETED").text = 'true'
            except:
               pass

      if extended_data!=None:
         extension_table = etree.SubElement(part,"PART_EXTENSION")
         for d in extended_data:
            key  = d[0]
            data = d[1]
            etree.SubElement(extension_table,key).text = data

      return part

   def build_header_on_xml(self, tree, site=None, operator=None, comment=None,\
                           run_number=None, run_begin=None, run_end=None):
      """Builds the XML header needed by the condition data. The parameters of
         the dataset are retrieved from the configuration dictionary. The site
         where the data was taken, the operator that performed the measurement
         and a comment can be input as optional parameters together with the
         XML element tree where to write the info."""
      header = etree.SubElement(tree,"HEADER")
      #type contains fixed data
      typeds = etree.SubElement(header,"TYPE")
      etree.SubElement(typeds,"EXTENSION_TABLE_NAME").text = self.retrieve('table_name')
      etree.SubElement(typeds,"NAME").text = self.retrieve('data_name')

      #run contains variable run number
      from time import gmtime, strftime
      run = etree.SubElement(header,"RUN")

      # check if we use run_name or run_type + run_number
      try:
         etree.SubElement(run,"RUN_NAME").text = self.retrieve('run_name')
      except BadConfiguration:
         try:
            etree.SubElement(run,"RUN_TYPE").text = self.retrieve('run_type')
            etree.SubElement(run,"RUN_NUMBER").text = run_number
         except BadConfiguration:
            raise MissingParameter(self.__class__.__name__,['run_name','run_type'])

      #etree.SubElement(run,"RUN_BEGIN_TIMESTAMP").text = \
      # strftime("%Y-%m-%d %H:%M:%S", gmtime())

      if run_begin is not None:
         etree.SubElement(run,"RUN_BEGIN_TIMESTAMP").text = run_begin
      if run_end is not None:
         etree.SubElement(run,"RUN_END_TIMESTAMP").text = run_end
      if operator is not None:
         etree.SubElement(run,'INITIATED_BY_USER').text = operator
      if site is not None:
         etree.SubElement(run,'LOCATION').text = site
      if comment is not None:
         etree.SubElement(run,'COMMENT_DESCRIPTION').text = comment

      #inserter = self.retrieve('inserter')
      #if inserter is not None:
      #   etree.SubElement(run,"RECORD_INSERTION_USER").text = inserter


   def build_runMetaData_on_xml(self, header):
      """Builds the XML run header needed by the condition data. The parameters
         of the dataset are retrieved from the configuration dictionary."""

      run = etree.SubElement(header,"RUN")

      # check if we have a run sequencer defined in configuration
      run_sequencer = self.retrieve('run_sequencer')

      # check if we use run_name / run_type + run_number / automatic run assignment
      try:
         etree.SubElement(run,"RUN_NAME").text = self.retrieve('run_name')
      except BadConfiguration:

         try: # we must recover a run_type at least
            etree.SubElement(run,"RUN_TYPE").text = self.retrieve('run_type')
         except BadConfiguration:
            raise MissingParameter(self.__class__.__name__,['run_name','run_type'])

         try: # run number may not be there .... use automatic assignment
            etree.SubElement(run,"RUN_NUMBER").text = self.retrieve('run_number')
         except BadConfiguration:
            run.set('mode','AUTO_INC_NUMBER')
            if run_sequencer!=None:
               run.set('mode','SEQUENCE_NUMBER')
               run.set('sequence','{}'.format(run_sequencer))

      # run begin timestamp is a mandatory parameter
      etree.SubElement(run,"RUN_BEGIN_TIMESTAMP").text = self.retrieve('run_begin')

      # optional parameters
      run_end      = self.retrieve('run_end')
      run_operator = self.retrieve('run_operator')
      run_location = self.retrieve('run_location')
      run_comment  = self.retrieve('run_comment')
      ###inserter     = self.retrieve('inserter')

      if run_end is not None:
         etree.SubElement(run,"RUN_END_TIMESTAMP").text = run_end
      if run_operator is not None:
         etree.SubElement(run,'INITIATED_BY_USER').text = run_operator
      if run_location is not None:
         etree.SubElement(run,'LOCATION').text = run_location
      if run_comment is not None:
         etree.SubElement(run,'COMMENT_DESCRIPTION').text = run_comment
      ###if inserter is not None:
      ###   etree.SubElement(run,"RECORD_INSERTION_USER").text = inserter


   def location(self,serial_number):
      """Retrieve the location from the configuration dictionary. It looks for
         "unique_location" and for "locations" in an exclusive way. The former
         sets a single location for the components entered by the class, while
         the latter allows for assigning the components to different locations
         according to their serial number."""
      location = ''
      try:
         location = self.retrieve('unique_location')
      except BadConfiguration:
         try:
            locations = self.retrieve('locations')
            for k in locations.keys():
               serial_list = locations[k]
               for i in serial_list:
                  if i == serial_number:
                     location = k
         except:
            raise MissingParameter(self.__class__.__name__, ['locations','unique_location'])
      if location in self.institutions.keys() or location is None:
         return location
      else:
         raise BadParameters(self.__class__.__name__,
                             f'location {location} is not described in CMSR')
         

   def institution(self,location,serial_number):
      """Associates the institution to a given location. It looks inside the
         static dictionary for an institution that pairs the location. If no
         institution is associated, then retrieves the institution from the
         configuration dictionary by looking for "unique_location" and for
         "locations" in an exclusive way. The former sets a single institution
         for the components entered by the class, while the latter allows for
         assigning the components to different locations according to their
         serial number."""
      if location in self.institutions.keys():
         return self.institutions[location]

      try:
         return self.retrieve('unique_institution')
      except BadConfiguration:
         try:
            institution = self.retrieve('institutions')
            for k in institution.keys():
               serial_list = institution[k]
               for i in serial_list:
                  if i == serial_number:  return k
         except BadConfiguration:
            raise MissingParameter(self.__class__.__name__, ['institutions','unique_institution'])


   def xml_builder(self,xml_root,*args):
      """Process data and steer the XML file building. The XML root tree is
         passed in as a parameter. At this stage this is a dummy function which
         must be implemented by the SubClasses."""
      pass

   def __str__(self):
      """Print out configuration and data content."""
      txt = []

      txt.append( '\nUploader for ' + Fore.BLUE + self.name + '\n')
      for key in self.configuration.keys():
         v = self.configuration[key]
         txt.append(Fore.RED+'  {:15.15}'.format(key) + Fore.BLACK + ' : {}\n'.format(v))


      if self.reader != None: txt.append( self.reader.__str__() )
      txt.append( Style.RESET_ALL + '\n' )

      return ''.join(txt)



class ConditionData(BaseUploader):
   """Class implementing the requirements needed to produce the run metadata
      header section of the xml file for uploading the Condition Data."""

   def __init__(self, name, cdict, dreader):
      """Constructor: it requires the name of the instance (name) the
      configuration (cdict) and the data reader (dreader).

      Mandatory configuration is:
         run_name:        name of condition run measurement;
         run_type:        type of condition run measurement;
         run_number:      run number to be associated to the run type
         run_begin:       timestamp of the begin of run
         kind_of_part:    the type of component whose data belong to;
         serial:          identifier of the component whose data belong to;
         barcode:         identifier of the component whose data belong to;
         name_label:      identifier of the component whose data belong to;
         cond_name:       kind_of_condition name corresponding to these data;
         table_name:      condition data table where data have to be recorded;
         data_version:    data set version;
         DBV_vs_Theader:  describe how to associate data table to DB variables;
      run_name and run_type + run_number are exclusive.

      Optional parameters are:
         run_end:      timestamp of the end of run
         run_operator: operator that performed the run
         run_location: site where the run was executed
         run_comment:  description/comment on the run
         data_comment: description/comment on the dataset
         inserter:     person recording the run metadata (override the user)
      """

      BaseUploader.__init__(self, name, cdict, dreader)

      self.mandatory += ['run_type','run_name','run_number','run_begin',\
                         'kind_of_part','name_label','serial','barcode',\
                         'cond_name','table_name','data_version',\
                         'DBV_vs_Theader']

      self.attach_part = True
      self.is_child    = False
      self.children    = []
      self.only_run    = False

   def add_child(self,child):
      """Add a child dataset to this one."""
      child.is_child = True
      self.children.append(child)

   def dump_xml_data(self, filename=''):
      """Dump the sensor condition data in the XML file for the database upload.
         It requires in input the name of the XML file to be produced."""
      root  = etree.Element("ROOT")

      self.xml_builder(root)

      if filename == '':
         filename = '{}_data.xml'.format(self.name)

      with open(filename, "w") as f:
         f.write( etree.tostring(root, pretty_print = True, \
                                       xml_declaration = True,\
                                       standalone="yes").decode('UTF-8') )


   def build_dataset_block(self,dataset):
      """Build the dataset block."""
      data_comment = self.retrieve('data_comment')
      if data_comment!=None:
         etree.SubElement(dataset,'COMMENT_DESCRIPTION').text = data_comment
      etree.SubElement(dataset,'VERSION').text   = self.retrieve('data_version')


   def build_part_block(self,part):
       """Build the connection with the component."""
       etree.SubElement(part,"KIND_OF_PART").text = self.retrieve('kind_of_part')
       try:
          etree.SubElement(part,"NAME_LABEL").text = self.retrieve('name_label')
       except BadConfiguration:
          try:
             etree.SubElement(part,"SERIAL_NUMBER").text = self.retrieve('serial')
          except BadConfiguration:
             try:
                etree.SubElement(part,"BARCODE").text = self.retrieve('barcode')
             except BadConfiguration:
                raise MissingParameter(self.__class__.__name__,\
                                          ['serial','barcode','name_label'])

   def build_data_block(self,dataset):
       """Build the data block."""
       table_data = self.reader.getDataAsCWiseDictRowSplit()
       data_description = self.retrieve('DBvar_vs_TxtHeader')
       for point in table_data:
          data = etree.SubElement(dataset,"DATA")
          for var in data_description.keys():
             try:
                value = point[data_description[var]]
                etree.SubElement(data,"{}".format(var)).text = value
             except:  pass


   def build_type_block(self,tree):
      """"Build the Type block."""
      # type contains fixed data for both normal and child data set
      typeds = etree.SubElement(tree,"TYPE")
      etree.SubElement(typeds,"EXTENSION_TABLE_NAME").text = self.retrieve('table_name')
      etree.SubElement(typeds,"NAME").text = self.retrieve('cond_name')


   def write_data_block(self):
      """Check if data block must be written."""
      return True


   def xml_builder(self,root_tree):
      """Process data in the reader."""
      if not self.write_data_block():    return

      dataset = None

      if self.only_run:
         header = etree.SubElement(root_tree,"HEADER")
         self.build_runMetaData_on_xml(header)
         return

      if not self.is_child:
         header = etree.SubElement(root_tree,"HEADER")
         self.build_type_block(header)
         self.build_runMetaData_on_xml(header)
         dataset = etree.SubElement(root_tree,"DATA_SET")
      else:
         child_dataset = etree.SubElement(root_tree,"CHILD_DATA_SET")
         header = etree.SubElement(child_dataset,"HEADER")
         self.build_type_block(header)
         dataset = etree.SubElement(child_dataset,"DATA_SET")

      # build data set block
      self.build_dataset_block(dataset)

      #build data set attribute block
      self.built_dataset_attributes(dataset)
      # connect dataset to component
      if self.attach_part:
         part = etree.SubElement(dataset,"PART")
         self.build_part_block(part)
      else:
         if not self.is_child: 
            # need to specify the kind_of_part to which the condition is related
            part = etree.SubElement(dataset,"PART")
            etree.SubElement(part,"KIND_OF_PART").text = self.retrieve('kind_of_part')

      # build the data block
      self.build_data_block(dataset)

      # build child datasets
      for ch in self.children:
         ch.xml_builder(dataset)


   def built_dataset_attributes(self, dataset):
      """Build the dataset attribute block."""
      attributes = self.retrieve('attributes')
      if attributes is not None and len(attributes)!=0:
         patt = etree.SubElement(dataset,"PREDEFINED_ATTRIBUTES")
         for a in attributes:
            attr = etree.SubElement(patt,"ATTRIBUTE")
            etree.SubElement(attr,"NAME").text = a[0]
            etree.SubElement(attr,"VALUE").text = a[1]
            try:
               if a[2]=='delete': etree.SubElement(attr,"DELETED").text = 'true'
            except:
               pass