from  Utils import DBaccess

class Query(DBaccess):
    def __init__(self, database='trker_cmsr', verbose=False):
        super(Query, self).__init__(database, verbose)
   
    def get_locations(self):
        query = "SELECT l.* FROM {}.trkr_locations_v l".format(self.database)

        print(query)
        data = self.data_query(query)
        return data

    def get_condition_data_tables(self, pTableName = None):
        if pTableName is None:
            query = "SELECT c.* FROM {}.conditions c".format(self.database)
        else:
            query = "SELECT c.* FROM {}.conditions c WHERE c.database_table='{}'".format(self.database,pTableName)

        print(query)
        data = self.data_query(query)  
        return data

    def get_part_data_tables(self, pPartName = None):
        if pPartName is None:
            query = "SELECT k.* FROM {}.kinds_of_part k".format(self.database)
        else:
            query = "SELECT k.* FROM {}.kinds_of_part k WHERE k.name='{}'".format(self.database,pPartName)

        print(query)
        data = self.data_query(query)
        return data

    def get_ot_module_metrology(self, pModule , pGetLast = False):
        table = self.get_condition_data_tables('TEST_MODULE_MTRLGY')[0]["conditionTable"]

        if pGetLast:
            query = "SELECT c.* \
                    FROM {}.{} c \
                    WHERE c.part_name_label='{}' \
                    ORDER BY c.condition_data_set_id DESC \
                    OFFSET 0 ROWS FETCH NEXT 1 ROWS ONLY".format(self.database,table,pModule)
        else:
            query = "SELECT c.* \
                    FROM {}.{} c \
                    WHERE c.part_name_label='{}'".format(self.database,table,pModule)
        
        print(query.replace("  ",""))
        data = self.data_query(query)
        return data
    
    def get_ot_module_iv(self, pModule , pGetLast = False):
        table = self.get_condition_data_tables('TEST_MODULE_IV')[0]["conditionTable"]

        if pGetLast:
            #Get highgest condidition data set id
            max_id_query = "SELECT c.condition_data_set_id \
                        FROM {}.{} c \
                        WHERE c.part_name_label='{}' \
                        ORDER BY c.condition_data_set_id DESC \
                        OFFSET 0 ROWS FETCH NEXT 1 ROWS ONLY".format(self.database,table, pModule)

            print(max_id_query.replace("  ",""))
            max_id = self.data_query(max_id_query)[0]["conditionDataSetId"]

            #get all entries with highest condition data set id
            query = "SELECT c.* \
                    FROM {}.{} c \
                    WHERE c.part_name_label='{}' \
                    AND c.condition_data_set_id={} \
                    ORDER BY c.condition_data_set_id DESC".format(self.database,table, pModule,max_id)
        else:
            query = "SELECT c.* \
                    FROM {}.{} c \
                    WHERE c.part_name_label='{}'".format(self.database,table,pModule)

        print(query.replace("  ",""))
        data = self.data_query(query)
        return data