#!/usr/bin/python
# $Id$
# Created by Stefan Maier <s.maier@kit.edu>, 22-January-2024

# This module contains the classes that produce the XML file for uploading
# the OT Module Metrology data.

from BaseUploader import ConditionData
from shutil       import copyfile
from copy         import deepcopy
from Utils        import search_files, check_ot_module_label
from Exceptions   import *


class OTDeeIntTest(ConditionData):
   """Produces the XML file to upload OT Module Metrology data."""

   data_description  = {    'kind_of_part' : 'TEDD DEE',     
                            'cond_name'    : 'DEE Module Integration Test',
                            'table_name'   : 'INTTEST_DEE',
                            'DBvar_vs_TxtHeader' : {
                               'SENSOR_TEMP_TEST':'SENSOR_TEMP_TEST',
                               'SENSOR_TEMP_DEGC':'SENSOR_TEMP_DEGC',
                               'OPTIC_TXM_TEST':'OPTIC_TXM_TEST',
                               'OPTIC_TXM_T':'OPTIC_TXM_T',
                               'OPTIC_TXM_V':'OPTIC_TXM_V',
                               'OPTIC_TXM_X':'OPTIC_TXM_X',
                               'OPTIC_TXM_TX':'OPTIC_TXM_TX',
                               'OPTIC_TXM_RX':'OPTIC_TXM_RX',
                               'CH_DEAD_TEST':'CH_DEAD_TEST',
                               'CH_DEAD':'CH_DEAD'},
                               'mandatory' : ['SENSOR_TEMP_TEST','OPTIC_TXM_TEST','CH_DEAD_TEST']}

   def __init__(self, pRunDataReader, pMeasurementDataReader=None):

      """Constructor: it requires configuration of the upload and the measurement data
      pDataType:
         SUMMARY or DATA depending which block of the xml file is generated
      pRunDataReader:
         NameLabel
         Date
         Commen
         Location
         Inserter
      pSummaryDataReader
         STATION
      pMeasurementDataReader:
         KIND_OF_METROLOGY:    metrology type
         ROTATION_URAD: Rotation of the sensors
         X_SHIFT_UM:    X shift of the sensors
         Y_SHIFT_UM:    Y shift of the sensors
      """

      dee_serial  = pRunDataReader.getDataFromTag('#SerialNumber')
      location    = pRunDataReader.getDataFromTag('#Location')
      comment     = pRunDataReader.getDataFromTag('#Comment')
      inserter    = pRunDataReader.getDataFromTag('#Inserter')
      data        = pRunDataReader.getDataFromTag("#Date")
      run_type    = pRunDataReader.getDataFromTag("#RunType")
      
      configuration = {}
      data_description = self.data_description

      configuration.update(deepcopy(data_description))
      configuration['inserter']     = inserter
      configuration['serial']       = dee_serial
      configuration['run_comment']  = comment
      configuration['data_comment'] = comment
      configuration['run_location'] = location
      configuration['run_begin']    = data
      configuration['run_type']    = run_type
      configuration['data_version'] = "1.0"
      #configuration['attributes']   = [['Data Quality','Good']]  

      name = 'DATA-{}'.format(dee_serial)
      configuration.update(self.data_description)
      ConditionData.__init__(self, name, configuration, pMeasurementDataReader)

      self.do_checks(dee_serial, location)

   def do_checks(self, module_name, location):
      #Check Block

      #Check if the module exists in the DB
      if self.db.component_id(module_name, 'serial_number') is None:
         print("Error - Module not in DB!")
         exit(1)
      else:
         print("Module part id: checked")


      location_id_of_location = self.db.get_location_id(location)

      #Check if the location name is valid
      if location_id_of_location is None:
         print("Error - location does not exist")
         exit(1)
      else:
         print("Location: checked")

      """
      #Check if the measurement is at the same location as the component is
      location_id_of_component = self.db.component_location(module_name)
      print(location_id_of_location)
      print(location_id_of_component)
      if location_id_of_location != location_id_of_component:
         print("Error - Location of measurement does not match location of coponentn in DB")
         exit(1)
      else:
         print("Location of component matches: checked")
      """