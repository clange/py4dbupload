#!/usr/bin/python
# $Id$
# Created by Stefan Maier <s.maier@kit.edu>, 22-January-2024

# This module contains the classes that produce the XML file for uploading
# the OT Module Metrology data.

from BaseUploader import ConditionData
from shutil       import copyfile
from copy         import deepcopy
from Utils        import search_files, check_ot_module_label
from Exceptions   import *


class OTModuleTestRootFile(ConditionData):
   """Produces the XML file to upload OT Module Test root file."""

   data_description  = {'2S':  {  
                            'kind_of_part' : '2S Module',     
                            'cond_name'    : 'Module Analysis Root Files',
                            'table_name'   : 'TEST_ROOT_FILES',
                            'DBvar_vs_TxtHeader' : {
                                             'ROOT_FILE'  : 'ROOT_FILE',
                                             'ROOT_DATE'     : 'ROOT_DATE',
                                             'ROOT_VER'     : 'ROOT_VER',
                                             'ROOT_CONF_VER': 'ROOT_CONF_VER',
                                             'ROOT_TEMP_DEGC' : 'ROOT_TEMP_DEGC'},
                               'mandatory' : ['ROOT_FILE']},
                        'PS':  {  
                            'kind_of_part' : 'PS Module',     
                            'cond_name'    : 'Module Analysis Root Files',
                            'table_name'   : 'TEST_ROOT_FILES',
                            'DBvar_vs_TxtHeader' : {
                                             'ROOT_FILE'  : 'ROOT_FILE',
                                             'ROOT_DATE'     : 'ROOT_DATE',
                                             'ROOT_VER'     : 'ROOT_VER',
                                             'ROOT_CONF_VER': 'ROOT_CONF_VER',
                                             'ROOT_TEMP_DEGC' : 'ROOT_TEMP_DEGC'},
                               'mandatory' : ['ROOT_FILE']},
                        }
   def __init__(self, pRunDataReader, pMeasurementDataReader=None):

      """Constructor: it requires configuration of the upload and the measurement data
      pRunDataReader:
         NameLabel
         Date
         Commen
         Location
         Inserter
      pMeasurementDataReader:
         ROOT_FILE
         ROOT_DATE
         ROOT_VER
         ROOT_CONF_VER
         ROOT_TEMP_DEGC
      """

      module_name = pRunDataReader.getDataFromTag('#NameLabel')
      location    = pRunDataReader.getDataFromTag('#Location')
      comment     = pRunDataReader.getDataFromTag('#Comment')
      inserter    = pRunDataReader.getDataFromTag('#Inserter')
      date        = pRunDataReader.getDataFromTag("#Date")
      run_type    = pRunDataReader.getDataFromTag("#RunType")
      
      configuration = {}
      if "2S" in module_name:
         data_description = self.data_description["2S"]
      else:
         if "PS" in module_name:
            data_description = self.data_description["PS"]
         else:
            print("Module Name does not contain 2S or PS")
            exit(0)

      configuration.update(deepcopy(data_description))
      configuration['inserter']     = inserter
      configuration['serial']       = module_name
      configuration['run_comment']  = comment
      configuration['data_comment'] = comment
      configuration['run_location'] = location
      configuration['run_begin']    = date
      configuration['run_type']     = run_type
      configuration['data_version'] = "1.0"
      configuration['attributes']   = [['Data Quality','Good']]  

         #Check if the data provided could cause harm during upload
      name = 'DATA-{}'.format(module_name)
      configuration.update(self.data_description)
      ConditionData.__init__(self, name, configuration, pMeasurementDataReader)
      self.do_checks(module_name, location)
      
   def do_checks(self, module_name, location):
      #Check Block

      #Check if the name label is correct
      if not check_ot_module_label(module_name):
         print("Error - Module label not correct!")
         exit(1)
      else:
         print("Module name label: checked")
      #Check if the module exists in the DB
      if self.db.component_id(module_name) is None:
         print("Error - Module not in DB!")
         exit(1)
      else:
         print("Module part id: checked")


      location_id_of_location = self.db.get_location_id(location)

      #Check if the location name is valid
      if location_id_of_location is None:
         print("Error - location does not exist")
         exit(1)
      else:
         print("Location: checked")


      #Check if the measurement is at the same location as the component is
      location_id_of_component = self.db.component_location(module_name)
      #print(location_id_of_location)
      #print(location_id_of_component)
      if location_id_of_location != location_id_of_component:
         print("Error - Location of measurement does not match location of coponentn in DB")
         exit(1)
      else:
         print("Location of component matches: checked")
