#!/usr/bin/python
# $Id$
# Created by Stefan Maier <s.maier@kit.edu>, 22-January-2024

# This module contains the classes that produce the XML file for uploading
# the OT Module Metrology data.

from BaseUploader import ConditionData
from shutil       import copyfile
from copy         import deepcopy
from Utils        import search_files, check_ot_module_label
from Exceptions   import *


class OTModuleMetrology(ConditionData):
   """Produces the XML file to upload OT Module Metrology data."""

   summary_data   = {'cond_name' : 'Tracker Module Assembly Summary Data',
                     'table_name': 'TEST_ASSEMBLY_SMMRY',                   
                     'run_type'  : 'mod_bareModule',
                     'DBvar_vs_TxtHeader' : {
                                 'STATION'          : 'STATION',
                                 'AV_TEMP_DEGC'     : 'Not in txt file',
                                 'AV_RH_PRCNT'      : 'Not in txt file',
                                 'RMS_FORCE_MEASURED_G' : 'Not in txt file',
                                 'RMS_FORCE_CORRECTED_G' : 'Not in txt file'
                                            },
                     'mandatory' : ['STATION']
                    }

   data_description  = {'2S':  {  
                            'kind_of_part' : '2S Module',     
                            'cond_name'    : 'Tracker Module Metrology',
                            'table_name'   : 'TEST_MODULE_MTRLGY',
                            'DBvar_vs_TxtHeader' : {
                                             'ROTATION_URAD'  : 'ROTATION_URAD',
                                             'X_SHIFT_UM'     : 'X_SHIFT_UM',
                                             'Y_SHIFT_UM'     : 'Y_SHIFT_UM',
                                             'KIND_OF_METROLOGY': 'KIND_OF_METROLOGY'},
                               'mandatory' : ['KIND_OF_METROLOGY','ROTATION_URAD','X_SHFIT_UM','Y_SHIFT_UM']},
                        'PS':  {  
                            'kind_of_part' : 'PS Module',     
                            'cond_name'    : 'Tracker Module Metrology',
                            'table_name'   : 'TEST_MODULE_MTRLGY',
                            'DBvar_vs_TxtHeader' : {
                                             'ROTATION_URAD'  : 'ROTATION_URAD',
                                             'X_SHIFT_UM'     : 'X_SHIFT_UM',
                                             'Y_SHIFT_UM'     : 'Y_SHIFT_UM',
                                             'KIND_OF_METROLOGY': 'KIND_OF_METROLOGY'},
                               'mandatory' : ['KIND_OF_METROLOGY','ROTATION_URAD','X_SHFIT_UM','Y_SHIFT_UM']}
                        }

   def __init__(self, pDataType, pRunDataReader, pSummaryDataReader, pMeasurementDataReader=None):

      """Constructor: it requires configuration of the upload and the measurement data
      pDataType:
         SUMMARY or DATA depending which block of the xml file is generated
      pRunDataReader:
         NameLabel
         Date
         Commen
         Location
         Inserter
      pSummaryDataReader
         STATION
      pMeasurementDataReader:
         KIND_OF_METROLOGY:    metrology type
         ROTATION_URAD: Rotation of the sensors
         X_SHIFT_UM:    X shift of the sensors
         Y_SHIFT_UM:    Y shift of the sensors
      """

      self.type   = pDataType
      module_name = pRunDataReader.getDataFromTag('#NameLabel')
      location    = pRunDataReader.getDataFromTag('#Location')
      comment     = pRunDataReader.getDataFromTag('#Comment')
      inserter    = pRunDataReader.getDataFromTag('#Inserter')
      date        = pRunDataReader.getDataFromTag("#Date")
      configuration = {}
      if "2S" in module_name:
         data_description = self.data_description["2S"]
      else:
         if "PS" in module_name:
            data_description = self.data_description["PS"]
         else:
            print("Module Name does not contain 2S or PS")
            exit(0)


      configuration.update(deepcopy(data_description))
      configuration['inserter']     = inserter
      configuration['serial']       = module_name
      configuration['run_comment']  = comment
      configuration['data_comment'] = comment
      configuration['run_location'] = location
      configuration['run_begin']    = date
      configuration['data_version'] = "1.0"
      configuration['attributes']   = [['Data Quality','Good']]  

      #print(configuration)
      if self.type == "SUMMARY":
         name = 'SUMMARY-{}'.format(module_name)
         configuration.update(self.summary_data)
         ConditionData.__init__(self, name, configuration, pSummaryDataReader)

         #Check if the data provided could cause harm during upload
         self.do_checks(module_name, location)

      if self.type == "DATA":
         name = 'DATA-{}'.format(module_name)
         configuration.update(self.data_description)
         ConditionData.__init__(self, name, configuration, pMeasurementDataReader)
      
   def do_checks(self, module_name, location):
      #Check Block

      #Check if the name label is correct
      if not check_ot_module_label(module_name):
         print("Error - Module label not correct!")
         exit(1)
      else:
         print("Module name label: checked")
      #Check if the module exists in the DB
      if self.db.component_id(module_name) is None:
         print("Error - Module not in DB!")
         exit(1)
      else:
         print("Module part id: checked")


      location_id_of_location = self.db.get_location_id(location)

      #Check if the location name is valid
      if location_id_of_location is None:
         print("Error - location does not exist")
         exit(1)
      else:
         print("Location: checked")


      #Check if the measurement is at the same location as the component is
      location_id_of_component = self.db.component_location(module_name)
      #print(location_id_of_location)
      #print(location_id_of_component)
      if location_id_of_location != location_id_of_component:
         print("Error - Location of measurement does not match location of coponentn in DB")
         exit(1)
      else:
         print("Location of component matches: checked")
