#!/usr/bin/python
# $Id$
# Created by Alessandro Di Mattia <dimattia@cern.ch>, 21-Aug-2023

# This module contains the classes that produce the XML file for uploading
# the VTRx+ data.


from BaseUploader import BaseUploader
from DataReader   import *
from progressbar  import *
from lxml         import etree
from shutil       import copyfile
import os

class VTRx(BaseUploader):
    """Class to produce the xml file for the DBloader upload. It handles the
      VTRx component data read from a csv file."""

    def __init__(self, cdefault, idDevice, idFuse, codePigtail):
        """Constructor: it requires the dict for configuration (cdefault) 
         defining the ancillary data needed for the database upload, the 
         fuse id (idFuse) and the pigtail code (codePigtail).
         Mandatory parameters in configuration:
            manufacturer:    the manufacturer of the Wafer;
            kind_of_part:    the part to be inserted in the database;
            unique_location: the location where all the components are;
            locations:       dictionary of locations for each components;
         unique_location and locations are mutually exclusive.
         Optionale parameters are:
            version:         the version of CBC chip produced;
            description      a descriptive comment;
      """
        import copy
        cdict = copy.deepcopy(cdefault)
        
        if 'manufacturer' not in cdict.keys():
            cdict['manufacturer'] = 'AEMtec'
        
        self.device_id=idDevice
        self.fuse_id=idFuse
        self.pigtail_code=codePigtail
        
        BaseUploader.__init__(self, f'vtrx_{self.serial_number()}', cdict)
        
        self.mandatory += ['manufacturer', 'kind_of_part',
         'unique_location', 'locations']

    def serial_number(self):
        """Produces the serial number of the VTRx."""
        return '%s' % self.fuse_id

    def barcode(self):
        """Produces the barcode of the VTRx."""
        barcode = 16611000000+int(self.device_id)
        return f'{barcode}'
    
    def name_label(self):
        """Produces the name label of the VTRx."""
        return f'VTRx+_{self.module_length():05.1f}cm_{self.barcode()}'
    
    def pigtail_length(self):
        """Produces the pigtail length (cm) from the code pigtail."""
        return float(self.pigtail_code.split('-')[-1])
    
    def module_length(self):
        """Produces the Module length (cm) from the code pigtail."""
        return float(self.pigtail_length())+3

    def dump_xml_data(self, filename=''):
        """Writes the wafer data in the XML file for the database upload. It
         requires in input the name of the XML file to be produced."""
        
        inserter  = self.retrieve('inserter')
        root = etree.Element('ROOT')
        if inserter is not None:
            root.set('operator_name',inserter)

        parts = etree.SubElement(root,"PARTS")
        
        part_id = self.db.component_id(self.name_label(),'name_label')
        if part_id==None: 
            # not registered:
            self.xml_builder(parts)

            if filename == '':
               filename = f'vtrx_{self.name_label()}.xml'

            with open(filename, "w") as f:
               f.write( etree.tostring(root, pretty_print = True, \
                                       xml_declaration = True,\
                                       standalone="yes").decode('UTF-8') )
            return filename
        
        print(f'{self.name} already registered!')
        return None

    def xml_builder(self, parts, *args):
        """Process data, reader is not used."""
        Serial = self.serial_number()
        NameLabel = self.name_label()
        Barcode   = self.barcode()

        e_data = [('PIGTAIL_LENGTH_CM', str(self.pigtail_length())), ('MODULE_LENGTH_CM', str(self.module_length()))]

        self.build_parts_on_xml(parts, serial=Serial, name_label=NameLabel, \
                                barcode=Barcode, extended_data=e_data)


