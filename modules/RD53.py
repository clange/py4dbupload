#!/usr/bin/python
# $Id$
# Created by Alessandro Di Mattia <dimattia@cern.ch>, 23-Mar-2019

# This module contains the classes that produce the XML file for uploading
# the RD53 chip and the RD53 Wafer data.

from BaseUploader import BaseUploader
from DataReader   import *
from lxml         import etree
from shutil       import copyfile

import re,os

class RD53Wafer(BaseUploader):
   """Class to produce the xml file for the DBloader upload. It handles the
      RD53 Wafer component data read from a worksheet."""

   def __init__(self, name , cdict, dreader):
      """Constructor: it requires the instance name, the dict for configuration
         (cdict) defining the ancillary data needed for the database upload and
         the text database data (dreader).
         Mandatory parameters in configuration:
            manufacturer:    the manufacturer of the Wafer;
            kind_of_part:    the part to be inserted in the database;
            version:         the version of RD53 chip produced;
            unique_location: the location where all the components are;
            locations:       dictionary of locations for each components;
         unique_location and locations are mutually exclusive.
      """
      #Use Hamamatsu as default Manufacturer for Sensors
      if 'manufacturer' not in cdict.keys():
         cdict['manufacturer'] = 'TMSC'

      BaseUploader.__init__(self, name, cdict, dreader)

      self.reader           = dreader

      self.mandatory += ['manufacturer','kind_of_part','version']

      if 'unique_location' not in cdict.keys():
         cdict['unique_location'] = self.read_location()

      #self.check_configuration()

   def serial_number(self):
      """Produces the serial number of the RD53 wafer."""
      data = self.reader.getDataAsCWiseDict()
      return data['%CMSwaferCode'][0]

   def name_label(self):
      """Produces the name label for the RD53 wafer."""
      data = self.reader.getDataAsCWiseDict()
      return data['%CMSwaferCode'][0]

   def read_location(self):
      """read the location for the RD53 wafer from the input database."""
      data = self.reader.getDataAsCWiseDict()
      return data['Location'][0]

   def dump_xml_data(self, filename=''):
      """Writes the wafer data in the XML file for the database upload. It
         requires in input the name of the XML file to be produced."""
      inserter  = self.retrieve('inserter')
      root      = etree.Element("ROOT")
      if inserter is not None:
         root.set('operator_name',inserter)

      self.xml_builder(root)

      if filename == '':
         filename = '%s_RD53wafers.xml'%self.name
      with open(filename, "w") as f:
         f.write( etree.tostring(root, pretty_print = True, \
                                       xml_declaration = True,\
                                       standalone="yes").decode('UTF-8') )
      #from zipfile import ZipFile
      #zipfilename = os.path.splitext(filename)[0]+".zip"

      #with ZipFile(zipfilename,'w') as zip:
      #   zip.write(filename)
      #   zip.write( os.path.basename(self.mapfile) )

      #os.remove(filename)
      #os.remove( os.path.basename(self.mapfile) )


class RD53Chip(BaseUploader):
   """Class to produce the xml file for the DBloader upload. It handles the
      RD53 chip component data read from a worksheet."""

   def __init__(self, name, cdict, dreader):
      """Constructor: it requires the instance name (name), the dict for
         configuration (cdict) defining the ancillary data needed for the
         database upload, the text database data (dreader).
         Mandatory parameters in configuration:
            manufacturer:    the manufacturer of the Wafer;
            kind_of_part:    the part to be inserted in the database;
            version:         the version of CBC chip produced;
            unique_location: the location where all the components are;
            locations:       dictionary of locations for each components;
         unique_location and locations are mutually exclusive.
      """
      #Use Hamamatsu as default Manufacturer for Sensors
      if 'manufacturer' not in cdict.keys():
         cdict['manufacturer'] = 'Global Foundries'

      BaseUploader.__init__(self, name, cdict, dreader)

      self.reader           = dreader

      self.mandatory += ['manufacturer','kind_of_part','version']


   def dump_xml_data(self, filename='', chip_wafer=None):
      """Writes the wafer data in the XML file for the database upload. It
         requires in input the name of the XML file to be produced."""
      inserter  = self.retrieve('inserter')
      root      = etree.Element("ROOT")
      if inserter is not None:
         root.set('operator_name',inserter)

      self.xml_builder(root,chip_wafer)

      if filename == '':
         filename = '%s_RD53chip.xml'%self.name

      with open(filename, "w") as f:
         f.write( etree.tostring(root, pretty_print = True, \
                                       xml_declaration = True,\
                                       standalone="yes").decode('UTF-8') )


class RD53WaferFromTxtDatabase(RD53Wafer):
   """Produces the XML file from the test database."""

   def __init__(self, name, cdict, dreader):
      """Constructor: it requires the reder of the text database."""
      RD53Wafer.__init__(self, name, cdict, dreader)

   def xml_builder(self,root_tree,*args):
      """Process data, reader is not used."""
      parts = etree.SubElement(root_tree,"PARTS")

      Serial    = self.serial_number()
      NameLabel = self.name_label()

      self.configuration['unique_location'] = self.read_location()

      self.build_parts_on_xml(parts,serial=Serial)

class RD53ChipFromTxtDatabase(RD53Chip):
   """Produces the XML file from the Excel spreadsheet that contains the chip
      serial numbers and the functional test result."""

   def __init__(self, name, cdict, dreader):
      """Constructor: it requires the same arguments of the base class."""
      RD53Chip.__init__(self, name, cdict, dreader)

   def serial_number(self):
      """Produces the serial number of the RD53 wafer."""
      data = self.reader.getDataAsCWiseDict()
      return data['%CMSwaferCode'][0]

   def xml_builder(self,root_tree,*args):
      """Process data in the reader."""
      chip_wafer = args[0]

      parts = etree.SubElement(root_tree,"PARTS")
      data = self.reader.getDataAsCWiseDict()

      chip_locations   = getColumnData(data,"Location")
      chip_serial_ids  = getColumnData(data,"CMSchipCode")
      chip_name_labels = getColumnData(data,"RD53chipID")
      chip_cols        = getColumnData(data,"ChipColumn")
      chip_rows        = getColumnData(data,"ChipRow")
      chip_comments    = getColumnData(data,"Comment")

      for p, chip in enumerate(chip_serial_ids,0):
         Serial       = chip_serial_ids[p]
         NameLabel    = chip_name_labels[p]
         row_pos_tag  = 'Row %s'%chip_rows[p]
         col_pos_tag  = 'Col %s'%chip_cols[p]

         chip_attributes = [('Chip Row on Wafer',row_pos_tag),
                            ('Chip Column on Wafer',col_pos_tag)]
         self.configuration['attributes']  = chip_attributes
         self.configuration['description'] = chip_comments[p]
         self.configuration['unique_location'] = chip_locations[p]

         if chip_wafer is not None:
            w_kop    = chip_wafer.retrieve('kind_of_part')
            w_serial = chip_wafer.serial_number()
            wp = etree.SubElement(parts, "PART", mode="auto")
            etree.SubElement(wp,"KIND_OF_PART").text  = w_kop
            etree.SubElement(wp,"SERIAL_NUMBER").text = w_serial
            children = etree.SubElement(wp,"CHILDREN")
            self.build_parts_on_xml(children,serial=Serial,name_label=NameLabel)
         else:
            self.build_parts_on_xml(parts,serial=Serial,name_label=NameLabel)

         #self.build_parts_on_xml(parts,serial=Serial,name_label=NameLabel)

class RD53ConditionData(BaseUploader):
   """Class to produce the xml file for the DBloader upload. It handles the
      RD53 chip condition data read from a worksheet."""

   def __init__(self, name, cdict, dreader, runp):
      """Constructor: it requires the instance name (name), the dict for
         configuration (cdict) defining the ancillary data needed for the
         database upload, the text database data (dreader) and the run number 
         provider (runp).
         Mandatory parameters in configuration:
            kind_of_part:     the part to which attach the dataset;
            run_name:         the name of the run;
            run_type:         the type of the run;
            run_location;     the location where the run was performed;
            table_name:       the name of the extension table;
            data_name:        the name of the kind of condition;
            dataset_version:  the version of the dataset;
         run_name and run_type are mutually exclusive.

         Optional parameters are:
            + run_operator:     the operator that performed the run;
            + run_begin:        the time when the run begun;
            + run_end:          the time when the run ended;
            + run_comment:      a description of the run;
            + dataset_comment:  a description of the dataset;
      """

      BaseUploader.__init__(self, name, cdict, dreader)

      self.reader = dreader
      self.runp   = runp

      self.mandatory += ['kind_of_part',    \
                         'run_name',        \
                         'run_type',        \
                         'run_location',    \
                         'table_name',      \
                         'data_name',       \
                         'dataset_version']


   def dump_xml_data(self, filename=''):
      """Writes the condition data in the XML file for the database upload. It
         requires in input the name of the XML file to be produced."""
      inserter  = self.retrieve('inserter')
      root      = etree.Element("ROOT")
      if inserter is not None:
         root.set('operator_name',inserter)

      self.xml_builder(root)

      if filename == '':
         filename = '%s_RD53data.xml'%self.name

      with open(filename, "w") as f:
         f.write( etree.tostring(root, pretty_print = True, \
                                       xml_declaration = True,\
                                       standalone="yes").decode('UTF-8') )

class RD53ProbingDataFromTxt(RD53ConditionData):
   """Class to produce the xml file for the DBloader upload. It handles the
      RD53 chip condition data read from a worksheet."""

   def __init__(self, name, cdict, dreader,runp):
      """Constructor: it requires the instance name (name), the dict for
         configuration (cdict) defining the ancillary data needed for the
         database upload, the text database data (dreader).
         Mandatory parameters in configuration:
            DBvar_vs_TxtHeader: map amon DB var name adn TXT header tag;
      """
      #Activate the run_sequencer
      #if 'run_sequencer_on' not in cdict.keys():
      #   cdict['run_sequencer_on'] = ''

      RD53ConditionData.__init__(self, name, cdict, dreader, runp)

      self.mandatory += ['DBvar_vs_TxtHeader']


   def xml_builder(self,root_tree,*args):
      """Process data in the reader."""
      data = self.reader.getDataAsCWiseDictRowSplit()

      for chip in data:

         chip_serial_id  = chip["CMSchipCode"]
         probingSite     = chip["ProbingSite"]
         Operator        = None
         RunBegin        = None
         RunEnd          = None
         if probingSite=='Bonn':
            Operator = 'Michael Daas'
            RunBegin = '2019-04-01'
            RunEnd   = '2019-04-30'
         elif probingSite=='CERN':
            Operator = 'Luis Miguel Jara Casas'
            RunBegin = '2019-04-01'
            RunEnd   = '2019-07-31'
         elif probingSite=='Torino':
            Operator = 'Ennio Monteil'
            RunBegin = '2019-08-01'
            RunEnd   = '2019-10-31'
         else:
             print ('{} non instrumented'.format(probingSite))

         # still missing run_operator, run_begin and run_end time
         self.build_header_on_xml(root_tree, site=probingSite, operator=Operator,
                                  run_number=self.runp.run_number(),
                                  run_begin=RunBegin, run_end=RunEnd)

         #build the dataset part
         dataset = etree.SubElement(root_tree,"DATA_SET")

         # missing the comment description
         #etree.SubElement(dataset,'COMMENT_DESCRIPTION').text = description
         etree.SubElement(dataset,'VERSION').text = \
                                             self.retrieve('dataset_version')

         part = etree.SubElement(dataset,"PART")
         etree.SubElement(part,"KIND_OF_PART").text=\
                                               self.retrieve('kind_of_part')

         etree.SubElement(part,"SERIAL_NUMBER").text = chip_serial_id

         #enter the data
         data = etree.SubElement(dataset,"DATA")
         DBvar_vs_TxtHeader = self.retrieve('DBvar_vs_TxtHeader')
         for var in DBvar_vs_TxtHeader.keys():
            try:
               value = chip[DBvar_vs_TxtHeader[var]]
               if var=='IREF_MUAMP': value = float(value)*1.e6
               etree.SubElement(data,"{}".format(var)).text = str(value)
            except:
                raise MissingData(var)


class RD53GradingDataFromTxt(RD53ConditionData):
   """Class to produce the xml file for the DBloader upload. It handles the
      RD53 chip grading data read from a worksheet."""

   def __init__(self, name, cdict, dreader, runp):
      """Constructor: it requires the instance name (name), the dict for
         configuration (cdict) defining the ancillary data needed for the
         database upload, the text database data (dreader).
         Mandatory parameters in configuration:
            DBvar_vs_TxtHeader: map amon DB var name adn TXT header tag;
      """
      #Activate the run_sequencer
      #if 'run_sequencer_on' not in cdict.keys():
      # cdict['run_sequencer_on'] = ''

      RD53ConditionData.__init__(self, name, cdict, dreader, runp)

      self.mandatory += ['DBvar_vs_TxtHeader']


   def xml_builder(self,root_tree,*args):
      """Process data in the reader."""
      data = self.reader.getDataAsCWiseDictRowSplit()

      for chip in data:

         chip_serial_id  = chip["CMSchipCode"]
         probingSite     = chip["ProbingSite"]
         if probingSite=='Bonn':
            Operator = 'Michael Daas'
            RunBegin = '2019-04-01'
            RunEnd   = '2019-04-30'
         elif probingSite=='CERN':
            Operator = 'Luis Miguel Jara Casas'
            RunBegin = '2019-04-01'
            RunEnd   = '2019-07-31'
         elif probingSite=='Torino':
            Operator = 'Ennio Monteil'
            RunBegin = '2019-08-01'
            RunEnd   = '2019-10-31'
         else:
             print ('{} non instrumented'.format(probingSite))
         # still missing run_operator, run_begin and run_end time
         self.build_header_on_xml(root_tree, site=probingSite, operator=Operator,
                                  run_number=self.runp.run_number(),
                                  run_begin=RunBegin, run_end=RunEnd)

         #build the dataset part
         dataset = etree.SubElement(root_tree,"DATA_SET")

         # missing the comment description
         #etree.SubElement(dataset,'COMMENT_DESCRIPTION').text = description
         etree.SubElement(dataset,'VERSION').text = \
                                             self.retrieve('dataset_version')

         part = etree.SubElement(dataset,"PART")
         etree.SubElement(part,"KIND_OF_PART").text=\
                                               self.retrieve('kind_of_part')

         etree.SubElement(part,"SERIAL_NUMBER").text = chip_serial_id

         #enter the data
         data = etree.SubElement(dataset,"DATA")
         DBvar_vs_TxtHeader = self.retrieve('DBvar_vs_TxtHeader')
         for var in DBvar_vs_TxtHeader.keys():
            try:
               value = chip[DBvar_vs_TxtHeader[var]]
               if var=='IREF_MUAMP': value = float(value)*1.e6
               etree.SubElement(data,"{}".format(var)).text = str(value)
            except:
                raise MissingData(var)
